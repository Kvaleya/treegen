﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	// Code adapted from: https://stackoverflow.com/a/17094254
	// https://stackoverflow.com/questions/17094189/crossplatform-random-number-generator
	public class SimpleRandom
	{
		const uint Magic1 = 0xdeadbeef;
		const uint Magic2 = 0x0badf00d;

		private uint _w = 0; /* must not be zero */
		private uint _z = 0; /* must not be zero */

		public SimpleRandom(int seed)
		{
			unchecked
			{
				uint s = (uint)seed;
				_z = s ^ Magic1;
				_w = s ^ Magic2;

				if (_z == 0)
					_z = Magic1;
				if (_w == 0)
					_w = Magic2;
			}
		}

		private uint GetRandom()
		{
			unchecked
			{
				_z = 36969 * (_z & 65535) + (_z >> 16);
				_w = 18000 * (_w & 65535) + (_w >> 16);
				return (_z << 16) + _w;  /* 32-bit result */
			}
		}

		/// <summary>
		/// Generates a random positive integer
		/// </summary>
		/// <returns>An integer in range 0..0x7ffffff</returns>
		public int Next()
		{
			unchecked
			{
				uint u = GetRandom();
				return (int)(u & 0x7fffffff);
			}
		}

		/// <summary>
		/// Generates an integer from the desired range.
		/// </summary>
		/// <param name="minInclusive">Lower bound of the range, inclusive. Smallest number that can be generated.</param>
		/// <param name="maxExclusive">Upper bound of this range, exclusive. All generated numbers will be lower than this value.</param>
		/// <returns>An integer in range [minInclusive, maxExclusive)</returns>
		public int Next(int minInclusive, int maxExclusive)
		{
			int range = maxExclusive - minInclusive;

			if (range < 1)
				throw new ArgumentException("Random number range is not valid!");

			return (int)((GetRandom() % range) + minInclusive);
		}

		/// <summary>
		/// Generates a random double in the range 0..1 (inclusive on both ends)
		/// </summary>
		public double NextDouble()
		{
			return GetRandom() / (double)uint.MaxValue;
		}

		/// <summary>
		/// Generates a random float in the range 0..1 (inclusive on both ends)
		/// </summary>
		public double NextFloat()
		{
			return (float)this.NextDouble();
		}

		/// <summary>
		/// Returns true with the specified probability
		/// </summary>
		public bool Attempt(double probability)
		{
			// Generates a random value even if the probability is 1
			var r = this.NextDouble();
			return r < probability || probability >= 1;
		}

		// Adapted from: https://stackoverflow.com/questions/5154970/how-do-i-create-a-hashcode-in-net-c-for-a-string-that-is-safe-to-store-in-a
		public static int HashString(string text)
		{
			if (text == null)
				return 37;

			unchecked
			{
				int hash = 23;
				foreach (char c in text)
				{
					hash = hash * 31 + c;
				}
				return hash;
			}
		}
	}
}
