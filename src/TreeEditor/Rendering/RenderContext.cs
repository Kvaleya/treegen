﻿using GlobCore;
using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreeEditor.Rendering
{
	class RenderContext
	{
		public int RenderWidth = 1, RenderHeight = 1;
		public Device Device;
	}
}
