﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using OpenTK.Mathematics;
using TreeEditor.Rendering;
using TreeEditor.UserInterface;
using Treegen;

namespace TreeEditor
{
	/// <summary>
	/// The main, high-level class of the editor.
	/// </summary>
	class Editor
	{
		public TreeBatch Tree = null;
		public WorldRenderer Renderer;
		public ViewWindow Window { get; private set; }
		public ImGuiRenderer ImGui;
		Stopwatch _stopwatch = new Stopwatch();
		double _lastElapsed = 0;
		double _currentElapsed = 0;
		double _currentDeltaT = 0;
		private double GetElapsed() { return _stopwatch.ElapsedTicks / (double)Stopwatch.Frequency; }
		public EditorCamera Camera = new EditorCamera();
		Vector2 _lastMouse = new Vector2(0);
		Input _oldInput = null;
		bool _mouseLocked = false;
		public TextOutput TextOutput;

		private SceneObject _ground;

		Scene _scene = new Scene();
		PolyplaneRenderer _polyplaneRenderer;

		public MenuGenerator MenuGenerator;

		/// <summary>
		/// Starts the rendering and input loop
		/// </summary>
		public void Run()
		{
			MenuGenerator = new MenuGenerator(this);

			TextOutput = new TextOutput();

			Window = new ViewWindow(() => this.UpdateAndDrawFrame(), (w, h) => this.Renderer.OnResize(w, h), TextOutput);
			Renderer = new WorldRenderer(Window.Device);

			ImGui = new ImGuiRenderer(Window, Window.Device);
			ImGui.RebuildFontAtlas();

			InitScene();

			_stopwatch.Start();
			Window.Size = new Vector2i(1280, 720);
			Window.Run();
		}

		void InitScene()
		{
			_scene.Exposure = 0.5f;
			_scene.SunDirection = new Vector3(1.0f, 0.8f, 0.4f).Normalized();
			_scene.SunLight = new Vector3(1.0f, 0.99f, 0.98f) * 8.0f;

			_scene.WorldObjects = new List<SceneObject>();

			_ground = new SceneObject()
			{
				Transform = GlobCore.Utils.GetWorldMatrix(new Vector3(0, 0, 0), Quaternion.Identity, new Vector3(1024f)),
				Mesh = Utils.TestGenPlane(),
				Color = new Vector4(0.5f, 0.5f, 0.5f, 1.0f),
			};

			_polyplaneRenderer = new PolyplaneRenderer(Window.Device, Renderer.MeshRenderer);

			Tree = new TreeBatch(TextOutput, _polyplaneRenderer);

			Tree.SetTrunkTextureDiffuse(Renderer.Device, "textures/bark1col.jpg");
			Tree.SetTrunkTextureNormal(Renderer.Device, "textures/bark1norm.jpg");
			Tree.SetLeafTextureDiffuse(ImGui, Renderer.Device, "textures/LeafSet014_2K_Color.jpg");
			Tree.SetLeafTextureNormal(Renderer.Device, "textures/LeafSet014_2K_Normal.jpg");
			Tree.SetLeafTextureAlpha(Renderer.Device, "textures/LeafSet014_2K_Opacity.jpg");
		}

		public void UpdateAndDrawFrame()
		{
			_currentElapsed = GetElapsed();
			_currentDeltaT = _currentElapsed - _lastElapsed;
			_lastElapsed = _currentElapsed;

			Input input = new Input(_currentDeltaT, _currentElapsed, Window.KeyboardState, Window.MouseState, _oldInput);
			_oldInput = input;

			// Update camera
			UpdateControls(input);

			// Update scene
			_scene.Camera = Camera.GetCamera(Window.Size.X, Window.Size.Y);

			_scene.WorldObjects.Clear();
			_scene.WorldObjects.Add(_ground);

			Tree.GatherForRendering(_scene.WorldObjects);

			// Render
			Renderer.DrawFrame(_scene, MenuGenerator.RenderWireframe);

			// Handle menus
			// Do not draw or update menus when using mouse look
			if(!_mouseLocked)
			{
				// Call BeforeLayout first to set things up
				ImGui.BeforeLayout((float)_currentDeltaT);

				// Draw our UI
				MenuGenerator.ImGuiLayout();

				// Call AfterLayout now to finish up and draw all the things
				ImGui.AfterLayout();

				MenuGenerator.AfterImgui();
			}
		}

		void UpdateControls(Input input)
		{
			// Camera lock/unlock
			if (input.IsKeyPressed(OpenTK.Windowing.GraphicsLibraryFramework.Keys.Space))
			{
				_mouseLocked = !_mouseLocked;
				//_editor.Window.CursorGrabbed = _mouseLocked; // Doesn't work?
			}

			if (!MenuGenerator.AllowMouseLook)
			{
				_mouseLocked = false;
			}
			
			if(Window.CursorVisible == _mouseLocked)
			{
				// Mouse locked state  has changed
				if(_mouseLocked)
				{
					Window.CursorVisible = false;
					Window.CursorGrabbed = true;
				}
				else
				{
					Window.CursorVisible = true;
					Window.CursorGrabbed = false;
				}
			}

			var mouse = new Vector2(input.Mouse.X, input.Mouse.Y);

			input.MouseDelta = mouse - _lastMouse;
			_lastMouse = mouse;

			if (_mouseLocked)
				Camera.UpdateControlsFPS(input);

			MenuGenerator.UpdateControls(input);
		}

		public void FileLoad(string file)
		{
			try
			{
				Tree.Deserialize(file);
				Tree.Filename = file;
				Tree.UpdateAll();
			}
			catch (Exception e)
			{
				TextOutput.OnError("Error loading file " + file + ": " + e.Message);
			}
		}

		public void FileSave()
		{
			try
			{
				Tree.Serialize(Tree.Filename);
			}
			catch (Exception e)
			{
				TextOutput.OnError("Error saving file " + (Tree.Filename ?? "null") + ": " + e.Message);
			}
		}

		public void FileExport(string file)
		{
			if (string.IsNullOrEmpty(file))
			{
				TextOutput.OnError("Invalid filename for export!");
				return;
			}

			// Ensure meshes reflect the latest parameters
			Tree.UpdateAll();

			Tree.Export(this.Window.Device, file);
		}
	}
}
