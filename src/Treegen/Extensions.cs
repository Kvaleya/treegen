﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Treegen
{
	static class Extensions
	{
		public static T First<T>(this List<T> list)
		{
			return list[0];
		}

		public static T Last<T>(this List<T> list)
		{
			return list[list.Count - 1];
		}

		public static void WriteChars(this BinaryWriter bw, string text)
		{
			for(int i = 0; i < text.Length; i++)
			{
				bw.Write((byte)text[i]);
			}
		}

		public static void WriteBytes(this BinaryWriter bw, byte val0, params byte[] vals)
		{
			bw.Write(val0);
			for(int i = 0; i < vals.Length; i++)
			{
				bw.Write(vals[i]);
			}
		}
	}
}
