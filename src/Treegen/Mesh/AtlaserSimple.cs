﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// A simple allocator of images in an atlas.
	/// </summary>
	class AtlaserSimple : IAtlaser
	{
		public List<Vector4d> FitRectangles(Vector2d targetArea, List<Vector2d> sizes)
		{
			List<Vector4d> result = new List<Vector4d>();

			if (sizes.Count == 0)
				return result;

			// Split input rectangle into same sized tiles,
			// that are large enough to hold the largest input rectangle,
			// assign each rectangle into one tile

			Vector2d largest = new Vector2d(0, 0);

			for(int i = 0; i < sizes.Count; i++)
			{
				var s = sizes[i];
				largest.X = Math.Max(largest.X, s.X);
				largest.Y = Math.Max(largest.Y, s.Y);
			}

			Vector2d tileTarget;

			// Find ideal tile size
			{
				bool fittingTiles(Vector2d tile)
				{
					long tilesx = Math.Max((long)Math.Floor(targetArea.X / tile.X), 0);
					long tilesy = Math.Max((long)Math.Floor(targetArea.Y / tile.Y), 0);
					long fits = tilesx * tilesy;
					return fits >= sizes.Count;
				}

				Vector2d tileSize = largest;

				// Make sure tileSize is smaller than targetArea
				if (tileSize.X > targetArea.X || tileSize.Y > targetArea.Y)
				{
					tileSize *= Math.Min(targetArea.X / tileSize.X, targetArea.Y / tileSize.Y);
				}

				tileSize /= sizes.Count;
				Vector2d tileSizeLast = tileSize / 2;

				// Binary search optimal tile size

				// Enlarging phase
				while (true)
				{
					if (!fittingTiles(tileSize))
						break;
					tileSizeLast = tileSize;
					tileSize *= 2.0;
				}

				Vector2d tileMin = tileSizeLast;
				Vector2d tileMax = tileSize;

				// Halving phase
				for (int i = 0; i < 20; i++)
				{
					Vector2d tileMid = (tileMin + tileMax) / 2;

					if (fittingTiles(tileMid))
					{
						// Can use larger tile
						tileMin = tileMid;
					}
					else
					{
						// Need smaller tile
						tileMax = tileMid;
					}
				}

				tileTarget = tileMin;
			}

			int tilesX = (int)Math.Floor(targetArea.X / tileTarget.X);

			// Place each image into a tile
			for(int i = 0; i < sizes.Count; i++)
			{
				int tx = i % tilesX;
				int ty = i / tilesX;

				Vector4d target = new Vector4d();
				target.X = tx * tileTarget.X;
				target.Y = ty * tileTarget.Y;

				target.Z = target.X + sizes[i].X / largest.X * tileTarget.X;
				target.W = target.Y + sizes[i].Y / largest.Y * tileTarget.Y;

				result.Add(target);
			}

			return result;
		}
	}
}
