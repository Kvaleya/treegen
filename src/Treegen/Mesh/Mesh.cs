﻿using System;
using System.Collections.Generic;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// A simple mesh class which stores vertices and indices of a mesh.
	/// </summary>
	public class Mesh
	{
		public List<MeshVertex> Vertices = new List<MeshVertex>();
		public List<int> Indices = new List<int>();

		public Mesh()
		{
		}

		/// <summary>
		/// Computes a bounding of this mesh using the supplied axes.
		/// Assumes the supplied min and max vectors are already inicialized,
		/// or contain a bounding box that will be extented to also include this mesh.
		/// </summary>
		internal void ComputeBoundingBox(Vector3 axis0, Vector3 axis1, Vector3 axis2, ref Vector3 min, ref Vector3 max)
		{
			for(int i = 0; i < Vertices.Count; i++)
			{
				var p = Vertices[i].Position;
				Vector3 distances = new Vector3(
					Vector3.Dot(p, axis0),
					Vector3.Dot(p, axis1),
					Vector3.Dot(p, axis2)
					);
				min = Vector3.ComponentMin(min, distances);
				max = Vector3.ComponentMax(max, distances);
			}
		}

		/// <summary>
		/// Computes a bounding of this mesh transformed using the supplied transform matrix.
		/// Assumes the supplied min and max vectors are already inicialized,
		/// or contain a bounding box that will be extented to also include this mesh.
		/// </summary>
		public void ComputeBoundingBox(Matrix4 transform, ref Vector3 min, ref Vector3 max)
		{
			for (int i = 0; i < Vertices.Count; i++)
			{
				var p = Vertices[i].Position;
				Vector3 distances = Vector3.TransformPosition(p, transform);
				min = Vector3.ComponentMin(min, distances);
				max = Vector3.ComponentMax(max, distances);
			}
		}

		/// <summary>
		/// Assumes the supplied min and max vectors are already inicialized,
		/// or contain a bounding box that will be extented to also include this mesh.
		/// </summary>
		public void ComputeBoundingBox(ref Vector3 min, ref Vector3 max)
		{
			for (int i = 0; i < Vertices.Count; i++)
			{
				var p = Vertices[i].Position;
				min = Vector3.ComponentMin(min, p);
				max = Vector3.ComponentMax(max, p);
			}
		}
	}
}
