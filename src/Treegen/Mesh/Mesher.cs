﻿using System;
using System.Collections.Generic;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Handles mesh generation
	/// </summary>
	public static class Mesher
	{
		/// <summary>
		/// Describes what will be triangulates as a single "bent tube" geometry.
		/// </summary>
		class Branch
		{
			public List<TreeVertex> Vertices;
			public TreeVertex Parent;
			public Vector3 StartingPosition;
			public float StartingRadius;
			public bool EndsWithPolyplane = false;
		}

		class TreeOrigin
		{
			public Vector3 StartingPosition;
			public TreeVertex Vertex;
		}

		/// <summary>
		/// Generates meshes and polyplane render commands from an array of tree graphs.
		/// Enables generating multiple trees that use the same polyplanes.
		/// </summary>
		/// <param name="mesherParams">Mesh generation parameters, mainly affecting the level of detail.</param>
		/// <param name="lodIndex">Which lod mesh parameters to use.</param>
		/// <param name="roots">The collection of root vertices of the tree graph, for each tree in this batch.</param>
		/// <returns>An object containing the resulting trunk mesh, leaf mesh, polyplane mesh and a list of instructions and meshes to render the polyplanes.</returns>
		public static MesherResult GenerateBatch(MesherParams mesherParams, int lodIndex, params TreeVertex[] roots)
		{
			MesherResult result = new MesherResult()
			{
				Meshes = new List<MesherResultMeshes>(),
				PolyplaneRenderCommands = new List<RenderCommand>(),
			};

			IAtlaser atlaser = new AtlaserSimple();
			IClusterGen clusterGen = new ClusterGenKmeans();

			
			List<Branch>[] mainBranches = new List<Branch>[roots.Length];
			List<TreeOrigin>[] polyplaneOrigins = new List<TreeOrigin>[roots.Length];
			List<TreeVertex> originVertices = new List<TreeVertex>();

			// For each tree in batch:
			for (int i = 0; i < roots.Length; i++)
			{
				var root = roots[i];

				// First, split vertices into continuous branches for easier mesh generation, and extract polyplane origins
				(List<Branch> mb, List<TreeOrigin> po) = SplitBranches(new TreeOrigin()
				{
					Vertex = root,
					StartingPosition = Vector3.Zero,
				}, mesherParams.LodParams[lodIndex].MinRadius);

				mainBranches[i] = mb;
				polyplaneOrigins[i] = po;

				// Generate clusters from polyplane origins
				foreach (var o in po)
				{
					originVertices.Add(o.Vertex);
				}
			}

			// Shared for all trees in batch:
			Dictionary<TreeVertex, TreeVertex> vertexToClusterMap = new Dictionary<TreeVertex, TreeVertex>();
			List<TreeVertex> clusterVertices = new List<TreeVertex>();
			clusterGen.GenerateClusters(originVertices, mesherParams.LodParams[lodIndex].DesiredClusterCount, vertexToClusterMap, clusterVertices);

			// Generate render commands and meshes for each cluster polyplane
			Dictionary<TreeVertex, Mesh> clusterMeshes = new Dictionary<TreeVertex, Mesh>();
			GeneratePolyplanes(clusterVertices, atlaser, mesherParams.LodParams[lodIndex].PolyplaneAtlasSize, mesherParams, result.PolyplaneRenderCommands, clusterMeshes);

			// For each tree in batch:
			for (int i = 0; i < roots.Length; i++)
			{
				var mi = new MesherResultMeshes();
				result.Meshes.Add(mi);

				// Place polyplane meshes at adequate positions in branches
				PlacePolyplaneCards(mi.MeshPolyplanes, polyplaneOrigins[i], vertexToClusterMap, clusterMeshes);

				// Generate geometry for each branch
				foreach (var branch in mainBranches[i])
				{
					GenerateBranch(branch, mi.MeshTrunk, mi.MeshLeaves,
						mesherParams.LodParams[lodIndex].TrunkParams, mesherParams.LeafParams,
						mesherParams.TrunkTexCoordDelta);
				}

				// Generate tangents for normal mapping
				MesherUtils.GenerateTangents(mi.MeshTrunk);
				MesherUtils.GenerateTangents(mi.MeshLeaves);
				MesherUtils.GenerateTangents(mi.MeshPolyplanes);
			}

			return result;
		}

		/// <summary>
		/// Takes a tree graph and splits in into "branches" that can later get triangulated into "bent tubes", or terminates branches early and generates descriptions of these early terminations, allowing them to be continued using polyplanes.
		/// </summary>
		/// <param name="treeOrigin">Describes the tree to split</param>
		/// <param name="minRadius">When greater than zero, branches will be terminated once their radius falls below this threshold, assuming a polyplane will later be generated at this spot.</param>
		/// <returns>A tuple of generated branch structures and a list of tree origins, where, for each branch terminated early, there is its tree vertex and starting position, so that the rest of the branch can be later made into a polyplane.</returns>
		static (List<Branch>, List<TreeOrigin>) SplitBranches(TreeOrigin treeOrigin, float minRadius = -1)
		{
			List<Branch> resultBranches = new List<Branch>();
			List<TreeOrigin> polyplanes = new List<TreeOrigin>();

			Stack<Branch> branchesToProcess = new Stack<Branch>();

			var firstBranch = new Branch()
			{
				Parent = null,
				Vertices = new List<TreeVertex>()
				{
					treeOrigin.Vertex
				},
				StartingPosition = treeOrigin.StartingPosition,
			};

			// Special case - entire tree is replaced by a polyplane
			if(treeOrigin.Vertex.Radius < minRadius)
			{
				polyplanes.Add(new TreeOrigin()
				{
					StartingPosition = treeOrigin.StartingPosition,
					Vertex = treeOrigin.Vertex,
				});
				return (resultBranches, polyplanes);
			}

			resultBranches.Add(firstBranch);
			branchesToProcess.Push(firstBranch);

			// Recursion with custom stack
			while(branchesToProcess.Count > 0)
			{
				var branch = branchesToProcess.Pop();

				Vector3 position = branch.StartingPosition;

				// Keep adding first child vertices into current branch
				while(branch.Vertices.Last().Children.Count > 0 && branch.Vertices.Last().Radius >= minRadius)
				{
					var last = branch.Vertices.Last();

					var children = last.Children;

					if(last != treeOrigin.Vertex)
						position += last.Forward * branch.Vertices.Last().Length;

					branch.Vertices.Add(children[0]);

					// Generate new branches for side children
					for (int i = 1; i < children.Count; i++)
					{
						var newbranch = new Branch()
						{
							Parent = last,
							Vertices = new List<TreeVertex>()
							{
								children[i],
							},
							StartingPosition = position,
							StartingRadius = branch.Vertices[branch.Vertices.Count - 2].Radius,
						};

						resultBranches.Add(newbranch);
						branchesToProcess.Push(newbranch);
					}
				}

				if (branch.Vertices.Last().Radius < minRadius)
				{
					branch.EndsWithPolyplane = true;
					polyplanes.Add(new TreeOrigin()
					{
						StartingPosition = position,
						Vertex = branch.Vertices.Last(),
					});
				}
			}

			return (resultBranches, polyplanes);
		}

		/// <summary>
		/// Helper class for polyplane generation
		/// </summary>
		class Polyplane
		{
			public RenderCommand[] Commands = new RenderCommand[3];
			public Vector3 BoxMin;
			public Vector3 BoxMax;
			public Vector3 Center => (BoxMin + BoxMax) / 2;
			public Vector3 Range => (BoxMax - BoxMin) / 2;
			public Vector3 LocalX;
			public Vector3 LocalY;
			public Vector3 LocalZ;
			public int AreasStart;
		}

		/// <summary>
		/// Takes a list of vertices that should be turned into polyplanes, outputs render commands for those polyplanes and card meshes (texture mapped to the corresponding polyplane) for each input vertex.
		/// </summary>
		/// <param name="rootVertices">Tree vertices from which polyplanes are to be generated</param>
		/// <param name="atlaser">Atlas generator, that packs polyplane texture rectangles into a bigger atlas</param>
		/// <param name="outputCommands">Render commands for generation of polyplanes</param>
		/// <param name="polyplaneCardMeshes">A map of input vertex -> a mesh of cards mapped to a planar rendering of the subtree created from the vertex.</param>
		static void GeneratePolyplanes(List<TreeVertex> rootVertices, IAtlaser atlaser, Vector2i atlasSize,
			MesherParams mesherParams, List<RenderCommand> outputCommands, Dictionary<TreeVertex, Mesh> polyplaneCardMeshes)
		{
			void GetOrientation(TreeVertex v, out Vector3 localX, out Vector3 localY, out Vector3 localZ)
			{
				localX = v.Right;
				localY = v.Forward;
				localZ = v.Down;
			}

			Matrix4 GetTrasform(Vector3 right, Vector3 up, Vector3 forward, Vector3 center)
			{
				// Mesh transform, which translates the mesh to 0,0,0 and rotates it
				Matrix4 transform = Matrix4.Identity;
				transform.Column0 = new Vector4(right, 0);
				transform.Column1 = new Vector4(up, 0);
				transform.Column2 = new Vector4(forward, 0);

				Matrix4 translation = Matrix4.Identity;
				translation.Row3.Xyz = -center;

				return transform * translation;
			}

			(Vector2, Vector2) makeViewport(Vector4d viewportMinMax, RenderCommand command)
			{
				const int border = 1;

				// Fill in viewport
				Vector2i viewportStart = new Vector2i(
					(int)Math.Ceiling(viewportMinMax.X) + border,
					(int)Math.Ceiling(viewportMinMax.Y) + border
					);
				Vector2i viewportEnd = new Vector2i(
					(int)Math.Floor(viewportMinMax.Z) - border,
					(int)Math.Floor(viewportMinMax.W) - border
					);
				command.ViewportMin = viewportStart;
				command.ViewportSize = viewportEnd - viewportStart;

				Vector2 tcMin = new Vector2(viewportStart.X / (float)atlasSize.X, viewportStart.Y / (float)atlasSize.Y);
				Vector2 tcMax = new Vector2(viewportEnd.X / (float)atlasSize.X, viewportEnd.Y / (float)atlasSize.Y);

				return (tcMin, tcMax);
			}

			var polyplanes = new List<Polyplane>();
			List<Vector2d> commandAreas = new List<Vector2d>();

			for(int i = 0; i < rootVertices.Count; i++)
			{
				var v = rootVertices[i];

				var poly = new Polyplane();
				polyplanes.Add(poly);

				// Generate render commands
				// Three commands for three planes
				var c0 = new RenderCommand();
				var c1 = new RenderCommand();
				var c2 = new RenderCommand();
				poly.Commands[0] = c0;
				poly.Commands[1] = c1;
				poly.Commands[2] = c2;
				poly.AreasStart = commandAreas.Count;

				// Only the first command gets meshes, other commands will share it with this one later
				c0.MeshTrunk = new Mesh();
				c0.MeshLeaves = new Mesh();

				// Generate branches for this polyplane
				(var mainBranches, _) = SplitBranches(new TreeOrigin()
				{
					Vertex = v,
					StartingPosition = Vector3.Zero,
				}, -1);

				// Generate geometry for each branch
				foreach (var branch in mainBranches)
				{
					GenerateBranch(branch, c0.MeshTrunk, c0.MeshLeaves,
						mesherParams.LodParams[0].TrunkParams, mesherParams.LeafParams,
						mesherParams.TrunkTexCoordDelta);
				}

				// Get branch orientation
				GetOrientation(v, out poly.LocalX, out poly.LocalY, out poly.LocalZ);

				var rotation = GetTrasform(poly.LocalX, poly.LocalY, poly.LocalZ, Vector3.Zero);
				//rotation.Transpose();

				// Compute branch bounding box using the oriented axes
				poly.BoxMin = new Vector3(float.PositiveInfinity);
				poly.BoxMax = new Vector3(float.NegativeInfinity);
				c0.MeshTrunk.ComputeBoundingBox(rotation, ref poly.BoxMin, ref poly.BoxMax);
				c0.MeshLeaves.ComputeBoundingBox(rotation, ref poly.BoxMin, ref poly.BoxMax);

				// Not setting viewport because we don't yet know where and what size each image will be

				// All the XZ swaps and negations are hacky, but it works now. The polyplanes correctly reflect the real tree, even in normals.
				// Don't touch it!
				// XY plane
				c0.Transform = GetTrasform(poly.LocalX, poly.LocalY, -poly.LocalZ, poly.Center);
				c0.Range = poly.Range;
				commandAreas.Add(new Vector2d(c0.Range.X, c0.Range.Y)); // Allocate atlas slot

				// Similar for the other two commands
				// ZY plane
				c1.Transform = GetTrasform(poly.LocalZ, poly.LocalY, poly.LocalX, new Vector3(poly.Center.Z, poly.Center.Y, poly.Center.X));
				c1.Range = poly.Range.Zyx;
				c1.MeshTrunk = c0.MeshTrunk;
				c1.MeshLeaves = c0.MeshLeaves;
				commandAreas.Add(new Vector2d(c1.Range.X, c1.Range.Y));

				// Top view
				// We draw from the top view (so that sun facing leaves are visible, not branches)
				c2.Transform = GetTrasform(poly.LocalZ, -poly.LocalX, poly.LocalY, new Vector3(poly.Center.Z, -poly.Center.X, poly.Center.Y));
				c2.Range = poly.Range.Zxy;
				c2.MeshTrunk = c0.MeshTrunk;
				c2.MeshLeaves = c0.MeshLeaves;
				commandAreas.Add(new Vector2d(c2.Range.X, c2.Range.Y));
			}

			// Fit images into atlas
			var atlaserResult = atlaser.FitRectangles(new Vector2d(atlasSize.X, atlasSize.Y), commandAreas);

			Random r = new Random(1);

			// Finish render commands and generate polyplane meshes
			for (int i = 0; i < rootVertices.Count; i++)
			{
				var poly = polyplanes[i];

				var c0 = poly.Commands[0];
				var c1 = poly.Commands[1];
				var c2 = poly.Commands[2];
				var v = rootVertices[i];

				(var tcMin0, var tcMax0) = makeViewport(atlaserResult[poly.AreasStart + 0], c0);
				(var tcMin1, var tcMax1) = makeViewport(atlaserResult[poly.AreasStart + 1], c1);
				(var tcMin2, var tcMax2) = makeViewport(atlaserResult[poly.AreasStart + 2], c2);

				// Generate polyplane mesh
				var mesh = new Mesh();
				polyplaneCardMeshes.Add(rootVertices[i], mesh);

				mesh.Indices = new List<int>()
				{
					// XY
					0, 2, 1,
					1, 2, 3,
					// YZ
					4, 6, 5,
					5, 6, 7,
					// XZ
					8, 10, 9,
					9, 10, 11,
				};

				Vector4 polyplaneColor = Vector4.One;
				Vector3 normal1 = -Vector3.UnitZ;
				Vector3 normal2 = Vector3.UnitX;
				Vector3 normal3 = -Vector3.UnitY;
				/*
				polyplaneColor.X = (float)r.NextDouble();
				polyplaneColor.Y = (float)r.NextDouble();
				polyplaneColor.Z = (float)r.NextDouble();
				float colorMin = Math.Min(Math.Min(polyplaneColor.X, polyplaneColor.Y), polyplaneColor.Z);
				float colorMax = Math.Max(Math.Max(polyplaneColor.X, polyplaneColor.Y), polyplaneColor.Z);

				polyplaneColor.Xyz = (polyplaneColor.Xyz - new Vector3(colorMin)) / (colorMax - colorMin);
				*/
				mesh.Vertices = new List<MeshVertex>()
				{
					// XY
					new MeshVertex(position: new Vector3(poly.BoxMin.X, poly.BoxMax.Y, 0),
						normal: normal1, color: polyplaneColor, texCoords: new Vector2(tcMin0.X, tcMax0.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMax.X, poly.BoxMax.Y, 0),
						normal: normal1, color: polyplaneColor, texCoords: new Vector2(tcMax0.X, tcMax0.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMin.X, poly.BoxMin.Y, 0),
						normal: normal1, color: polyplaneColor, texCoords: new Vector2(tcMin0.X, tcMin0.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMax.X, poly.BoxMin.Y, 0),
						normal: normal1, color: polyplaneColor, texCoords: new Vector2(tcMax0.X, tcMin0.Y), tangent: Vector3.Zero),
					// YZ
					new MeshVertex(position: new Vector3(0, poly.BoxMax.Y, poly.BoxMin.Z),
						normal: normal2, color: polyplaneColor, texCoords: new Vector2(tcMin1.X, tcMax1.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(0, poly.BoxMax.Y, poly.BoxMax.Z),
						normal: normal2, color: polyplaneColor, texCoords: new Vector2(tcMax1.X, tcMax1.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(0, poly.BoxMin.Y, poly.BoxMin.Z),
						normal: normal2, color: polyplaneColor, texCoords: new Vector2(tcMin1.X, tcMin1.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(0, poly.BoxMin.Y, poly.BoxMax.Z),
						normal: normal2, color: polyplaneColor, texCoords: new Vector2(tcMax1.X, tcMin1.Y), tangent: Vector3.Zero),
					// XZ
					new MeshVertex(position: new Vector3(poly.BoxMax.X, poly.Center.Y, poly.BoxMin.Z),
						normal: normal3, color: polyplaneColor, texCoords: new Vector2(tcMin2.X, tcMin2.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMax.X, poly.Center.Y, poly.BoxMax.Z),
						normal: normal3, color: polyplaneColor, texCoords: new Vector2(tcMax2.X, tcMin2.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMin.X, poly.Center.Y, poly.BoxMin.Z),
						normal: normal3, color: polyplaneColor, texCoords: new Vector2(tcMin2.X, tcMax2.Y), tangent: Vector3.Zero),
					new MeshVertex(position: new Vector3(poly.BoxMin.X, poly.Center.Y, poly.BoxMax.Z),
						normal: normal3, color: polyplaneColor, texCoords: new Vector2(tcMax2.X, tcMax2.Y), tangent: Vector3.Zero),
				};

				outputCommands.Add(c0);
				outputCommands.Add(c1);
				outputCommands.Add(c2);
			}
		}

		/// <summary>
		/// Takes a list of branches that are terminated due to being replaced by polyplanes, and places the polyplane cards at the ends of those branches.
		/// </summary>
		/// <param name="meshPolyplanes">Mesh into which the polyplane cards will be added.</param>
		/// <param name="origins">List of branch ends / polyplane origins, where card meshes will be placed.</param>
		/// <param name="clusterMap">Map of branch end vertex -> cluster vertex</param>
		/// <param name="polyplaneCardMeshes">Map of cluster vertex -> polyplane card mesh</param>
		static void PlacePolyplaneCards(Mesh meshPolyplanes, List<TreeOrigin> origins, Dictionary<TreeVertex, TreeVertex> clusterMap, Dictionary<TreeVertex, Mesh> polyplaneCardMeshes)
		{
			// Card mesh assumptions:
			// - the branch direction at the point of being replaced with cards is +Y in card mesh space
			// - the mesh X axis is aligned with the right vector of the cluster vertex that generated the cards
			// - the point 0,0,0 in mesh is where the original branch is terminated

			foreach(var origin in origins)
			{
				var cluster = clusterMap[origin.Vertex];
				var mesh = polyplaneCardMeshes[cluster];

				Matrix4 transform = Matrix4.Identity;
				transform.Row0 = new Vector4(origin.Vertex.Right, 0);
				transform.Row1 = new Vector4(origin.Vertex.Forward, 0);
				transform.Row2 = new Vector4(origin.Vertex.Down, 0);
				transform.Row3 = new Vector4(origin.StartingPosition, 1);
				transform.Transpose();
				
				MesherUtils.AppendMesh(meshPolyplanes, MesherUtils.TransformMesh(mesh, transform));
			}
		}

		/// <summary>
		/// Generates the mesh of a single branch. Does not handle polyplanes in any way other than ending the branch early if it is to be replaces with a polyplane.
		/// </summary>
		/// <param name="branch">The branch to generate.</param>
		/// <param name="meshTrunk">Mesh into which trunk vertices (that the trunk texture) will be added.</param>
		/// <param name="meshLeaves">Mesh into which leaf vertices will be added.</param>
		/// <param name="trunkParams">Trunk mesh geometry generation parameters.</param>
		/// <param name="leafParams">Leaf geometry generation parameters.</param>
		/// <param name="texCoordDelta">Controls how much should trunk texture coordinates change with unit branch length. When X is nonzero, the texture will wrap around the trunk in a spiral manner.</param>
		static void GenerateBranch(Branch branch, Mesh meshTrunk, Mesh meshLeaves, MesherTrunkParams trunkParams,
			MesherLeafParams leafParams, Vector2 texCoordDelta)
		{
			// Step 0: tesselate the branch
			SmoothBranch(branch);

			VertexRing previousRing = VertexRing.Invalid;
			Vector3 previousRight = Vector3.UnitZ;
			Vector3 previousDown = Vector3.UnitX;
			Vector3 previousCenter = branch.StartingPosition;

			Vector4 trunkVertexColor = Vector4.One;

			// Uncomment to visualize branches:
			// Random r = new Random(branch.Vertices.Count);
			// trunkVertexColor = new Vector4((float)r.NextDouble(), (float)r.NextDouble(), (float)r.NextDouble(), 1);

			if (branch.Vertices == null || branch.Vertices.Count == 0)
				throw new Exception("Branch is invalid!");

			Vector2 texCoord = Vector2.Zero;

			void advanceTexcoords(TreeVertex v)
			{
				texCoord.X += v.Length * texCoordDelta.X;
				
				// If we didn't divide Y by radius, then as the branch radius
				// keeps getting smaller, the texture would get streched in the Y direction
				// because Y texcoord would still be advancing the same distance for unit branch length,
				// but the X that wraps around branch would cover much less space.

				// This way, bark texture gets "smaller" as branch radius decreases,
				// which is unreallistic, but better than stretching.

				texCoord.Y += v.Length * texCoordDelta.Y / v.Radius;
			}

			// Special case for branch of only 1 vertex
			if(branch.Vertices.Count < 2)
			{
				var point = branch.Vertices[0];

				var ring = MesherUtils.GenVertexRing(meshTrunk, branch.StartingPosition, point.Right, point.Down,
					point.Radius, trunkParams.GetCount(point.Radius), trunkVertexColor, texCoord.Y, texCoord.X);

				Vector3 tip = branch.StartingPosition + point.Forward * point.Length;

				advanceTexcoords(point);

				// Generate a almost zero-radius ring of vertices instead of a single vertex
				// to avoid ugly texture stretching artifacts
				// Radius is nonzero to allow computing proper tangents, and is negative to avoid creating a tiny hole in the tip
				// The ring normals will also point in the direction of the tip
				var tipRing = MesherUtils.GenVertexRing(meshTrunk, tip, point.Right, point.Down,
					0.0f, trunkParams.GetCount(point.Radius), trunkVertexColor, texCoord.Y, texCoord.X, surfaceNormal: true);

				MesherUtils.ConnectRingsFan(meshTrunk, tipRing, ring);

				if (!branch.EndsWithPolyplane)
				{
					GenLeaf(point, tip, meshLeaves, leafParams);
				}

				return;
			}

			int startingVertex = 0;

			// Is this the root branch?
			if(branch.Parent == null)
			{
				// ...yes, this is a root branch
				// Handle the first vertex specially
				startingVertex = 1;

				var first = branch.Vertices.First();

				previousRight = first.Right;
				previousDown = first.Down;

				previousRing = MesherUtils.GenVertexRing(meshTrunk, previousCenter, previousRight,
					previousDown, first.Radius, trunkParams.GetCount(first.Radius), trunkVertexColor, texCoord.Y, texCoord.X);
			}
			else
			{
				// ...no, common case, branch is growing out of another branch

				var first = branch.Vertices.First();

				previousRight = first.Right;
				previousDown = first.Down;

				previousRing = MesherUtils.GenVertexRing(meshTrunk, previousCenter, previousRight, previousDown,
					first.Radius, trunkParams.GetCount(first.Radius), trunkVertexColor, texCoord.Y, texCoord.X);
			}

			int vertexIndex = startingVertex;

			// Common case, vertex inside branch
			for(; vertexIndex < branch.Vertices.Count - 1; vertexIndex++)
			{
				var vertex = branch.Vertices[vertexIndex];

				advanceTexcoords(vertex);

				Vector3 center = previousCenter + vertex.Forward * vertex.Length;

				var newRing = MesherUtils.GenVertexRingAligned(meshTrunk, center, vertex.Right,
					vertex.Down, previousRight, previousCenter, vertex.Radius,
					trunkParams.GetCount(vertex.Radius), trunkVertexColor, texCoord.Y, texCoord.X);

				MesherUtils.ConnectRings(meshTrunk, newRing, previousRing);

				previousRing = newRing;

				previousRight = vertex.Right;
				previousDown = vertex.Down;
				previousCenter = center;
			}

			// End of branch - generate end point vertex and connect it
			{
				// Consider vertexIndex to be the last vertex, since we may abort the previous for loop early
				var last = branch.Vertices[vertexIndex];
				Vector3 tip = previousCenter + last.Forward * last.Length;

				advanceTexcoords(last);

				// Generate a almost zero-radius ring of vertices instead of a single vertex
				// to avoid ugly texture stretching artifacts
				// Radius is nonzero to allow computing proper tangents, and is negative to avoid creating a tiny hole in the tip
				// The ring normals will also point in the direction of the tip
				var tipRing = MesherUtils.GenVertexRing(meshTrunk, tip, last.Right, last.Down,
					0.0f, trunkParams.GetCount(branch.Vertices[vertexIndex - 1].Radius), trunkVertexColor, texCoord.Y, texCoord.X, surfaceNormal: true);

				MesherUtils.ConnectRingsFan(meshTrunk, tipRing, previousRing);

				if (!branch.EndsWithPolyplane)
				{
					GenLeaf(last, tip, meshLeaves, leafParams);
				}
			}
		}

		static void GenLeaf(TreeVertex tip, Vector3 tipPosition, Mesh mesh, MesherLeafParams leafParams)
		{
			float leafSize = (float)Math.Sqrt(leafParams.LeafAreaScale);

			int firstIndex = mesh.Vertices.Count;

			Vector3 up = -tip.Down;
			Vector3 left = -tip.Right;

			if (up.Y < 0)
			{
				up = -up;
				left = -left;
			}

			left *= (float)leafParams.LeafAspect;
			left *= 0.5f;

			Vector4 color = Vector4.One;

			mesh.Vertices.Add(new MeshVertex(tipPosition + left * leafSize, up, color,
				leafParams.LeafTexCoords[0].ToOpenTK(), Vector3.UnitX));
			mesh.Vertices.Add(new MeshVertex(tipPosition - left * leafSize, up, color,
				leafParams.LeafTexCoords[1].ToOpenTK(), Vector3.UnitX));
			mesh.Vertices.Add(new MeshVertex(tipPosition + tip.Forward * leafSize - left * leafSize, up, color,
				leafParams.LeafTexCoords[2].ToOpenTK(), Vector3.UnitX));
			mesh.Vertices.Add(new MeshVertex(tipPosition + tip.Forward * leafSize + left * leafSize, up, color,
				leafParams.LeafTexCoords[3].ToOpenTK(), Vector3.UnitX));

			mesh.Indices.Add(firstIndex + 0);
			mesh.Indices.Add(firstIndex + 1);
			mesh.Indices.Add(firstIndex + 2);
			mesh.Indices.Add(firstIndex + 0);
			mesh.Indices.Add(firstIndex + 2);
			mesh.Indices.Add(firstIndex + 3);
		}

		/// <summary>
		/// Smooths branch geometry using a spline
		/// </summary>
		/// <param name="branch">Branch whose vertices will be smoothed.</param>
		static void SmoothBranch(Branch branch)
		{
			if (branch.Vertices.Count < 1)
				return;

			var controlPoints = new Vector3[branch.Vertices.Count + 2];

			// Generate spline control points
			{
				Vector3 lastPos = Vector3.Zero;

				// Fake parent vertex
				controlPoints[0] = -branch.Vertices[0].Forward * branch.Vertices[0].Length;

				if (branch.Parent != null)
				{
					controlPoints[0] = -branch.Parent.Forward * branch.Parent.Length;
				}

				for (int i = 0; i <= branch.Vertices.Count; i++)
				{
					controlPoints[i + 1] = lastPos;

					if (i >= branch.Vertices.Count)
						break;

					lastPos += branch.Vertices[i].Forward * branch.Vertices[i].Length;
				}
			}
			
			var spline = new Spline(controlPoints);

			// Smooth branch vertices using the spline
			{
				var lastPos = Vector3.Zero;

				for (int i = 0; i < branch.Vertices.Count; i++)
				{
					var vert = branch.Vertices[i];

					float inSpline = (2 + i) / (float)(branch.Vertices.Count + 1); // +1s for the parent vertex included into spline, +2 because we query the next vertex

					Vector3 pos, dir;
					pos = spline.GetSplinePoint(inSpline, out dir).Xyz;

					Vector3 toNext = pos - lastPos;
					lastPos = pos;

					vert.Forward = toNext.Normalized();
					vert.Length = toNext.Length;
				}
			}
		}
	}
}
