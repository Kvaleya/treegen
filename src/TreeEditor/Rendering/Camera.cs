﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Immutable object describing a camera.
	/// Coordinate system:
	/// X: left
	/// Y: up
	/// Z: forward
	/// </summary>
	class Camera
	{
		public readonly Vector3 Position;
		public readonly Vector3 Forward;
		public readonly Vector3 Left;
		public readonly float HorizontalFov; // In degrees
		public readonly float Aspect; // width / height
		public readonly Vector3 Up;
		public readonly Vector3 Ray00;
		public readonly Vector3 Ray01;
		public readonly Vector3 Ray10;
		public readonly Vector3 Ray11;

		/// <summary>
		/// Creates a new camera object.
		/// </summary>
		/// <param name="position">Position of the camera</param>
		/// <param name="rotation">Orientation of the camera</param>
		/// <param name="hfov">Horizontal field of view in degrees</param>
		/// <param name="aspect">Image aspect ratio (width / height)</param>
		public Camera(Vector3 position, float hfov, float aspect, Quaternion rotation)
		{
			Position = position;
			HorizontalFov = hfov;
			Aspect = aspect;

			var m = Matrix3.CreateFromQuaternion(rotation);
			Left = m.Row0.Normalized();
			Up = m.Row1.Normalized();
			Forward = m.Row2.Normalized();

			Ray00 = GetRayDirection(new Vector2(-1, -1));
			Ray01 = GetRayDirection(new Vector2(-1, +1));
			Ray10 = GetRayDirection(new Vector2(+1, -1));
			Ray11 = GetRayDirection(new Vector2(+1, +1));
		}

		public Camera(Vector3 position, float hfov, float aspect, Vector3 forward,
			Vector3 left = default(Vector3), Vector3 up = default(Vector3))
		{
			Position = position;
			HorizontalFov = hfov;
			Aspect = aspect;

			if (forward == Vector3.UnitY)
				forward.Z = 0.0001f;

			Forward = forward.Normalized();
			
			if(left == default(Vector3))
			{
				left = Vector3.Cross(Vector3.UnitY, forward);
			}

			Left = left.Normalized();

			if(up == default(Vector3))
			{
				up = Vector3.Cross(Forward, Left);
			}

			Up = up.Normalized();

			Ray00 = GetRayDirection(new Vector2(-1, -1));
			Ray01 = GetRayDirection(new Vector2(-1, +1));
			Ray10 = GetRayDirection(new Vector2(+1, -1));
			Ray11 = GetRayDirection(new Vector2(+1, +1));
		}

		public Matrix4 GetCameraMatrix()
		{
			Matrix4 translation = Matrix4.Identity;
			translation.Row3 = new Vector4(-Position, 1);
			Matrix4 rotation = Matrix4.Identity;
			rotation.Column0 = new Vector4(-Left, 0);
			rotation.Column1 = new Vector4(Up, 0);
			rotation.Column2 = new Vector4(-Forward, 0);
			return translation * rotation;
		}

		public Matrix4 GetCameraProjectionMatrix()
		{
			var cam = GetCameraMatrix();

			var proj = GlobCore.Utils.GetProjectionPerspective(Aspect, HorizontalFov, 1f / 128, 256f);

			return cam * proj;
		}

		/// <summary>
		/// Returns a direction for a ray going through a specific point of the screen.
		/// </summary>
		/// <param name="normalizedCoords">Ray coordinates on screen in range -1..1, (-1, -1) being the lower right corner.</param>
		/// <returns>The ray direction scaled so that the vector's distance from the camera plane is one.</returns>
		Vector3 GetRayDirection(Vector2 normalizedCoords)
		{
			float dist = 1f / MathF.Tan(HorizontalFov / 180 * MathF.PI * 0.5f);
			Vector3 dir = Forward * dist - Left * normalizedCoords.X + Up * normalizedCoords.Y / Aspect;
			dir /= Vector3.Dot(Forward, dir);
			return dir;
		}
	}
}
