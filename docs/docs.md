# Tree editor

## Controls

* Spacebar: toggles between menus and free mouselook+movement
* W, A, S, D: classical FPS style movement
* E, Q: movement up and down
* Left shift (hold): faster movement
* Left alt (hold): slower movement

At the top of the screen there is a menu bar with file options, such as save, load, export, and tools menu, with mesh rebuilt button, wireframe toggle and mesher settings menu toggle. There is also a textures menu for loading textures and setting leaf texture coordinates.

Some of these options have a shortcut assigned to them, for example current tree mesh can be rebuilt using *Ctrl+R*.

There is also a large subwindow with tree generation settings. Modifying these settings will not trigger mesh rebuilt, one must use *Ctrl+R* to see their effects. To get an idea of how these values affect the tree, see the *Growth modules* section below.

## Code structure

The main class which connect everything together is the `Editor.cs`. It launches a OpenTK GameWindow and uses its internal render loop for periodic updates and rendering.

The `UserInterface` directory contains most of the UI-related code, mainly the `UserInterface/MenuGenerator.cs` class, which handles all the menus, including generating module menus at runtime using reflection. Custom attributes are used to control whether a growth module property appears as a slider or as a text field.

The `UserInterface/EditorCamera.cs` class handles first person movement. The `UserInterface/FilePicker.cs` class handles the file picking dialog menu. This class is mostly adapted from the user [prime31](https://www.gitmemory.com/issue/mellinoe/ImGui.NET/22/488557014) with some modifications, such as the addition of a popup dialog when overwriting an existing file.

The `TreeBatch.cs` class is the main interface with the TreeGen library, and also stores the current tree "project".

### Renderer

TreeEditor uses a very simple OpenGL forward renderer. Most of rendering related code is contained in the `Rendering` directory. `Rendering/ImguiRenderer.cs` class handles the rendering commands ImGui generates. This class is mostly adapted from an [ImGui.NET sample](https://github.com/mellinoe/ImGui.NET/blob/master/src/ImGui.NET.SampleProgram.XNA/ImGuiRenderer.cs).

The renderer uses a "module" based architecture adapted from my hobby engine. `Rendering/TonemapModule.cs` as well as the related shaders are also ports from the engine.

Shaders can be found in the `src/shaders` directory and can be edited at runtime.

### Libraries

Several third party libraries are used in TreeGen and TreeEditor. The most notable one is OpenTK 4, which primarily handles OpenGL bindings, input and vector math. ImGui.NET is used for C# binding for the ImGui library, which is a cross-platform, easy to use, immediate-mode GUI library that can be made to work on top of any renderer. Newtonsoft.Json is used for JSON serialization and deserialization.

Rendering is assisted by the GlobCore library. This is my personal helper library for OpenGL projects, ported to .NET Core, and is mostly irrelevant to the rest of the project. Its main features are Vulkan-inspired pipeline state objects for easier OpenGL state machine manipulation, texture objects and shaders that can be reloaded and recompiled at runtime.

# Treegen library

The main interface of this library is the `Growing/Grower.cs` class and growth modules for tree generation and `Mesh/Mesher.cs` class for mesh generation. For an usage example, see `TreeObject.cs` class of the editor or the [sample program](src/TreeSample/Program.cs).

## Code structure

The code is mostly divided into two directories, `Growing` for anything related to tree generation and `Mesh` for geometry generation.

Mesh related classes consist of a `Mesh.cs` class for storing vertices and indices, `MeshVertex.cs` struct which stores vertex data and is also indented to be used directly by the GPU, `ObjWriter.cs` which is used for exporting wavefront .obj and `Mesher.cs`, which turns a tree in abstract graph representation into an actual mesh.

The main tree generation class is the `Grower.cs`, which contains the logic that applies growth modules to tree vertices. Growth modules themselves are described below.

## Tree generation

The generated tree is represented by a graph of *tree vertices*. A tree vertex stores attributes used in generation, such as age or dominance, and geometric information, such as forward and right vectors, branch radius and length (distance from previous tree vertex).

The attributes are:

- *Age* - the amount of nutrients available, its value is `1` at tree root and near zero at branch tips. It is directly proportional to branch cross-section area.
- *Dominance* - how dominant a branch is. Its value is usually `1` for the main trunk and decreases with each branch fork. Dominant branches tend to grow straighter and more upwards. It could also be called "trunkness".
- *Stiffness* - how resistant the branch is to being affected by tropisms.
- *BranchHormone* - slowly accumulates, once it reaches a certain threshold a branching occurs (and the hormone is reset to zero in all child branches).
- *Roll* - in radians, helper value for controlling branch direction.
- *Gravitropism* - how much the branch grows towards or away from the ground.
- *LastBranchRadius* - helper value, used to smooth out sharp radius transition when branching.

Tree generation is done from the root up. Once a vertex is finished, it is not ever modified again, which allows for easy parallelization.

The generation itself is done by several *growth modules*. Modules have a defined order in which they are executed. A tree is fully defined by a seed, a list of growth modules and their parameters.

## Growth modules

### Root module

This module's only purpose is to set initial attributes of the root vertex to a user-specified value. Currently it only sets dominance.

### Gravitropism module

This module sets the (anti)gravitropism of each vertex.
Antigravitropism is used for dominant branches, gravitropism for non-dominant. Both can be configured in the editor.

### Geometry module

This module uses the age attribute to set the geometry attributes of the vertex, such as length and radius.
Radius is set to the square root of age multiplied by the `RadiusScale` parameter.
When a branch forks, its child branches will inherit its radius for the first few vertices to smooth the transition. This can be controlled with the `BranchRadiusTrailing` parameter.
Length is computed by first raising age to the power of `LengthPow` and then multiplying it with `LengthScale`.

### Branching module

This is the most important module, as it determines the shape of the tree.

To determine whether to create a new branch, the branch hormone value must pass a `HormoneThreshold`.
Each vertex adds a random amount to this value according to the range defined by `HormoneBuildUpMin` and `HormoneBuildUpMax`.

`TripleBranchProbability` controls the probability of a fork with three
outgoing branches (including the current branch) being generated instead of
a fork with two outgoing branches.

`BranchSlant` is used to control the angle between
the direction of original branch growth and the direction of child branches.
Note that the actual slant angle of each child branch is dependent
on their relative ages. Branches with higher age will grow straighter and others will be pushed away.

`BranchRoll` is used to rotate each new set of child branches
relative to the previous set.

`MaxLeafAge` controls when a branch is terminated with a leaf.
Once age drops below this threshold, the branch will stop growing and a leaf will be generated.
This also indirectly controls the number of leaves.

`OriginalBranchAge` controls how thick the primary branch is compared to its child branches. This value is randomized according to `OriginalBranchAgeDeviation`.

`DominanceInheritance` controls how much the dominance attribute decreases with each branching for child branches. It remains unchanged for the parent branch,

`DominanceForwardnessFactor` and `MainTrunkForwardnessFactor` controls how much dominant branches (or the trunk) tend to continue growing in their original direction instead of being pushed away by child branches.

`AgeSapping` determines how much nutrients the tree loses at each branch node. The value determines the ratio of how much nutrients survive, so `1` means no loss.

## Mesher

When turning a tree into a mesh the first step is to decompose the tree into *branches*, where each branch is a continuous path of tree vertices. Every branch is then turned into a "tube": a vertex ring is generated for each tree vertex of a branch and neighboring rings are connected with triangles. Every branch ends in a leaf card.
Vertex rings can vary in vertex count based on their radius.

Additionally, branches that fall below a certain radius threshold may be replaced with polyplanes - a plane with an image of the original branch.
For this purpose, all vertices where real branch geometry ends and polyplanes begin and first collected.
Then these vertices are sorted into clusters, where each cluster shares the same polyplane images.
Texture space from a single atlas is allocated for each polyplane image,
and then the meshes and render commands needed to produce the polyplane atlas are generated.
Finally, cards with texture coordiantes referring to the right place in the polyplane atlas are added to the proper locations in the tree model.

Multiple tree instances of the same species and level of detail may share a single polyplane atlas (and clusters).

## Randomness

To ensure consistent behavior across different platforms and framework versions, TreeGen uses a simple custom random generator adapted from <https://stackoverflow.com/a/17094254>.

Randomness is accessed mostly from `TreeRandomRepository.cs` class. A TreeRandomRepository is shared across all vertices in a single branch.
When a new branch splits off, a new instance of this class is created and seeded.
Each new branch will always be seeded in the same way no matter how the parent repository was used,
so this leads to consistent generation even if tree parameters change drastically.
There is also support for different random generators inside the repository that do not affect each other.

The purpose of this setup is so that small changes to tree parameters will not lead to unexpectedly different tree geometry.
For example if the parameter forthe  number of tree vertices between branchings changes, the overall shape of the tree remains mostly the same.
