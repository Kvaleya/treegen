﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using OpenTK.Windowing.GraphicsLibraryFramework;
using TreeEditor.Rendering;

namespace TreeEditor.UserInterface
{
	/// <summary>
	/// Holds the state of the editor camera and handles its movement.
	/// </summary>
	class EditorCamera
	{
		public Vector3 Position = new Vector3(0, 2, -8); // Starting position
		public float HorizontalFov = 90f; // In degrees
		public double Pitch = 0; // In radians
		public double Yaw = 0; // In radians

		// Positive yaw = looking right
		public double YawDegrees { get { return Yaw * 180 / Math.PI; } set { Yaw = value / 180 * Math.PI; } }
		// Positive pitch = looking up
		public double PitchDegrees { get { return Pitch * 180 / Math.PI; } set { Pitch = value / 180 * Math.PI; } }

		public Vector2 MouseSensitivity = new Vector2(0.3f);
		public float MoveSpeed = 8.0f;

		public Keys KeyForward = Keys.W, KeyBack = Keys.S, KeyLeft = Keys.A,
			KeyRight = Keys.D, KeyUp = Keys.E, KeyDown = Keys.Q,
			KeySprint = Keys.LeftShift, KeyWalk = Keys.LeftAlt;

		public Vector3 Forward
		{
			get
			{
				return new Vector3(
					(float)(Math.Sin(-Yaw) * Math.Cos(Pitch)),
					(float)(Math.Sin(Pitch)),
					(float)(Math.Cos(Yaw) * Math.Cos(Pitch))
				);
			}
		}

		public Vector3 Right { get { return Vector3.Cross(Forward, Vector3.UnitY).Normalized(); } }

		public Camera GetCamera(int width, int height)
		{
			return new Camera(Position, HorizontalFov, width / (float)height, Forward);
		}

		/// <summary>
		/// Handles camera controls
		/// </summary>
		public void UpdateControlsFPS(Input input)
		{
			// Camera look
			YawDegrees += input.MouseDelta.X * MouseSensitivity.X;
			PitchDegrees -= input.MouseDelta.Y * MouseSensitivity.Y;

			while (Yaw < 0)
				Yaw += Math.PI * 2;
			while (Yaw > Math.PI * 2)
				Yaw -= Math.PI * 2;

			double pitchLimit = Math.PI * 0.49;

			if (Pitch > pitchLimit)
				Pitch = pitchLimit;
			if (Pitch < -pitchLimit)
				Pitch = -pitchLimit;

			// Camera movement
			Vector3 move = Vector3.Zero;

			if (input.Keyboard.IsKeyDown(KeyForward))
				move.Z += 1;
			if (input.Keyboard.IsKeyDown(KeyBack))
				move.Z -= 1;
			if (input.Keyboard.IsKeyDown(KeyRight))
				move.X += 1;
			if (input.Keyboard.IsKeyDown(KeyLeft))
				move.X -= 1;
			if (input.Keyboard.IsKeyDown(KeyUp))
				move.Y += 1;
			if (input.Keyboard.IsKeyDown(KeyDown))
				move.Y -= 1;

			if (input.Keyboard.IsKeyDown(KeySprint))
				move *= 10;
			if (input.Keyboard.IsKeyDown(KeyWalk))
				move /= 10;

			move *= (float)input.TimeDelta * MoveSpeed;

			Position += Forward * move.Z;
			Position += Right * move.X;
			Position += Vector3.UnitY * move.Y;
		}
	}
}
