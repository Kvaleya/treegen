﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
	public class GrowParamSlider : Attribute
	{
		public double Min, Max;

		public GrowParamSlider(double min, double max)
		{
			Min = min;
			Max = max;
		}
	}
}
