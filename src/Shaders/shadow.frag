#version 430

#if defined(ALPHA_TEST)
layout(location = 1) in vec2 vTexCoord;

uniform sampler2D texAlpha;

void main()
{
	if(texture(texAlpha, vTexCoord).r < 0.5)
		discard;
}

#else
layout(early_fragment_tests) in;

void main() {}
#endif
