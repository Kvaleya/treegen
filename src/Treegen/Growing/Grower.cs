﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Treegen
{
	/// <summary>
	/// Handles high-level tree graph generation.
	/// </summary>
	public class Grower
	{
		/// <summary>
		/// The growth modules used by this grower
		/// </summary>
		public List<GrowModule> Modules { get; set; } = new List<GrowModule>();

		public delegate void OnError(string message);

		public void SortModules()
		{
			Modules.Sort();
		}

		/// <summary>
		/// Generates a tree from the input GrowContext
		/// </summary>
		/// <param name="context">Input GrowContext, mainly contains the tree root vertex.</param>
		/// <param name="onError">Callback used when an error occurs.</param>
		public void Grow(GrowContext context, OnError onError)
		{
			SortModules();

			ParallelProcessContext p = new ParallelProcessContext();

			ProcessParallel(context, context.Root, p);

			p.Done.Wait();
			p.Done.Dispose();

			if(p.Failed)
			{
				onError("Tree generation failed: too many nodes!");
			}
		}

		private class ParallelProcessContext
		{
			public int TaskCount = 1; // Set to one, because the initial call to ProcessParallel also decrements this
			public ManualResetEventSlim Done = new ManualResetEventSlim(false);
			public int MaxVertices = 1000000;
			public int CurrentVertices = 1;
			public bool Failed = false;
		}

		void ProcessParallel(GrowContext context, TreeVertex v, ParallelProcessContext p)
		{
			while(true)
			{
				for (int i = 0; i < Modules.Count; i++)
				{
					Modules[i].Apply(context, v);
				}

				var totalCount = Interlocked.Add(ref p.CurrentVertices, v.Children.Count);

				if(totalCount > p.MaxVertices)
				{
					p.Failed = true;
					break;
				}

				for (int i = 1; i < v.Children.Count; i++)
				{
					Interlocked.Increment(ref p.TaskCount);
					var child = v.Children[i];
					Task.Run(() =>
					{
						ProcessParallel(context, child, p);
					});
				}

				if(v.Children.Count > 0)
				{
					v = v.Children[0];
				}
				else
				{
					break;
				}
			}

			int result = Interlocked.Decrement(ref p.TaskCount);

			// Correct because:
			// if TaskCount were to be increased from 0, then a ProcessParallel task must still be running,
			// and this task would not yet decrement TaskCount, so TaskCount must not be zero
			if (result == 0)
			{
				p.Done.Set();
			}
		}

		/// <summary>
		/// Creates a Grower instance with all growth modules from the Treegen library added.
		/// Note that modules from other assemblies will not be included.
		/// </summary>
		public static Grower CreateWithAllModules()
		{
			var grower = new Grower();

			var moduleTypes = typeof(Grower).Assembly.GetTypes()
				.Where(t => typeof(GrowModule).IsAssignableFrom(t) && t != typeof(GrowModule));
			foreach (var type in moduleTypes)
			{
				grower.Modules.Add((GrowModule)Activator.CreateInstance(type));
			}

			return grower;
		}
	}
}
