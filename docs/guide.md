# Treegen user guide

## The first tree

When you first launch the tree editor, you will be greeted with this screen:

![](screen0.jpg)

Note the menu at the top of the screen and the large window on the left.

First, let's generate a tree. Press *Ctrl+R*, or go to "Tools" and press "Rebuild trees". This action may take a few seconds. Once it's done, you should see a tree or two:

![](screen1.jpg)

## Basic controls

Now, most of the tree is covered by the menu, and you can't see the top. Press *space* to switch between menu control and camera control mode.
When you are in camera control mode, you can fly around using classical FPS movement: **WASD**, **mouse** for looking around,
**E** and **Q** to go up or down. Hold **left shift** to go faster, or **left alt** to go slower.

Let's find a better camera angle:

![](screen2.jpg)

## Tuning growth parameters

Now, back to that huge window on the left part of the screen, with lots of sliders and text boxes.
That window is used to set parameters of the **tree growth**. In other words, use that window to edit your trees.

Let's make a change: set **Dominance** to around `0.5`. Nothing visibly changes - that is because you must trigger tree rebuild yourself.
Go ahead and press **Ctrl+R** to trigger the rebuild.

![](screen3.jpg)

The tree changed: it is more spread out, and the main trunk is not growing as straight.
*RootModule*, the parameter of which we just changed, sets the *Dominance* of the tree's root vertex.
Dominance determines how "trunk-like" a branch is. Trunk-like branches tend to grow straight
and are not displaced as much when they fork into more branches.
We just made the main tree branch less trunky.

For a description of all growth parameters please see the **[brief docs](docs/docs.md)**.

## Multiple tree instances

As you might have noticed, there are actually two trees in the default scene.
If you look closely at their branch structure, you might notice that they are actually very similar.

In the top menu, go to "Tools" and select "Tree instances":

![](screen4.jpg)

In this window you can see that there are two tree instances in the scene
at this time. Note that the instances share the same *seed*, but differ
in their *age*.

Tree generation is randomized, however each aspect of tree growth uses
its own deterministic random generator. This means that changing one parameter
of tree growth will only affect that one aspect of the tree: for example,
when we changed dominance, the overall branch structure remained the same,
only the branch directions changed.

*Seed* is used to initialize these random generators. You different seeds
to generate different randomized instances of the same tree species.

*Age* is a parameter that defines how many nutrients pass through a tree's
vertex. The more nutrient, the thicker the branch, and the more branches it can generate down the line.
Older trees have more time to grow and can pump more nutrients through their trunk, hence the parameter's name.
This menu sets the root's age.

Now you can see that the second tree instance is just a younger version of the first.

You can use this menu to set up what tree instances you generate.
If you add a new instance or change its parameters, don't forget to rebuild the trees to see the effects.

## Leaf setup

Now, let's have a close look at the tree's leaves.

The leaves are simple alpha-tested rectangles. You can set their textures
(and the trunk textures) using the "Textures" option in the top menu.

Go ahead and select "Textures" -> "Select leaf texcoords":

![](screen5.jpg)

This menu is used to set what portion of the original texture will be mapped
to the leaf cards. You can set the texcoord of the corners using the textboxes.

The red rectangle highlights the area that will be mapped to leaves.

The texture coordinates will always be mapped to a rectangle for simplicity.
Using other shapes will result in stretching.

The leaf origin will always be placed at the lower edge, and leaf will extend upwards.

## Level of detail - mesher settings

Now, let's look at how to set up level of detail. Go to "Tools" -> "Mesher settings",
and open both collapsed tabs:

![](screen6.jpg)

The "Common parameters" part sets parameters that affect all LODs. The
"TrunkTexcoordDelta" options control how much trunk texcoords change along the branch.

Trunk texture always wraps along the entire branch exactly once along its X axis.
Setting "TrunkTexcoordDeltaX" to a nonzero value will result in the trunk texture spiraling
along branches.

"TrunkTexcoordDeltaY" controls how much the texture moves in the Y coordinate with unit length.
Note that for thinner branches, trunk texture is repeated more often vertically in order
to avoid stretching.

"LeafAreaScale" sets approximate leaf area, and "LeafAspect" controls the width of a leaf.

Now, for the actual LOD parameters. You may define however many LODs you want.
In the default state, only one LOD is defined.

The first four parameters define how branch geometry will be generated.
It consists of vertex rings connected into tubes.
A vertex ring will have `VertexCountMax` vertices when the circumference of the branch
is at least `CircumferenceMax`, and will have `VertexCountMin` when circumference is no
more than `CircumferenceMin`. Vertex counts for in-between values are linearly interpolated.

## Level of detail - polyplanes

The other four parameters concern *polyplanes* - when a branch is entirely replaced
by a plane with an image of the original branch.

The `MinRadius` parameter controls when real branch geometry is cut off to be
replaced by a polyplane. One the radius of a branch falls below `MinRadius`, it is replaced by a polyplane.

Try setting this value to 0.01:

![](screen7.jpg)

Note that individual leaves are gone, they are replaced by images of small branches.

In order to better see the difference, you can turn on **wireframe** in the "Tools" tab.

There are hundreds of individual polyplanes in the tree now. Placing that many images
into a reasonably sized atlas is impossible while maintaining good resolution.
For this reason, similar polyplane branches are grouped into a *cluster*.
A single cluster will only use one set of polyplane images (a set consists of three actual images, one for each axis).

The `DesiredClusterCount` parameter controls how many clusters will be used.
Using more clusters leads to better representation of the original tree's shape, but lower the resolution of each polyplane.

The size of the polyplane atlas can be set using the last two parameters.

Note that each LOD will use a different polyplane atlas, but all tree instances of one LOD will share the same one.
This is because the most important parameter when clustering polyplanes is the branch radius (or *age*),
and that is also the parameter that affected by setting `MinRadius`.
Two different LOD would likely not share a single polyplane image, but any instances of the same LOD are very likely to have similar branches.

For this reason, for LODs that replace the entire tree with a polyplane,
set cluster count to the number of different instance ages you use.
Otherwise their least detailed LOD might use the same textures as visibly different (more mature) trees.

Lastly, you can inspect how many vertices and faces each LOD of each instance has by going to "Tools" -> "Mesh stats".

## Saving, loading, exporting

You can save your work using the "File" -> "Save" option.
This creates a small json file that stores the current growth parameters, texture filenames, mesher (and LOD) settings and tree instances.

You can load this file later, or you can load one of the demo trees.
Go ahead and load `demotree2.json`.
This will automatically build the loaded trees, so it might take a while.

![](screen8.jpg)

You can see an entire grid of trees. It consists of 5 different instances, each with 6 LODs.
Note that the least detailed LODs are just three planes with an image of the entire tree.

You can export all LODs of all instances (including the polyplane textures) by using this option in the "File" menu.
A zip file containing all meshes and textures will be generated.
Note that some meshes will be empty. If a tree uses polyplanes, it will not have any leaves, and vice versa.
Exporting many trees might take up to several minutes.

## Growth parameters

For details about the growth parameters, please refer to the brief docs or
the bachelor thesis linked from [readme](../README.md).
