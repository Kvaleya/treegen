#version 430

layout(location = 0) in vec3 vPos;
layout(location = 1) in vec2 vTexCoord;
layout(location = 2) in vec4 vColor;
layout(location = 3) in vec3 vNormal;
layout(location = 4) in vec3 vTangent;
layout(location = 5) in vec3 vBiTangent;

#ifdef GBUFFER
layout(location = 0) out vec4 outDiffuse;
layout(location = 1) out vec4 outNormal;
layout(location = 2) out vec4 outAlpha;
layout(location = 3) out vec4 outDepth;
#else
layout(location = 0) out vec4 outColor;
#endif

#ifndef ALPHA_TEST
layout(early_fragment_tests) in;
#endif

// Textures
layout(binding = 0) uniform sampler2D texColor;
layout(binding = 1) uniform sampler2D texNormal;
layout(binding = 2) uniform sampler2D texAlpha;

// Material
uniform vec4 materialColor;

// Lighting uniforms
uniform vec3 sunDirection;
uniform vec3 sunLight;

uniform float exposure;

const float gamma = 2.2;

#define PI 3.1415

void main() {
	//
	// Alpha test
	//
	
	#ifdef ALPHA_TEST
	if(texture(texAlpha, vTexCoord).r < 0.5)
		discard;
	#endif
	
	//
	// Get values needed for lighting
	//
	
	#ifdef NORMALMAP
	vec3 surfaceNormal = normalize(texture(texNormal, vTexCoord).xyz * 2.0 - 1.0);
	#else
	vec3 surfaceNormal = vec3(0, 0, 1);
	#endif
	
	//surfaceNormal = normalize(surfaceNormal);	
	
	// Also two-faced
	#ifdef ALPHA_TEST
	if(!gl_FrontFacing)
	{
		surfaceNormal.z *= -1;
	}
	#endif
	
	surfaceNormal.xyz = normalize(mat3(vTangent, vBiTangent, vNormal) * surfaceNormal.xyz);
	
	#ifdef ALPHA_TEST
	#ifdef GBUFFER
	if(!gl_FrontFacing)
	{
		//surfaceNormal.z *= 0.5;
	}
	#endif
	#endif
	
	#ifdef COLORMAP
	vec4 colorFromTex = texture(texColor, vTexCoord);
	
	// Convert to linear
	colorFromTex.rgb = pow(colorFromTex.rgb, vec3(gamma));
	
	//colorFromTex = vec4(1.0);
	vec3 surfaceColor = vColor.rgb * colorFromTex.rgb * materialColor.rgb;
	#else
	vec3 surfaceColor = vColor.rgb * materialColor.rgb;
	#endif
	
	#ifdef GBUFFER
	{
		outAlpha = vec4(1.0);
		outDiffuse = vec4(pow(surfaceColor, vec3(1.0 / gamma)), 1.0);
		outNormal = vec4(surfaceNormal * 0.5 + 0.5, 1.0);
	}
	#else
	{
		//
		// Lighting
		//
		
		#ifdef ALPHA_TEST
		// Fake subsurface scattering
		float ndotl = dot(surfaceNormal, sunDirection);
		vec3 finalColor = (max(ndotl, 0.0) + max(-ndotl * 0.3, 0.0)) * surfaceColor * sunLight / PI;
		#else
		vec3 finalColor = max(dot(surfaceNormal, sunDirection), 0.0) * surfaceColor * sunLight / PI;
		#endif
		
		// Fake amient
		finalColor += surfaceColor * vec3(0.5, 0.6, 0.8) * 0.5;
		
		//
		// Output
		//
		
		finalColor *= exposure;
		
		outColor = vec4(finalColor, 0.0);
		
		//outColor = vec4(fract(vTexCoord), 0.0, 1.0);
		//outColor = vec4(surfaceNormal * 0.5 + 0.5, 1.0);
	}
	#endif
}
