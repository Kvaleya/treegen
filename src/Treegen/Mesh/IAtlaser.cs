﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	interface IAtlaser
	{
		/// <summary>
		/// Fits rectangles into a rectangular area. May rescale the rectangles, but maintains their aspect ratios and relative size.
		/// </summary>
		/// <param name="targetArea">Size of target rectangular area</param>
		/// <param name="sizes">Sizes of individual rectanges.</param>
		/// <returns>List of (minX, minY, maxX, maxY) destinations of the input rectangles. Will fit into targetArea.</returns>
		public List<Vector4d> FitRectangles(Vector2d targetArea, List<Vector2d> sizes);
	}
}
