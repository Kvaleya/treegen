﻿using System;
using System.Linq;
using Treegen;

namespace TreeSample
{
	/// <summary>
	/// An example usage of the Treegen library.
	/// Polyplane generation is not included, please see TreeEditor/Rendering/PolyplaneRenderer.cs
	/// </summary>
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Generating a tree...");

			// Create an instance of Grower
			var grower = Grower.CreateWithAllModules();

			// We don't want to generate too many leaves (which we will ignore anyway), so we increase the MaxLeafAge parameter of BranchingModule
			var branchingModule = grower.Modules.SingleOrDefault(x => x.GetType() == typeof(BranchingModule)) as BranchingModule;
			if(branchingModule != null)
			{
				branchingModule.MaxLeafAge = 0.01f;
			}

			// Use a low age value in order to generate a small tree.
			var root = TreeVertex.CreateRoot(seed: 42, age: 0.1f);

			// Grow the tree
			grower.Grow(new GrowContext()
			{
				Root = root,
			}, (message) => Console.WriteLine(message));

			// Generate meshes with the default parameters
			// Only a single LOD level is defined by default.
			var result = Mesher.GenerateBatch(new MesherParams(), lodIndex: 0, root);

			// The mesher returns a collection of resulting meshes.
			// Since we have only supplied one root, it will only contain
			// a single set of meshes in our case.
			var trunk = result.Meshes[0].MeshTrunk;

			// Export the trunk mesh
			ObjWriter.ExportMesh(trunk, "sampleTree.obj");

			Console.WriteLine($"Done! It has {trunk.Vertices.Count} vertices!");
		}
	}
}
