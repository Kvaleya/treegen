﻿using System;

namespace Treegen
{
	public abstract class GrowModule : IComparable<GrowModule>
	{
		/// <summary>
		/// Modules with lower values of order get executed first
		/// </summary>
		public abstract double Order { get; }

		public GrowModule()
		{
		}

		/// <summary>
		/// Applies this module to a tree vertex
		/// </summary>
		public abstract void Apply(GrowContext context, TreeVertex vertex);

		public int CompareTo(GrowModule other)
		{
			return this.Order.CompareTo(other.Order);
		}
	}
}
