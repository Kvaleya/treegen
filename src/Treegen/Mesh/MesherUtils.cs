﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Stores the indices for a single vertex ring used when triangulating tube geometry for branches
	/// </summary>
	struct VertexRing
	{
		public int FirstVertex;
		public int Count; // First and last vertices are in the same position

		public static VertexRing Invalid = new VertexRing(-1, -1);

		public bool IsInvalid { get { return FirstVertex < 0 || Count < 0; } }

		public VertexRing(int firstVertex, int count)
		{
			FirstVertex = firstVertex;
			Count = count;
		}
	}

	static internal class MesherUtils
	{
		/// <summary>
		/// Generates a ring of vertices.
		/// </summary>
		/// <param name="mesh">Mesh into which the ring is generated</param>
		/// <param name="center">Center of the ring</param>
		/// <param name="right">Right vector, defines the ring plane together with down</param>
		/// <param name="down">Down vector, defines the ring plane together with right</param>
		/// <param name="radius">Ring radius</param>
		/// <param name="vertexCountParams">Describes how to calculate number of vertices in the ring</param>
		/// <param name="color">Color of vertices in the ring</param>
		/// <param name="texCoordY">Y component of texture coordinates, same for all vertices of the ring.</param>
		/// <param name="texCoordXOffset">Offset of the X component of texture coordinates. The entire texture is wrapped along the ring in the X direction. (Vertex texcoords go from 0 to 1 plus offset)</param>
		/// <param name="surfaceNormal">When true, the normals will point in the direction of the ring plane. When false, normals will lie in the ring plane, pointing away from the ring.</param>
		public static VertexRing GenVertexRing(Mesh mesh, Vector3 center, Vector3 right,
			Vector3 down, float radius, int vertexCount, Vector4 color, float texCoordY, float texCoordXOffset, bool surfaceNormal = false)
		{
			VertexRing ring = new VertexRing();
			ring.FirstVertex = mesh.Vertices.Count;
			ring.Count = vertexCount + 1; // One extra vertex to make uv wrap around the ring

			Vector3 planeNormal = Vector3.Normalize(Vector3.Cross(right, down));

			for (int i = 0; i <= vertexCount; i++)
			{
				var angle = i / (double)vertexCount * Math.PI * 2;
				float sin = (float)Math.Sin(angle);
				float cos = (float)Math.Cos(angle);

				Vector3 normal = planeNormal;
				if(!surfaceNormal)
				{
					normal = right * sin + down * cos;
					normal.Normalize();
				}

				Vector3 pos = center + normal * radius;

				float tcx = i / (float)vertexCount;
				mesh.Vertices.Add(new MeshVertex(pos, normal, color, new Vector2(texCoordXOffset + tcx, texCoordY), Vector3.Zero));
			}

			return ring;
		}

		/// <summary>
		/// Generates a ring of vertices aligned with the previous ring, eliminating ugly twists in the resulting geometry.
		/// See GenVertexRing for parameter details.
		/// </summary>
		public static VertexRing GenVertexRingAligned(Mesh mesh, Vector3 center, Vector3 right,
			Vector3 down, Vector3 previousRight, Vector3 previousCenter, float radius,
			int vertexCount, Vector4 color, float texCoordY, float texCoordXOffset)
		{
			Vector3 forward = Vector3.Cross(right, down).Normalized();

			float angle = 120;

			Vector3 direction = (center - previousCenter);
			//Vector3 prevVertex = -direction + previousRight;
			Vector3 prevVertex = previousRight;

			const int iterations = 20;

			// Find a rotation where edges between vertices of previous ring and current ring point in the same direction as the difference of the ring's origins
			for (int i = 0; i < iterations; i++)
			{
				Vector3 a = Quaternion.FromAxisAngle(forward, angle) * right;
				Vector3 b = Quaternion.FromAxisAngle(forward, -angle) * right;

				float dotLast = Vector3.Dot(previousRight, right);
				float dotA = Vector3.Dot(previousRight, a);
				float dotB = Vector3.Dot(previousRight, b);

				if (dotLast < dotA || dotLast < dotB)
				{
					if (dotA > dotB)
					{
						right = a;
					}
					else
					{
						right = b;
					}
				}

				angle /= 2;
			}

			right.Normalize();
			down = Vector3.Cross(forward, right).Normalized();

			return GenVertexRing(mesh, center, right, down, radius, vertexCount, color, texCoordY, texCoordXOffset);
		}

		/// <summary>
		/// Generates indices connecting a vertex ring to a single vertex.
		/// </summary>
		/// <param name="mesh">Mesh into which the indices are generated</param>
		/// <param name="ring">Vertex ring</param>
		/// <param name="point">Index of the single vertex the ring will be connected to</param>
		/// <param name="reverse">Flip triangle faces?</param>
		public static void ConnectRingToPoint(Mesh mesh, VertexRing ring, int point, bool reverse)
		{
			for (int i = 0; i < ring.Count - 1; i++)
			{
				int i1 = ring.FirstVertex + i;
				int i2 = ring.FirstVertex + ((i + 1) % ring.Count);
				int i3 = point;

				if (!reverse)
				{
					mesh.Indices.Add(i1);
					mesh.Indices.Add(i2);
					mesh.Indices.Add(i3);
				}
				else
				{
					mesh.Indices.Add(i3);
					mesh.Indices.Add(i2);
					mesh.Indices.Add(i1);
				}
			}
		}

		public static void ConnectRings(Mesh mesh, VertexRing first, VertexRing second)
		{
			if (first.Count != second.Count)
			{
				// Different vertex counts
				int lastLower = 0;
				int lastUpper = 0;

				while (lastLower < first.Count - 1 || lastUpper < second.Count - 1)
				{
					double distLower = lastLower / (double)(first.Count - 1);
					double distUpper = lastUpper / (double)(second.Count - 1);

					if (distLower < distUpper)
					{
						mesh.Indices.Add(first.FirstVertex + lastLower);
						mesh.Indices.Add(first.FirstVertex + lastLower + 1);
						mesh.Indices.Add(second.FirstVertex + lastUpper);
						lastLower++;
					}
					else
					{
						mesh.Indices.Add(first.FirstVertex + lastLower);
						mesh.Indices.Add(second.FirstVertex + lastUpper + 1);
						mesh.Indices.Add(second.FirstVertex + lastUpper);
						lastUpper++;
					}
				}
			}
			else
			{
				// Same vertex counts
				int count = first.Count;

				// Create triangles
				// Note that first and last vertices share the same position, but have different texcoords
				for (int i = 0; i < count - 1; i++)
				{
					int f1 = first.FirstVertex + i;
					int s1 = second.FirstVertex + i;
					int f2 = first.FirstVertex + (i + 1);
					int s2 = second.FirstVertex + (i + 1);

					mesh.Indices.Add(f1);
					mesh.Indices.Add(f2);
					mesh.Indices.Add(s1);

					mesh.Indices.Add(f2);
					mesh.Indices.Add(s2);
					mesh.Indices.Add(s1);
				}
			}
		}

		/// <summary>
		/// Connects two vertex rings with the same vertex count, but only creates a single triangle per rectangular segment, having two vertices at the bottom and one at the top.
		/// Intended for creating branch tips, where the top ring has zero radius.
		/// </summary>
		public static void ConnectRingsFan(Mesh mesh, VertexRing first, VertexRing second)
		{
			if (first.Count != second.Count)
			{
				throw new NotImplementedException("Only intended for creating branch tips where vertex counts are the same");
			}
			else
			{
				// Same vertex counts
				int offset = 0;
				int count = first.Count;

				// Create triangles
				// Note that first and last vertices share the same position
				for (int i = 0; i < count - 1; i++)
				{
					int f1 = first.FirstVertex + i;
					int s1 = second.FirstVertex + (i + offset) % count;
					int f2 = first.FirstVertex + (i + 1) % count;
					int s2 = second.FirstVertex + (i + offset + 1) % count;

					//mesh.Indices.Add(f1);
					//mesh.Indices.Add(f2);
					//mesh.Indices.Add(s1);

					mesh.Indices.Add(f2);
					mesh.Indices.Add(s2);
					mesh.Indices.Add(s1);
				}
			}
		}

		public static void GenerateTangents(Mesh mesh)
		{
			if (mesh.Indices.Count == 0)
				return;

			// First clear tangents to zeroes
			for (int i = 0; i < mesh.Vertices.Count; i++)
			{
				var v = mesh.Vertices[i];
				v.Tangent = Vector3.Zero;
				mesh.Vertices[i] = v;
			}

			// Compute tangents for each triangle
			MeshVertex v0, v1, v2;
			for (int i = 2; i < mesh.Indices.Count; i += 3)
			{
				int i0 = mesh.Indices[i - 2];
				int i1 = mesh.Indices[i - 1];
				int i2 = mesh.Indices[i - 0];
				v0 = mesh.Vertices[i0];
				v1 = mesh.Vertices[i1];
				v2 = mesh.Vertices[i2];

				Vector3 edge1 = v1.Position - v0.Position;
				Vector3 edge2 = v2.Position - v0.Position;

				float deltaU1 = v1.TexCoords.X - v0.TexCoords.X;
				float deltaV1 = v1.TexCoords.Y - v0.TexCoords.Y;
				float deltaU2 = v2.TexCoords.X - v0.TexCoords.X;
				float deltaV2 = v2.TexCoords.Y - v0.TexCoords.Y;

				float f = 1.0f / (deltaU1 * deltaV2 - deltaU2 * deltaV1);

				Vector3 tangent;

				tangent.X = f * (deltaV2 * edge1.X - deltaV1 * edge2.X);
				tangent.Y = f * (deltaV2 * edge1.Y - deltaV1 * edge2.Y);
				tangent.Z = f * (deltaV2 * edge1.Z - deltaV1 * edge2.Z);

				if (float.IsNaN(tangent.X) || float.IsNaN(tangent.Y) || float.IsNaN(tangent.Z))
				{
					tangent = new Vector3(1, 0, 0);
				}

				tangent.Normalize();

				v0.Tangent += tangent;
				v1.Tangent += tangent;
				v2.Tangent += tangent;

				mesh.Vertices[i0] = v0;
				mesh.Vertices[i1] = v1;
				mesh.Vertices[i2] = v2;
			}

			// Normalize tangents
			for (int i = 0; i < mesh.Vertices.Count; i++)
			{
				var v = mesh.Vertices[i];
				v.Tangent = v.Tangent.Normalized();
				mesh.Vertices[i] = v;
			}
		}

		/// <summary>
		/// Appends one mesh to another.
		/// </summary>
		/// <param name="baseMesh">Mesh that will be expanded.</param>
		/// <param name="toAppend">Mesh that will be added to the other one.</param>
		public static void AppendMesh(Mesh baseMesh, Mesh toAppend)
		{
			int originalVertexCount = baseMesh.Vertices.Count;

			for (int i = 0; i < toAppend.Vertices.Count; i++)
			{
				baseMesh.Vertices.Add(toAppend.Vertices[i]);
			}

			for (int i = 0; i < toAppend.Indices.Count; i++)
			{
				baseMesh.Indices.Add(originalVertexCount + toAppend.Indices[i]);
			}
		}

		/// <summary>
		/// Transforms the vertices of a mesh using a matrix, returns a new mesh with transformed vertices and a copy of the indices. Source mesh is left unchanged.
		/// </summary>
		/// <param name="m">Source mesh for the transform. This mesh will not be changed.</param>
		/// <param name="transform">Transform matrix. Should not contain scaling, since normals will be transformed by this matrix as well.</param>
		/// <returns></returns>
		public static Mesh TransformMesh(Mesh m, Matrix4 transform)
		{
			Mesh result = new Mesh();

			for (int i = 0; i < m.Vertices.Count; i++)
			{
				var v = m.Vertices[i];

				result.Vertices.Add(new MeshVertex(
					(transform * new Vector4(v.Position, 1)).Xyz,
					(transform * new Vector4(v.Normal, 0)).Xyz,
					v.Color,
					v.TexCoords,
					(transform * new Vector4(v.Tangent, 0)).Xyz
				));
			}

			for (int i = 0; i < m.Indices.Count; i++)
			{
				result.Indices.Add(m.Indices[i]);
			}

			return result;
		}
	}
}
