﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace TreeEditor.Rendering
{
	class Scene
	{
		// Camera
		public Camera Camera;

		// Lighting
		public float Exposure;
		public Vector3 SunLight;
		public Vector3 SunDirection;

		// Objects
		public List<SceneObject> WorldObjects;
	}
}
