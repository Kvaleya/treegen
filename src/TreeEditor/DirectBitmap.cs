﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.Runtime.InteropServices;
using OpenTK.Mathematics;

namespace TreeEditor
{
	[Flags]
	public enum BitmapChannel
	{
		Blue = 1,
		Green = 2,
		Red = 4,
		Alpha = 8,
	}

	// Adapted from: http://stackoverflow.com/questions/24701703/c-sharp-faster-alternatives-to-setpixel-and-getpixel-for-bitmaps-for-windows-f
	public class DirectBitmap : IDisposable
	{
		// This class deserves a refactor.
		// But don't fix it if it ain't broken...

		public Bitmap Bitmap { get; private set; }
		public byte[] Bits { get; private set; }	// BGRA
		public bool Disposed { get; private set; }
		public int Height { get; private set; }
		public int Width { get; private set; }
		public int PixelSize { get; private set; }

		protected GCHandle BitsHandle { get; private set; }

		public DirectBitmap(int width, int height)
		{
			Width = width;
			Height = height;
			PixelSize = 4;
			Bits = new byte[Width * Height * PixelSize];
			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(width, height, width * PixelSize, PixelFormat.Format32bppArgb, BitsHandle.AddrOfPinnedObject());
		}

		public DirectBitmap(Image image)
		{
			Width = image.Width;
			Height = image.Height;

			PixelSize = 4;

			Bits = new byte[Width * Height * PixelSize];

			Bitmap bm = new Bitmap(image);

			var data = bm.LockBits(new Rectangle(0, 0, bm.Width, bm.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);

			Marshal.Copy(data.Scan0, Bits, 0, Bits.Length);

			bm.UnlockBits(data);

			bm.Dispose();

			BitsHandle = GCHandle.Alloc(Bits, GCHandleType.Pinned);
			Bitmap = new Bitmap(Width, Height, Width * PixelSize, PixelFormat.Format32bppArgb, BitsHandle.AddrOfPinnedObject());
		}

		public Bitmap ToBitmap()
		{
			var bm = new Bitmap(Width, Height, PixelFormat.Format32bppArgb);
			for(int y = 0; y < Height; y++)
			{
				for(int x = 0; x < Width; x++)
				{
					bm.SetPixel(x, y, Bitmap.GetPixel(x, y));
				}
			}
			return bm;
		}

		public static DirectBitmap FromSingleValue(float x, float y, float z, float w)
		{
			return FromSingleValue(new Vector4(x, y, z, w));
		}

		public static DirectBitmap FromSingleValue(Vector4 value)
		{
			var dbm = new DirectBitmap(1, 1);
			dbm.ImageStore(0, 0, value);
			return dbm;
		}

		public void Dispose()
		{
			if(Disposed) return;
			Disposed = true;
			Bitmap.Dispose();
			BitsHandle.Free();
		}

		/// <summary>
		/// Combines the two bitmaps into one with per-channel mask
		/// </summary>
		/// <param name="a">The first bitmap to combine</param>
		/// <param name="b">The second bitmap to combine</param>
		/// <returns></returns>
		public static DirectBitmap Combine(DirectBitmap a, DirectBitmap b, BitmapChannel maskA, BitmapChannel maskB)
		{
			if(a.Width != b.Width || a.Height != b.Height)
				throw new Exception("The bitmaps must have same dimensions!");
			DirectBitmap final = new DirectBitmap(a.Width, a.Height);

			for (int y = 0; y < final.Height; y++)
			{
				for (int x = 0; x < final.Width; x++)
				{
					int i = (x + y * final.Width) * final.PixelSize;

					List<BitmapChannel> channels = new List<BitmapChannel>()
					{
						BitmapChannel.Blue,
						BitmapChannel.Green,
						BitmapChannel.Red,
						BitmapChannel.Alpha
					};

					foreach (BitmapChannel channel in channels)
					{
						int value = ((((int)maskA & (int)channel) != 0)
						? a.Bits[i + ChannelToBit(channel)]
						: (byte)0);
						value += ((((int)maskB & (int)channel) != 0)
							? b.Bits[i + ChannelToBit(channel)]
							: (byte)0);
						final.Bits[i + ChannelToBit(channel)] = (byte)Math.Min(Math.Max(value, 0), 255);
					}
				}
			}

			return final;
		}

		static int ChannelToBit(BitmapChannel channel)
		{
			int c = 0;
			switch (channel)
			{
				case BitmapChannel.Blue:
					{
						c = 0;
						break;
					}
				case BitmapChannel.Green:
					{
						c = 1;
						break;
					}
				case BitmapChannel.Red:
					{
						c = 2;
						break;
					}
				case BitmapChannel.Alpha:
					{
						c = 3;
						break;
					}
				default:
					{
						throw new Exception("Wrong channel value!");
					}
			}
			return c;
		}

		/// <summary>
		/// Sets the same value to the specified channel for all pixels of the bitmap
		/// </summary>
		/// <param name="channel">The channel to set the value to</param>
		/// <param name="value">The value</param>
		public void SetChannelValue(BitmapChannel channel, byte value)
		{
			int c = ChannelToBit(channel);

			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int i = (x + y * Width) * PixelSize + c;
					Bits[i] = value;
				}
			}
		}

		public void SwapChannels(BitmapChannel channelA, BitmapChannel channelB)
		{
			int a = ChannelToBit(channelA);
			int b = ChannelToBit(channelB);

			for (int y = 0; y < Height; y++)
			{
				for (int x = 0; x < Width; x++)
				{
					int i = (x + y * Width) * PixelSize;
					byte temp = Bits[i + a];
					Bits[i + a] = Bits[i + b];
					Bits[i + b] = temp;
				}
			}
		}

		public DirectBitmap GenerateMip()
		{
			int w = Width >> 1;
			int h = Height >> 1;

			DirectBitmap mip = new DirectBitmap(w, h);

			for (int y = 0; y < h; y++)
			{
				for (int x = 0; x < w; x++)
				{
					int s00 = ((x * 2) + (y * 2) * Width) * PixelSize;
					int s10 = ((x * 2 + 1) + (y * 2) * Width) * PixelSize;
					int s11 = ((x * 2 + 1) + (y * 2 + 1) * Width) * PixelSize;
					int s01 = ((x * 2) + (y * 2 + 1) * Width) * PixelSize;

					int[] data = new int[4];
					for (int i = 0; i < 4; i++)
					{
						data[i] += Bits[s00 + i];
						data[i] += Bits[s10 + i];
						data[i] += Bits[s11 + i];
						data[i] += Bits[s01 + i];
						data[i] /= 4;
					}
					for (int i = 0; i < 4; i++)
					{
						mip.Bits[(x + y * mip.Width) * PixelSize + i] = (byte) data[i];
					}
				}
			}

			return mip;
		}

		public Vector4 TexelFetch(int x, int y)
		{
			x = Math.Min(Math.Max(x, 0), Width - 1);
			y = Math.Min(Math.Max(y, 0), Height - 1);

			int i = (y * Width + x) * 4;
			Vector4 value = new Vector4(Bits[i + 2], Bits[i + 1], Bits[i + 0], Bits[i + 3]);
			return value / 255f;
		}

		public Vector4 Sample(Vector2d texcoord)
		{
			Vector2d texel = Vector2d.Clamp(texcoord * new Vector2d(Width, Height) - new Vector2d(0.5f), Vector2d.Zero, new Vector2d(Width - 1, Height - 1));
			int x = (int)texel.X;
			int y = (int)texel.Y;

			Vector2 fract = new Vector2((float)(texel.X - x), (float)(texel.Y - y));

			return Vector4.Lerp(Vector4.Lerp(TexelFetch(x, y), TexelFetch(x + 1, y), fract.X),
				Vector4.Lerp(TexelFetch(x, y + 1), TexelFetch(x + 1, y + 1), fract.X), fract.Y);
		}

		public void ImageStore(int x, int y, Vector4 value)
		{
			int i = (y * Width + x) * 4;
			value = Vector4.Clamp(value, Vector4.Zero, Vector4.One);
			Bits[i + 2] = (byte)(value.X * 255);
			Bits[i + 1] = (byte)(value.Y * 255);
			Bits[i + 0] = (byte)(value.Z * 255);
			Bits[i + 3] = (byte)(value.W * 255);
		}
	}
}
