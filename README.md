# Tree generator

Made by Jakub Pelc as a bachelor thesis at Faculty of Mathematics and Physics, Charles University.
I would like to thank Jan Hovora for his consultation and guidance of this project,
and Josef Pelikán for his supervision.

## Quick links

If you wish to make **trees with LODs**, see the [user tutorial](docs/guide.md).
If you want a **quick overview** of how the generator works, see the [brief docs](docs/docs.md).
If you would like to **integrate the generator** library into your project,
see the [sample integration](src/TreeSample/Program.cs).
And if you want to read an **in-depth explanation** of everything, see the bachelor thesis (coming soon).

## Compiling

The program is written with VS2019, so that should be able to compile it without problems.
Note that the final executable must be run in the **root directory of the repository**.
It will look for shaders in `src/Shaders`, etc.

Compiling and starting it through Visual Studio should automatically run it in the proper directory.
If not, then either change where VS runs the compiled executable (go to project properties -> Debug -> Working directory) or copy the compiled .exe, all .dll and .json files and the `runtimes` directory into the root directory of the repository and run it there.

![](docs/screen3.jpg)
