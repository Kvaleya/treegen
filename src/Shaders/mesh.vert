#version 430

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec4 vertexColor;
layout(location = 3) in vec2 vertexTexCoord;
layout(location = 4) in vec3 vertexTangent;

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec3 vPos;
layout(location = 1) out vec2 vTexCoord;
layout(location = 2) out vec4 vColor;
layout(location = 3) out vec3 vNormal;
layout(location = 4) out vec3 vTangent;
layout(location = 5) out vec3 vBiTangent;

// On Nvidia Pascal, normalizing a zero vector results in NaNs
// On AMD GCN 1.0 this results in a zero vector, as does this function
// Only use this normalize variant when completely necesary
vec3 safeNormalize(vec3 v)
{
	float l = dot(v, v);
	return l > 0 ? v * inversesqrt(l) : v;
}

uniform mat4 meshTransform;
uniform mat4 projection;

void main() {
	vec3 n = safeNormalize((meshTransform * vec4(vertexNormal, 0.0)).xyz);
	vec3 t = safeNormalize((meshTransform * vec4(vertexTangent, 0.0)).xyz);
	t = safeNormalize(t - dot(t, n) * n);
	vTangent = t;
	vNormal = n;
	vBiTangent = safeNormalize(cross(t, n));
	vColor = vertexColor;
	
	vec3 pos = vertexPosition;
	pos = (meshTransform * vec4(pos, 1.0)).xyz;
	vPos = pos;
	
	vTexCoord = vertexTexCoord;
	
    gl_Position = projection * vec4(pos, 1.0);
}
