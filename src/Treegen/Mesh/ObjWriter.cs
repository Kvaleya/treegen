﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Globalization;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// A simple Wavefront .obj writer
	/// </summary>
	public static class ObjWriter
	{
		static string VecToString(Vector2 v)
		{
			return v.X.ToString(CultureInfo.InvariantCulture)
				+ " " + v.Y.ToString(CultureInfo.InvariantCulture);
		}

		static string VecToString(Vector3 v)
		{
			return v.X.ToString(CultureInfo.InvariantCulture)
				+ " " + v.Y.ToString(CultureInfo.InvariantCulture)
				+ " " + v.Z.ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Writes a Wavefront .obj of a mesh.
		/// </summary>
		/// <param name="writer">Text writer that will be used to write the obj.</param>
		/// <param name="mesh">Mesh that will be exported to obj.</param>
		/// <param name="preExistingVertexCount">Number of vertices already printed into the obj. Assumes that every vertex prints its position, normal and texcoord.</param>
		/// <param name="flipTexcoords">When true, flips the texcoord's Y axis.</param>
		public static void WriteObj(TextWriter writer, Mesh mesh, int preExistingVertexCount = 0, bool flipTexcoords = true)
		{
			for(int i = 0; i < mesh.Vertices.Count; i++)
			{
				var v = mesh.Vertices[i];
				writer.WriteLine("v " + VecToString(v.Position));
				writer.WriteLine("vn " + VecToString(v.Normal));

				if(flipTexcoords)
					writer.WriteLine("vt " + VecToString(new Vector2(v.TexCoords.X, 1f - v.TexCoords.Y)));
				else
					writer.WriteLine("vt " + VecToString(v.TexCoords));
			}

			for(int i = 0; i+2 < mesh.Indices.Count; i += 3)
			{
				var e0 = mesh.Indices[i + 0] + 1 + preExistingVertexCount;
				var e1 = mesh.Indices[i + 1] + 1 + preExistingVertexCount;
				var e2 = mesh.Indices[i + 2] + 1 + preExistingVertexCount;
				writer.WriteLine($"f {e0}/{e0}/{e0} {e1}/{e1}/{e1} {e2}/{e2}/{e2}");
			}
		}

		/// <summary>
		/// Exports a mesh into a Wavefront .obj file.
		/// </summary>
		/// <param name="mesh">Mesh that will be exported to obj.</param>
		/// <param name="filename">File that will be created.</param>
		/// <param name="flipTexCoords">When true, flips the texcoord's Y axis.</param>
		public static void ExportMesh(Mesh mesh, string filename, bool flipTexCoords = true)
		{
			using(StreamWriter sw = new StreamWriter(filename))
			{
				WriteObj(sw, mesh, flipTexcoords: flipTexCoords);
			}
		}

		/// <summary>
		/// Exports a mesh into a Wavefront .obj file.
		/// </summary>
		/// <param name="mesh">Mesh that will be exported to obj.</param>
		/// <param name="stream">Stream into which the .obj will be written.</param>
		/// <param name="flipTexCoords">When true, flips the texcoord's Y axis.</param>
		public static void ExportMesh(Mesh mesh, Stream stream, bool flipTexCoords = true)
		{
			using (StreamWriter sw = new StreamWriter(stream))
			{
				WriteObj(sw, mesh, flipTexcoords: flipTexCoords);
			}
		}
	}
}
