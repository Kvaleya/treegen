﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Stores parameters affecting mesh generation and level of detail, including the default parameters.
	/// </summary>
	public class MesherParams
	{
		/// <summary>
		/// List of geometry settings for different LODs. Lower indices are more detailed.
		/// </summary>
		public List<MesherLodParams> LodParams = new List<MesherLodParams>() { new MesherLodParams() };

		/// <summary>
		/// Leaf parameters
		/// </summary>
		public MesherLeafParams LeafParams = new MesherLeafParams();

		/// <summary>
		/// Controls how much should trunk texture coordinates change with unit branch length. When X is nonzero, the texture will wrap around the trunk in a spiral manner.
		/// The actual change of Y texcoord is also divided by branch radius.
		/// </summary>
		[JsonIgnore]
		public Vector2 TrunkTexCoordDelta = new Vector2(0.25f, 0.125f);

		// Hack to get my imgui menu generator to work with vec2
		public float TrunkTexCoordDeltaX
		{
			get
			{
				return TrunkTexCoordDelta.X;
			}
			set
			{
				TrunkTexCoordDelta.X = value;
			}
		}

		public float TrunkTexCoordDeltaY
		{
			get
			{
				return TrunkTexCoordDelta.Y;
			}
			set
			{
				TrunkTexCoordDelta.Y = value;
			}
		}
	}

	/// <summary>
	/// Parameters for generating a single level of detail, includes trunk mesh parameters, polyplane min radius and polyplane atlas size.
	/// </summary>
	public class MesherLodParams
	{
		/// <summary>
		/// Trunk parameters used when generating main mesh
		/// </summary>
		public MesherTrunkParams TrunkParams { get; set; } = new MesherTrunkParams();

		/// <summary>
		/// If branch radius falls below this value, the branch mesh will be ended with a tip and the rest of the branch will be replaced with a polyplane.
		/// </summary>
		public float MinRadius { get; set; } = -0.01f;

		/// <summary>
		/// Polyplane cluster count. Actual count may be lower.
		/// Instead of generating different polyplane textures for each branch, branches are assigned to a smaller number clusters that share
		/// similar growth parameters and also share polyplane textures.
		/// </summary>
		public int DesiredClusterCount { get; set; } = 12;

		/// <summary>
		/// Dimensions of the resulting polyplane atlas, in texels
		/// </summary>
		[JsonIgnore]
		public Vector2i PolyplaneAtlasSize = new Vector2i(4096, 4096);

		/// <summary>
		/// Creates a deep copy.
		/// </summary>
		public MesherLodParams Clone()
		{
			return new MesherLodParams()
			{
				TrunkParams = TrunkParams.Clone(),
				MinRadius = MinRadius,
				DesiredClusterCount = DesiredClusterCount,
				PolyplaneAtlasSize = PolyplaneAtlasSize,
			};
		}

		// Hack to get my imgui menu generator to work with vec2
		public int PolyplaneAtlasSizeX
		{
			get
			{
				return PolyplaneAtlasSize.X;
			}
			set
			{
				PolyplaneAtlasSize.X = value;
			}
		}

		public int PolyplaneAtlasSizeY
		{
			get
			{
				return PolyplaneAtlasSize.Y;
			}
			set
			{
				PolyplaneAtlasSize.Y = value;
			}
		}
	}

	public class MesherTrunkParams
	{
		/// <summary>
		/// Maximum vertex count of rings that make up the trunk tube geometry
		/// </summary>
		public int VertexCountMax { get; set; } = 12;
		/// <summary>
		/// Minimum vertex count of rings that make up the trunk tube geometry
		/// </summary>
		public int VertexCountMin { get; set; } = 3;
		/// <summary>
		/// Rings at or above this curcumference will use the maximum vertex count. Rings between maximal and minimal circumference will use linearly interpolated number of vertices.
		/// </summary>
		public double CircumferenceMax { get; set; } = 0.8f;
		/// <summary>
		/// Rings at or below this curcumference will use the minimum vertex count. Rings between maximal and minimal circumference will use linearly interpolated number of vertices.
		/// </summary>
		public double CircumferenceMin { get; set; } = 0.0001f;

		/// <summary>
		/// Computes the vertex count for a ring of certain radius.
		/// </summary>
		/// <param name="radius">The ring radius</param>
		/// <returns>Vertex count</returns>
		internal int GetCount(double radius)
		{
			double circumference = radius * Math.PI * 2;
			double vertexCount = Math.Min(Math.Max(circumference - CircumferenceMin, 0) / (CircumferenceMax - CircumferenceMin), 1);
			vertexCount = vertexCount * (VertexCountMax - VertexCountMin);
			return VertexCountMin + (int)vertexCount;
		}

		/// <summary>
		/// Creates a deep copy.
		/// </summary>
		public MesherTrunkParams Clone()
		{
			return new MesherTrunkParams()
			{
				VertexCountMax = VertexCountMax,
				VertexCountMin = VertexCountMin,
				CircumferenceMax = CircumferenceMax,
				CircumferenceMin = CircumferenceMin,
			};
		}
	}

	public class MesherLeafParams
	{
		/// <summary>
		/// Leaf dimensions are computed to match its desired area, which is in turn determined by the "age" (nutrients) the leaf provides, multiplied by this value.
		/// </summary>
		public double LeafAreaScale { get; set; } = 0.025f;

		/// <summary>
		/// Ratio of leaf width to leaf length (length = distance from tip to origin)
		/// </summary>
		public double LeafAspect { get; set; } = 0.75f;

		/// <summary>
		/// Texture coordinates to use on the leaf card.
		/// When looking at the leaf in such a way that the origin is at the bottom and tip is at the top, the order of texcoords is: 
		/// - bottom left
		/// - bottom right
		/// - top right
		/// - top left
		/// </summary>
		public JsonableVector2[] LeafTexCoords { get; set; } = new JsonableVector2[4]
		{
			new JsonableVector2(0.05f, 0.96f),
			new JsonableVector2(0.33f, 0.96f),
			new JsonableVector2(0.33f, 0.5f),
			new JsonableVector2(0.05f, 0.5f),
		};
	}

	public class JsonableVector2
	{
		public float X { get; set; }
		public float Y { get; set; }

		public JsonableVector2()
		{
		}

		public JsonableVector2(float x, float y)
		{
			X = x;
			Y = y;
		}

		public System.Numerics.Vector2 ToNumerics()
		{
			return new System.Numerics.Vector2(X, Y);
		}

		public OpenTK.Mathematics.Vector2 ToOpenTK()
		{
			return new Vector2(X, Y);
		}
	}
}
