﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	interface IClusterGen
	{
		/// <summary>
		/// Gets a list of tree vertices, and attempts to reduce them into a specified lesser number of vertices ("clusters"), so that the cluster assigned to every vertex is as similar to the original vertex as possible.
		/// </summary>
		/// <param name="vertices">List of input vertices</param>
		/// <param name="desiredClusterCount">Desired number of output vertices (clusters). Actual number of generated clusters may be lower.</param>
		/// <param name="clusterMap">Dictionary mapping origina vertex -> cluster vertex</param>
		/// <param name="resultVertices">List of resulting cluster vertices</param>
		public void GenerateClusters(List<TreeVertex> vertices, int desiredClusterCount, Dictionary<TreeVertex, TreeVertex> clusterMap, List<TreeVertex> resultVertices);
	}
}
