﻿using System;
using System.Collections.Generic;
using System.Text;
using Treegen;

namespace Treegen
{
	/// <summary>
	/// Creates new branches
	/// Assumes that the values in current vertex are final (so any modules that modify, for instance, age, should already be completed)
	/// Should be the very last executed module!
	/// </summary>
	public class BranchingModule : GrowModule
	{
		public override double Order => 100.0;

		[GrowParamSlider(1, 20)]
		public int HormoneBuildUpMin { get; set; } = 1; // Inclusive

		[GrowParamSlider(1, 20)]
		public int HormoneBuildUpMax { get; set; } = 10; // Inclusive

		[GrowParamSlider(1, 100)]
		public int HormoneThreshold { get; set; } = 30; // Any value including and over this will produce branches

		[GrowParamSlider(0, 1)]
		public float TripleBranchProbability { get; set; } = 0.9f;

		[GrowParamSlider(0, 90)]
		public float BranchSlant { get; set; } = 60f;

		[GrowParamSlider(0, 180)]
		public float BranchRoll { get; set; } = 120f;

		[GrowParamSlider(0.0001, 0.02)]
		public float MaxLeafAge { get; set; } = 0.005f;

		[GrowParamSlider(0.01, 0.99)]
		public float OriginalBranchAge { get; set; } = 0.5f;

		[GrowParamSlider(0.01, 0.5)]
		public float OriginalBranchAgeDeviation { get; set; } = 0.1f;

		[GrowParamSlider(0.001, 1.0)]
		public float DominanceInheritance { get; set; } = 0.5f;

		[GrowParamSlider(0.1, 5.0)]
		public float DominanceForwardnessFactor { get; set; } = 1.0f;

		[GrowParamSlider(0.1, 10.0)]
		public float MainTrunkForwardnessFactor { get; set; } = 5.0f;

		// TODO: place this elsewhere
		[GrowParamSlider(0.9, 1.0)]
		public float AgeSapping { get; set; } = 0.99f;

		public override void Apply(GrowContext context, TreeVertex vertex)
		{
			// Add hormone
			vertex[TreeAttribute.BranchHormone] += vertex.Random.Next(TreeRandomType.BranchHormone, HormoneBuildUpMin, HormoneBuildUpMax + 1);
			// Sap age
			vertex[TreeAttribute.Age] *= AgeSapping;

			if (vertex[TreeAttribute.Age] < MaxLeafAge * MaxLeafAge)
				return;
			
			vertex.Children.Add(vertex.CreateCurrentBranch());

			// Create new branches if hormone amount is sufficient
			if(vertex[TreeAttribute.BranchHormone] >= HormoneThreshold)
			{
				CreateBranches(context, vertex);
			}

			CalcStiffness(vertex);
		}

		void CreateBranches(GrowContext context, TreeVertex vertex)
		{
			int localRandomSeed = vertex.Random.Next(TreeRandomType.BranchType);

			// Ensure that the behaviour of branches stays the same even if the number of branches before this branching changes
			SimpleRandom random = new SimpleRandom(localRandomSeed);

			// Determine number of new branches (including the default one)
			int branchCount = 1 + (random.Attempt(TripleBranchProbability) ? 2 : 1);

			// Create vertices for new children
			for (int i = 1; i < branchCount; i++)
			{
				vertex.Children.Add(vertex.CreateNewBranch());
			}

			// Clear hormone amounts for children
			foreach (var child in vertex.Children)
			{
				child[TreeAttribute.BranchHormone] = 0;
				
				// Radius is already final since GeometryModule should already be executed
				child[TreeAttribute.LastBranchRadius] = vertex.Radius;
			}

			// Determine age ratios of new branches
			float[] ages = new float[branchCount];

			ages[0] = Math.Min(OriginalBranchAge * (float)((random.NextDouble() * 2.0 - 1.0) * OriginalBranchAgeDeviation + 1.0), 1);

			// Generate and normalize age ratios for non-main branches
			float sumNonMain = 0;
			for (int i = 1; i < branchCount; i++)
			{
				ages[i] = ((float)random.NextDouble() * 0.5f + 0.5f);
				sumNonMain += ages[i];
			}
			for (int i = 1; i < branchCount; i++)
			{
				ages[i] = ages[i] / sumNonMain * (1 - ages[0]);
			}

			// Apply the age ratios and dominance
			for (int i = 0; i < branchCount; i++)
			{
				vertex.Children[i][TreeAttribute.Age] = vertex[TreeAttribute.Age] * ages[i];
				vertex.Children[i][TreeAttribute.Dominance] = vertex[TreeAttribute.Dominance] * DominanceInheritance;
			}

			// Keep same dominance for main branch
			vertex.Children[0][TreeAttribute.Dominance] = vertex[TreeAttribute.Dominance];

			DirectBranches(context, vertex);
		}

		// Manages branch directions
		void DirectBranches(GrowContext context, TreeVertex vertex)
		{
			double localBranchRoll = BranchRoll / 180 * Math.PI;

			// Apply branch rotations
			for (int i = 0; i < vertex.Children.Count; i++)
			{
				var c = vertex.Children[i];
				c[TreeAttribute.Roll] += (float)(Math.PI * 2 / vertex.Children.Count * i + localBranchRoll);
				c.Slant((float)(Math.PI / 180 * BranchSlant * (1 - vertex.Children[i][TreeAttribute.Age] / vertex[TreeAttribute.Age])));
				float forwardness = 0f;
				forwardness += c[TreeAttribute.Dominance] > 0.99f ? MainTrunkForwardnessFactor : 0; // TODO: this should be antigravitropism, not forwardness
				forwardness += c[TreeAttribute.Dominance] * DominanceForwardnessFactor;
				c.SetOrientation(c.Forward + vertex.Forward * forwardness, c.Right);
			}

			// Orient child branches up
			for (int i = 1; i < vertex.Children.Count; i++)
			{
				vertex.Children[i].OrientUp();
			}
		}

		void CalcStiffness(TreeVertex vertex)
		{
			for(int i = 0; i < vertex.Children.Count; i++)
			{
				var v = vertex.Children[i];
				v[TreeAttribute.Stiffness] = v[TreeAttribute.Age] * v[TreeAttribute.Dominance];
			}
		}
	}
}
