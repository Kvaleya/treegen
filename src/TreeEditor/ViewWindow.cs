﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Text;
using GlobCore;
using ImGuiNET;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Common;
using OpenTK.Windowing.Desktop;

namespace TreeEditor
{
	/// <summary>
	/// An OpenTK game window, handling window creation and delegating update and render events to the editor.
	/// </summary>
	class ViewWindow : GameWindow
	{
		public const string ShadersPath = "src/Shaders";

		public delegate void DrawFrame();
		public delegate void Resize(int width, int height);

		public Device Device { get; private set; }

		private DrawFrame _onDrawFrame;
		private Resize _onResize;
		private TextOutput _textOutput;

		public ViewWindow(DrawFrame onDrawFrame, Resize onResize, TextOutput textOutput)
			: base(GameWindowSettings.Default, new NativeWindowSettings()
			{
				API = ContextAPI.OpenGL,
				Flags = ContextFlags.Debug,
				APIVersion = new Version(4, 5)
			})
		{
			_onDrawFrame = onDrawFrame;
			_onResize = onResize;
			_textOutput = textOutput;

			this.Title = "Tree editor";

			string shadersPath = Path.Combine(Environment.CurrentDirectory, ShadersPath);

			Device = new Device((type, message) =>
			{
				if (type != OutputTypeGlob.LogOnly)
					_textOutput.OnMessage(type.ToString() + ": " + message); // TODO: multiple message types from glob
			}, filename =>
			{
				const int tries = 10;

				for (int i = 0; i < tries; i++)
				{
					try
					{
						return File.OpenRead(Path.Combine(shadersPath, filename));
					}
					catch (IOException e)
					{
						if (i == tries - 1)
							throw e;
						// File is probably in use by another process - the shader editor
						// wait for it to finish saving the file
						Thread.Sleep(50);
					}
				}

				return null;
			});
			DebugMessageManager.SetupDebugCallback(Device);
			Device.StartShaderFileWatcher(shadersPath);
			//GL.ClipControl(ClipOrigin.LowerLeft, ClipDepthMode.ZeroToOne);
			GL.Enable(EnableCap.DepthTest);
			GL.Enable(EnableCap.StencilTest);
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			Device.Update();
			Device.Invalidate();

			GL.Viewport(0, 0, this.Size.X, this.Size.Y);
			GL.ClearColor(Color.Black);
			GL.ClearDepth(1);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			_onDrawFrame?.Invoke();

			SwapBuffers();
		}

		protected override void OnResize(ResizeEventArgs e)
		{
			base.OnResize(e);
			_onResize.Invoke(e.Width, e.Height);
		}
	}
}
