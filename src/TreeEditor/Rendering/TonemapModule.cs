﻿using GlobCore;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Handles tonemapping and gamma correction.
	/// </summary>
	class TonemapModule : RenderModule
	{
		GraphicsPipeline _psoTonemap;
		GraphicsPipeline _psoNoTonemap;

		public TonemapModule(RenderContext context)
			: base(context)
		{
			_psoTonemap = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("tonemap.frag"), null, new RasterizerState(), new DepthState());
			_psoNoTonemap = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("tonemap.frag", new List<Tuple<string, string>>()
				{
					new Tuple<string, string>("NO_TONEMAP_GAMMA", ""),
				}), null, new RasterizerState(), new DepthState());
		}

		public void Apply(Texture2D scene, bool doTonemap)
		{
			if (doTonemap)
				Device.BindPipeline(_psoTonemap);
			else
				Device.BindPipeline(_psoNoTonemap);

			Device.BindTexture(scene, 0);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
