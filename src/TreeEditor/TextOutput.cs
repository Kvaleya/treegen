﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TreeEditor
{
	// All messages will go through this class for possible future logging
	class TextOutput
	{
		public void OnError(string message)
		{
			Console.WriteLine(message);
		}

		public void OnMessage(string message)
		{
			Console.WriteLine(message);
		}
	}
}
