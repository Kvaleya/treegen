﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GlobCore;
using ImGuiNET;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;
using OpenTK.Windowing.Desktop;
using OpenTK.Windowing.GraphicsLibraryFramework;

namespace TreeEditor.Rendering
{
    // Adapted from: https://github.com/mellinoe/ImGui.NET/blob/master/src/ImGui.NET.SampleProgram.XNA/ImGuiRenderer.cs
    public class ImGuiRenderer
    {
	    // Graphics
	    private const int VertexSize = 20;

	    private GameWindow _gameWindow;
	    private Device _device;

	    private GraphicsPipeline _psoDraw;

	    private byte[] _vertexData;
        private int _vertexBuffer;
        private int _vertexBufferSize = 0;
        private VertexBufferSource _vboSource;

        private byte[] _indexData;
        private int _indexBuffer;
        private int _indexBufferSize;

        // Textures
        private Dictionary<IntPtr, Texture2D> _loadedTextures;

        private int _textureId;
        private IntPtr? _fontTextureId;

        // Input
        private int _scrollWheelValue;

        private List<int> _keys = new List<int>();

        public ImGuiRenderer(GameWindow gameWindow, Device device)
        {
            var context = ImGui.CreateContext();
            ImGui.SetCurrentContext(context);
            
            _gameWindow = gameWindow ?? throw new ArgumentNullException(nameof(gameWindow));
            _device = device ?? throw new ArgumentNullException(nameof(device));

            _loadedTextures = new Dictionary<IntPtr, Texture2D>();

            _vertexBuffer = GL.GenBuffer();
            _indexBuffer = GL.GenBuffer();

            _psoDraw = new GraphicsPipeline(
	            _device,
	            _device.GetShader("imgui.vert"),
	            _device.GetShader("imgui.frag"),
	            new VertexBufferFormat(new VertexAttribDescription[]
		            {
                        new VertexAttribDescription(
	                        0,
	                        0,
	                        2,
	                        0,
	                        VertexAttribType.Float,
	                        VertexAttribClass.Float,
	                        false),
                        new VertexAttribDescription(
	                        1,
	                        0,
	                        2,
	                        8,
	                        VertexAttribType.Float,
	                        VertexAttribClass.Float,
	                        false),
                        new VertexAttribDescription(
	                        2,
	                        0,
	                        4,
	                        16,
	                        VertexAttribType.UnsignedByte,
	                        VertexAttribClass.Float,
	                        true),
                    }),
	            new RasterizerState(CullfaceState.None),
	            new DepthState(DepthFunction.Always, false),
	            new BlendState(BlendMode.AlphaBlending)
	            );

            SetupInput();
        }

        #region ImGuiRenderer

        /// <summary>
        /// Creates a texture and loads the font data from ImGui. Should be called when the <see cref="Device" /> is initialized but before any rendering is done
        /// </summary>
        public virtual unsafe void RebuildFontAtlas()
        {
            // Get font texture from ImGui
            var io = ImGui.GetIO();
            io.Fonts.GetTexDataAsRGBA32(out byte* pixelData, out int width, out int height, out int bytesPerPixel);

            // Copy the data to a managed array
            var pixels = new byte[width * height * bytesPerPixel];
            unsafe { Marshal.Copy(new IntPtr(pixelData), pixels, 0, pixels.Length); }

            // Create and register the texture as an XNA texture
            var tex2d = new Texture2D(_device, "ImguiFontAtlas", SizedInternalFormatGlob.RGBA8, width, height, 1);
            tex2d.TexSubImage2D(_device, 0, 0, 0, width, height, PixelFormat.Rgba, PixelType.UnsignedByte, pixels);

            // Should a texture already have been build previously, unbind it first so it can be deallocated
            if (_fontTextureId.HasValue) UnbindTexture(_fontTextureId.Value);

            // Bind the new texture to an ImGui-friendly id
            _fontTextureId = BindTexture(tex2d);

            // Let ImGui know where to find the texture
            io.Fonts.SetTexID(_fontTextureId.Value);
            io.Fonts.ClearTexData(); // Clears CPU side texture data
        }

        /// <summary>
        /// Creates a pointer to a texture, which can be passed through ImGui calls such as <see cref="ImGui.Image" />. That pointer is then used by ImGui to let us know what texture to draw
        /// </summary>
        public virtual IntPtr BindTexture(Texture2D texture)
        {
            var id = new IntPtr(_textureId++);

            _loadedTextures.Add(id, texture);

            return id;
        }

        /// <summary>
        /// Removes a previously created texture pointer, releasing its reference and allowing it to be deallocated
        /// </summary>
        public virtual void UnbindTexture(IntPtr textureId)
        {
            var tex = _loadedTextures[textureId];
            _loadedTextures.Remove(textureId);
        }

        /// <summary>
        /// Changes an id to point to a different texture
        /// </summary>
        public virtual void RebindTexture(IntPtr textureId, Texture2D texture)
        {
            _loadedTextures[textureId] = texture;
        }

        /// <summary>
        /// Sets up ImGui for a new frame, should be called at frame start
        /// </summary>
        public virtual void BeforeLayout(float deltaTimeSeconds)
        {
            ImGui.GetIO().DeltaTime = deltaTimeSeconds;

            UpdateInput();

            ImGui.NewFrame();
        }

        /// <summary>
        /// Asks ImGui for the generated geometry data and sends it to the graphics pipeline, should be called after the UI is drawn using ImGui.** calls
        /// </summary>
        public virtual void AfterLayout()
        {
            ImGui.Render();

            unsafe { RenderDrawData(ImGui.GetDrawData()); }
        }

        #endregion ImGuiRenderer

        #region Setup & Update

        /// <summary>
        /// Maps ImGui keys to XNA keys. We use this later on to tell ImGui what keys were pressed
        /// </summary>
        protected virtual void SetupInput()
        {
            var io = ImGui.GetIO();

            _keys.Add(io.KeyMap[(int)ImGuiKey.Tab] = (int)Keys.Tab);
            _keys.Add(io.KeyMap[(int)ImGuiKey.LeftArrow] = (int)Keys.Left);
            _keys.Add(io.KeyMap[(int)ImGuiKey.RightArrow] = (int)Keys.Right);
            _keys.Add(io.KeyMap[(int)ImGuiKey.UpArrow] = (int)Keys.Up);
            _keys.Add(io.KeyMap[(int)ImGuiKey.DownArrow] = (int)Keys.Down);
            _keys.Add(io.KeyMap[(int)ImGuiKey.PageUp] = (int)Keys.PageUp);
            _keys.Add(io.KeyMap[(int)ImGuiKey.PageDown] = (int)Keys.PageDown);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Home] = (int)Keys.Home);
            _keys.Add(io.KeyMap[(int)ImGuiKey.End] = (int)Keys.End);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Delete] = (int)Keys.Delete);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Backspace] = (int)Keys.Backspace);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Enter] = (int)Keys.Enter);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Escape] = (int)Keys.Escape);
            _keys.Add(io.KeyMap[(int)ImGuiKey.A] = (int)Keys.A);
            _keys.Add(io.KeyMap[(int)ImGuiKey.C] = (int)Keys.C);
            _keys.Add(io.KeyMap[(int)ImGuiKey.V] = (int)Keys.V);
            _keys.Add(io.KeyMap[(int)ImGuiKey.X] = (int)Keys.X);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Y] = (int)Keys.Y);
            _keys.Add(io.KeyMap[(int)ImGuiKey.Z] = (int)Keys.Z);

            _gameWindow.TextInput += args =>
            {
	            if(args.Unicode == '\t')
		            return;

	            io.AddInputCharacter((uint) args.Unicode);
            };

            ImGui.GetIO().Fonts.AddFontDefault();
        }

        /// <summary>
        /// Sends input state to ImGui
        /// </summary>
        protected virtual void UpdateInput()
        {
            var io = ImGui.GetIO();

            var mouse = _gameWindow.MouseState;
            var keyboard = _gameWindow.KeyboardState;

            for (int i = 0; i < _keys.Count; i++)
            {
                io.KeysDown[_keys[i]] = keyboard.IsKeyDown((Keys)_keys[i]);
            }

            io.KeyShift = keyboard.IsKeyDown(Keys.LeftShift) || keyboard.IsKeyDown(Keys.RightShift);
            io.KeyCtrl = keyboard.IsKeyDown(Keys.LeftControl) || keyboard.IsKeyDown(Keys.RightControl);
            io.KeyAlt = keyboard.IsKeyDown(Keys.LeftAlt) || keyboard.IsKeyDown(Keys.RightAlt);
            io.KeySuper = keyboard.IsKeyDown(Keys.LeftSuper) || keyboard.IsKeyDown(Keys.RightSuper);

            io.DisplaySize = new System.Numerics.Vector2(_gameWindow.Size.X, _gameWindow.Size.Y);
            io.DisplayFramebufferScale = new System.Numerics.Vector2(1f, 1f);

            io.MousePos = new System.Numerics.Vector2(mouse.X, mouse.Y);

            io.MouseDown[0] = mouse[MouseButton.Left];
            io.MouseDown[1] = mouse[MouseButton.Right];
            io.MouseDown[2] = mouse[MouseButton.Middle];

            io.MouseWheel = mouse.ScrollDelta.Y;
        }

        #endregion Setup & Update

        #region Internals

        /// <summary>
        /// Gets the geometry as set up by ImGui and sends it to the graphics device
        /// Assumes viewport set to entire window.
        /// Disables scissor test.
        /// </summary>
        private void RenderDrawData(ImDrawDataPtr drawData)
        {
            // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled, vertex/texcoord/color pointers
            GL.Enable(EnableCap.ScissorTest);

            // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
            drawData.ScaleClipRects(ImGui.GetIO().DisplayFramebufferScale);

            UpdateBuffers(drawData);

            _device.BindPipeline(_psoDraw);
            var io = ImGui.GetIO();
            _psoDraw.ShaderVertex.SetUniformF("offset_scale", new Vector4(-1, 1, 2f / io.DisplaySize.X, -2f / io.DisplaySize.Y));
            RenderCommandLists(drawData);

            // Restore modified state
            GL.Disable(EnableCap.ScissorTest);
        }

        private unsafe void UpdateBuffers(ImDrawDataPtr drawData)
        {
            if (drawData.TotalVtxCount == 0)
            {
                return;
            }

            // Expand buffers if we need more room
            if (drawData.TotalVtxCount > _vertexBufferSize)
            {
                if(_vertexBuffer > 0)
                    GL.DeleteBuffer(_vertexBuffer);

                _vertexBuffer = GL.GenBuffer();
                _vboSource = new VertexBufferSource(0, new VertexBufferBinding(_vertexBuffer, 0, VertexSize, 0));

                _vertexBufferSize = (int)(drawData.TotalVtxCount * 1.5f);
                GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBuffer);
                GL.BufferData(BufferTarget.ArrayBuffer, new IntPtr(VertexSize * _vertexBufferSize), IntPtr.Zero, BufferUsageHint.DynamicDraw);
                GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
                _vertexData = new byte[_vertexBufferSize * VertexSize];
            }

            if (drawData.TotalIdxCount > _indexBufferSize)
            {
	            if (_indexBuffer > 0)
		            GL.DeleteBuffer(_indexBuffer);

	            _indexBuffer = GL.GenBuffer();

                _indexBufferSize = (int)(drawData.TotalIdxCount * 1.5f);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBuffer);
                GL.BufferData(BufferTarget.ElementArrayBuffer, new IntPtr(2 * _indexBufferSize), IntPtr.Zero, BufferUsageHint.DynamicDraw);
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
                _indexData = new byte[_indexBufferSize * sizeof(ushort)];
            }

            // Copy ImGui's vertices and indices to a set of managed byte arrays
            int vtxOffset = 0;
            int idxOffset = 0;

            GL.BindBuffer(BufferTarget.ArrayBuffer, _vertexBuffer);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBuffer);

            // TODO: many calls to glBufferSubData, probably slow
            for (int n = 0; n < drawData.CmdListsCount; n++)
            {
                ImDrawListPtr cmdList = drawData.CmdListsRange[n];

                GL.BufferSubData(BufferTarget.ArrayBuffer, new IntPtr(vtxOffset * VertexSize), new IntPtr(cmdList.VtxBuffer.Size * VertexSize), cmdList.VtxBuffer.Data);

                GL.BufferSubData(BufferTarget.ElementArrayBuffer, new IntPtr(idxOffset * 2), new IntPtr(cmdList.IdxBuffer.Size * 2), cmdList.IdxBuffer.Data);

                vtxOffset += cmdList.VtxBuffer.Size;
                idxOffset += cmdList.IdxBuffer.Size;
            }

            GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        private unsafe void RenderCommandLists(ImDrawDataPtr drawData)
        {
            _device.BindVertexBufferSource(_vboSource);
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBuffer);

            int vtxOffset = 0;
            int idxOffset = 0;

            for (int n = 0; n < drawData.CmdListsCount; n++)
            {
                ImDrawListPtr cmdList = drawData.CmdListsRange[n];

                for (int cmdi = 0; cmdi < cmdList.CmdBuffer.Size; cmdi++)
                {
                    ImDrawCmdPtr drawCmd = cmdList.CmdBuffer[cmdi];

                    if (!_loadedTextures.ContainsKey(drawCmd.TextureId))
                    {
                        throw new InvalidOperationException($"Could not find a texture with id '{drawCmd.TextureId}', please check your bindings");
                    }

                    GL.Scissor(
						(int)drawCmd.ClipRect.X,
						_gameWindow.Size.Y - (int)drawCmd.ClipRect.W,
                        (int)(drawCmd.ClipRect.Z - drawCmd.ClipRect.X),
                        (int)(drawCmd.ClipRect.W - drawCmd.ClipRect.Y)
                    );

                    _device.BindTexture(_loadedTextures[drawCmd.TextureId], 0);

                    GL.DrawElementsBaseVertex(PrimitiveType.Triangles, (int) drawCmd.ElemCount,
	                    DrawElementsType.UnsignedShort, new IntPtr(idxOffset * 2), vtxOffset);

                    idxOffset += (int)drawCmd.ElemCount;
                }

                vtxOffset += cmdList.VtxBuffer.Size;
            }

            GL.BindBuffer(BufferTarget.ElementArrayBuffer, 0);
        }

        #endregion Internals
    }
}
