﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;
using GlobCore;
using Treegen;
using TreeEditor;
using TreeEditor.Rendering;

namespace TreeEditor
{
	/// <summary>
	/// Stores a single lod level meshes of a single tree instance
	/// </summary>
	class TreeLodInstance : IDisposable
	{
		public Mesh MeshTrunk = null;
		public Mesh MeshLeaves = null;
		public Mesh MeshPolyplanes = null;
		public VertexBufferSet VertexBuffersTrunk = new VertexBufferSet();
		public VertexBufferSet VertexBuffersLeaves = new VertexBufferSet();
		public VertexBufferSet VertexBuffersPolyplanes = new VertexBufferSet();
		public SceneObject SceneTrunk, SceneLeaves, ScenePolyplanes;

		public TreeBatch Batch;

		public TreeLodInstance(TreeBatch batch)
		{
			Batch = batch;
		}

		public void UpdateFromMesherResult(MesherResultMeshes result)
		{
			MeshTrunk = result.MeshTrunk;
			MeshLeaves = result.MeshLeaves;
			MeshPolyplanes = result.MeshPolyplanes;

			VertexBuffersTrunk.UpdateFromMesh(MeshTrunk.Vertices, MeshTrunk.Indices);
			VertexBuffersLeaves.UpdateFromMesh(MeshLeaves.Vertices, MeshLeaves.Indices);
			VertexBuffersPolyplanes.UpdateFromMesh(MeshPolyplanes.Vertices, MeshPolyplanes.Indices);
		}

		public void AddSceneObjects(List<SceneObject> objects, PolyplaneRenderer.PolyplaneAtlas polyplaneAtlas, Vector3 position, Vector4 color)
		{
			// Trunk
			objects.Add(new SceneObject()
			{
				Transform = GlobCore.Utils.GetWorldMatrix(position, Quaternion.Identity, new Vector3(1f)),
				Mesh = VertexBuffersTrunk,
				Color = color,
				TexDiffuse = Batch.TexTrunkDiffuse,
				TexNormal = Batch.TexTrunkNormal,
			});
			// Leaves
			objects.Add(new SceneObject()
			{
				Transform = GlobCore.Utils.GetWorldMatrix(position, Quaternion.Identity, new Vector3(1f)),
				Mesh = VertexBuffersLeaves,
				Color = color,
				TexDiffuse = Batch.TexLeavesDiffuse,
				TexNormal = Batch.TexLeavesNormal,
				TexAlpha = Batch.TexLeavesAlpha,
			});
			// Polyplanes
			objects.Add(new SceneObject()
			{
				Transform = GlobCore.Utils.GetWorldMatrix(position, Quaternion.Identity, new Vector3(1f)),
				Mesh = VertexBuffersPolyplanes,
				Color = color,
				TexDiffuse = polyplaneAtlas.TexDiffuse,
				TexNormal = polyplaneAtlas.TexNormal,
				TexAlpha = polyplaneAtlas.TexAlpha,
			});
		}

		public void Dispose()
		{
			this.VertexBuffersTrunk.Dispose();
			this.VertexBuffersLeaves.Dispose();
			this.VertexBuffersPolyplanes.Dispose();
		}

		public TreeStats GetStats()
		{
			var trunk = GetMeshStats(MeshTrunk);
			var leaves = GetMeshStats(MeshLeaves);
			var polyplanes = GetMeshStats(MeshPolyplanes);

			return new TreeStats()
			{
				Trunk = trunk,
				Leaves = leaves,
				Polyplanes = polyplanes,
				Total = new MeshStats()
				{
					Vertices = trunk.Vertices + leaves.Vertices + polyplanes.Vertices,
					Faces = trunk.Faces + leaves.Faces + polyplanes.Faces,
				},
			};
		}

		static MeshStats GetMeshStats(Mesh m)
		{
			return new MeshStats()
			{
				Vertices = m.Vertices.Count,
				Faces = m.Indices.Count / 3,
			};
		}
	}

	public class MeshStats
	{
		public int Vertices { get; set; }
		public int Faces { get; set; }
	}

	class TreeStats
	{
		public MeshStats Trunk { get; set; }
		public MeshStats Leaves { get; set; }
		public MeshStats Polyplanes { get; set; }
		public MeshStats Total { get; set; }
	}

	/// <summary>
	/// A tree batch groups trees that use the same textures and growth parameters,
	/// but may use different seeds, starting ages, and different lods.
	/// Also stores the entire project state in one of its members.
	/// </summary>
	class TreeBatch
	{
		// Filename of the current tree "project"
		public string Filename = null;

		// Textures used by the trees
		public Texture2D TexTrunkDiffuse;
		public Texture2D TexTrunkNormal;
		public Texture2D TexLeavesDiffuse;
		public Texture2D TexLeavesNormal;
		public Texture2D TexLeavesAlpha;

		// Imgui handle of the leaf texture
		public IntPtr LeafTextureImguiID = IntPtr.Zero;

		// The entire project is stored here
		public TreeEditorState State;

		/// <summary>
		/// The grid of generated trees [#lods * #instances]
		/// </summary>
		private TreeLodInstance[,] _instances = new TreeLodInstance[0, 0];

		private PolyplaneRenderer.PolyplaneAtlas[] _polyplaneAtlases = new PolyplaneRenderer.PolyplaneAtlas[0];

		private TextOutput _textOutput;
		private PolyplaneRenderer _polyplaneRenderer;

		private float _spacing = 1.0f;

		public TreeBatch(TextOutput textOutput, PolyplaneRenderer polyplaneRenderer)
		{
			_textOutput = textOutput;
			_polyplaneRenderer = polyplaneRenderer;

			State = TreeEditorState.CreateDefault();

			// Populates our grower with all modules automatically
			State.Grower = Grower.CreateWithAllModules();

			// This is only done to make the modules appear in the execution order in the menus.
			// If we did not do this, the modules would start in a random order and
			// reorder themselves once a tree is first generated.
			State.Grower.SortModules();
		}

		private TreeVertex CreateTree(TreeInstanceSettings settings)
		{
			var root = TreeVertex.CreateRoot(settings.Seed, settings.Age);

			State.Grower.Grow(new GrowContext()
			{
				Root = root,
			}, (message) => _textOutput.OnError(message));

			return root;
		}

		public void UpdateAll()
		{
			// Ensure there is at least one lod and one instance
			if(State.Instances.Count == 0)
			{
				State.Instances.Add(new TreeInstanceSettings());
			}
			if(State.MesherParams.LodParams.Count == 0)
			{
				State.MesherParams.LodParams.Add(new MesherLodParams());
			}

			// First, clean up previous trees
			foreach(var t in _instances)
			{
				t.Dispose();
			}
			foreach(var a in _polyplaneAtlases)
			{
				a.Dispose();
			}

			int lodCount = State.MesherParams.LodParams.Count;
			int instanceCount = State.Instances.Count;

			// Generate tree graphs
			var roots = new TreeVertex[instanceCount];
			for(int i = 0; i < roots.Length; i++)
			{
				roots[i] = CreateTree(State.Instances[i]);
			}

			// Generate tree meshes
			var meshResults = new MesherResult[lodCount];
			Parallel.For(0, lodCount, lod =>
			{
				meshResults[lod] = Mesher.GenerateBatch(State.MesherParams, lod, roots);
			});

			// Generate tree polyplanes (not parallel because we use the GPU)
			_polyplaneAtlases = new PolyplaneRenderer.PolyplaneAtlas[lodCount];
			for (int lod = 0; lod < lodCount; lod++)
			{
				var lodparams = State.MesherParams.LodParams[lod];
				_polyplaneAtlases[lod] = new PolyplaneRenderer.PolyplaneAtlas(_polyplaneRenderer,
					new Vector2i(lodparams.PolyplaneAtlasSizeX, lodparams.PolyplaneAtlasSizeY));
				_polyplaneAtlases[lod].ExecuteRenderCommands(this, meshResults[lod].PolyplaneRenderCommands);
			}

			// Inicialize new trees
			_instances = new TreeLodInstance[lodCount, instanceCount];
			for (int lod = 0; lod < lodCount; lod++)
			{
				for (int ins = 0; ins < instanceCount; ins++)
				{
					_instances[lod, ins] = new TreeLodInstance(this);
					_instances[lod, ins].UpdateFromMesherResult(meshResults[lod].Meshes[ins]);
				}
			}

			// Compute spacing for the tree grid
			_spacing = 0;

			Vector3 min = new Vector3(float.PositiveInfinity);
			Vector3 max = new Vector3(float.NegativeInfinity);

			for(int lod = 0; lod < lodCount; lod++)
			{
				var mesh = meshResults[lod].Meshes[0];
				mesh.MeshTrunk.ComputeBoundingBox(ref min, ref max);
				mesh.MeshLeaves.ComputeBoundingBox(ref min, ref max);
				mesh.MeshPolyplanes.ComputeBoundingBox(ref min, ref max);
			}

			_spacing = Math.Max(Math.Max(min.X, min.Z), Math.Max(max.X, max.Z));

			// Done!
		}

		public void GatherForRendering(List<SceneObject> objects)
		{
			const float spacingMult = 3.0f;

			for(int lod = 0; lod < _instances.GetLength(0); lod++)
			{
				for (int i = 0; i < _instances.GetLength(1); i++)
				{
					var instance = _instances[lod, i];
					instance.AddSceneObjects(
						objects: objects,
						polyplaneAtlas: _polyplaneAtlases[lod],
						position: new Vector3(_spacing * i * spacingMult * -1, 0, _spacing * lod * spacingMult),
						color: Vector4.One);
				}
			}
		}

		/// <summary>
		/// For each tree instance an array of lod stats is returned.
		/// </summary>
		/// <returns></returns>
		public TreeStats[][] GetStats()
		{
			TreeStats[][] stats = new TreeStats[_instances.GetLength(1)][];

			for(int i = 0; i < stats.Length; i++)
			{
				var lodStats = new TreeStats[_instances.GetLength(0)];
				stats[i] = lodStats;

				for(int lod = 0; lod < lodStats.Length; lod++)
				{
					lodStats[lod] = _instances[lod, i].GetStats();
				}
			}

			return stats;
		}

		#region Save Load

		static JsonSerializerSettings JsonSettings = new JsonSerializerSettings()
		{
			TypeNameHandling = TypeNameHandling.All,
			Formatting = Formatting.Indented,
			ObjectCreationHandling = ObjectCreationHandling.Replace,
		};

		static void SaveTexture(Device device, Texture2D tex, Stream stream, bool png)
		{
			using (DirectBitmap dbm = new DirectBitmap(tex.Width, tex.Height))
			{
				device.BindTexture(tex, 0);
				GL.GetTexImage(tex.Target, 0, PixelFormat.Bgra, PixelType.UnsignedByte, dbm.Bits);
				device.BindTexture(TextureTarget.Texture2D, 0, 0);

				using (var bm = dbm.ToBitmap())
				{
					bm.Save(stream, png ? System.Drawing.Imaging.ImageFormat.Png : System.Drawing.Imaging.ImageFormat.Jpeg);
				}
			}
		}

		public void Serialize(string filename)
		{
			using(StreamWriter sw = new StreamWriter(File.OpenWrite(filename)))
			{
				var json = JsonConvert.SerializeObject(this.State, JsonSettings);
				sw.Write(json);
			}
		}

		public void Deserialize(string filename)
		{
			string data = null;

			using(StreamReader sr = new StreamReader(File.OpenRead(filename)))
			{
				data = sr.ReadToEnd();
			}

			var newstate = JsonConvert.DeserializeObject<TreeEditorState>(data, JsonSettings);

			if (newstate == null || newstate.Grower.Modules.Count == 0 || newstate.MesherParams.LodParams.Count == 0)
				throw new Exception("Failed to load tree from json.");

			this.State = newstate;
		}

		public void Export(Device device, string name)
		{
			const bool flipTextureCoords = true;

			using(var stream = new FileStream(name + ".zip", FileMode.Create, FileAccess.Write))
			{
				using(var zip = new ZipArchive(stream, ZipArchiveMode.Create))
				{
					void MakeEntry(string name, Action<Stream> writeFunc)
					{
						var entry = zip.CreateEntry(name, CompressionLevel.Optimal);
						using(var entryStream = entry.Open())
						{
							writeFunc(entryStream);
						}
					}

					for (int lod = 0; lod < _instances.GetLength(0); lod++)
					{
						string namelod = $"LOD{lod}";

						MakeEntry($"{namelod}_diffuse.png", s => SaveTexture(device, _polyplaneAtlases[lod].TexDiffuse, s, png: true));
						MakeEntry($"{namelod}_normal.png", s => SaveTexture(device, _polyplaneAtlases[lod].TexNormal, s, png: true));
						MakeEntry($"{namelod}_alpha.png", s => SaveTexture(device, _polyplaneAtlases[lod].TexAlpha, s, png: true));

						for (int i = 0; i < _instances.GetLength(1); i++)
						{
							var ins = _instances[lod, i];
							MakeEntry($"{namelod}_INS{i}_trunk.obj", s => ObjWriter.ExportMesh(ins.MeshTrunk, s, flipTexCoords: flipTextureCoords));
							MakeEntry($"{namelod}_INS{i}_leaves.obj", s => ObjWriter.ExportMesh(ins.MeshLeaves, s, flipTexCoords: flipTextureCoords));
							MakeEntry($"{namelod}_INS{i}_polyplanes.obj", s => ObjWriter.ExportMesh(ins.MeshPolyplanes, s, flipTexCoords: flipTextureCoords));
						}
					}
				}
			}
		}

		#endregion

		#region Textures

		public void SetTrunkTextureDiffuse(GlobCore.Device device, string diffuseFile)
		{
			GlobCore.Texture2D texDiffuse = null;

			if (diffuseFile != null)
			{
				texDiffuse = TryLoadTexture(device, _textOutput, diffuseFile);
			}

			TexTrunkDiffuse?.Dispose();

			if (texDiffuse != null)
			{
				TexTrunkDiffuse = texDiffuse;
				State.TexFileTrunk = diffuseFile;
			}
			else
			{
				TexTrunkDiffuse = null;
				State.TexFileTrunk = null;
			}
		}

		public void SetTrunkTextureNormal(GlobCore.Device device, string normalFile)
		{
			GlobCore.Texture2D texNormal = null;

			if (normalFile != null)
			{
				texNormal = TryLoadTexture(device, _textOutput, normalFile);
			}

			TexTrunkNormal?.Dispose();

			if (texNormal != null)
			{
				TexTrunkNormal = texNormal;
				State.TexFileTrunkNormal = normalFile;
			}
			else
			{
				TexTrunkNormal = null;
				State.TexFileTrunkNormal = null;
			}
		}

		public void SetLeafTextureDiffuse(ImGuiRenderer imgui, GlobCore.Device device, string diffuseFile)
		{
			GlobCore.Texture2D texDiffuse = null;

			if(diffuseFile != null)
			{
				texDiffuse = TryLoadTexture(device, _textOutput, diffuseFile);
			}

			TexLeavesDiffuse?.Dispose();

			if (texDiffuse != null)
			{
				// Also update ImGui handle
				if (LeafTextureImguiID == IntPtr.Zero)
				{
					LeafTextureImguiID = imgui.BindTexture(texDiffuse);
				}
				else
				{
					imgui.RebindTexture(LeafTextureImguiID, texDiffuse);
				}

				TexLeavesDiffuse = texDiffuse;
				State.TexFileLeaf = diffuseFile;
			}
			else
			{
				LeafTextureImguiID = IntPtr.Zero;
				TexLeavesDiffuse = null;
				State.TexFileLeaf = null;
			}
		}

		public void SetLeafTextureNormal(GlobCore.Device device, string normalFile)
		{
			GlobCore.Texture2D texNormal = null;

			if (normalFile != null)
			{
				texNormal = TryLoadTexture(device, _textOutput, normalFile);
			}

			TexLeavesNormal?.Dispose();

			if (texNormal != null)
			{
				TexLeavesNormal = texNormal;
				State.TexFileLeafNormal = normalFile;
			}
			else
			{
				TexLeavesNormal = null;
				State.TexFileLeafNormal = null;
			}
		}

		public void SetLeafTextureAlpha(GlobCore.Device device, string alphaFile)
		{
			GlobCore.Texture2D texAlpha = null;

			if (alphaFile != null)
			{
				// Generating proper mips for alpha-tested textures is complex.
				// For simplicity, we just disable mips altogether.
				texAlpha = TryLoadTexture(device, _textOutput, alphaFile, genMips: false);
			}

			TexLeavesAlpha?.Dispose();

			if (texAlpha != null)
			{
				TexLeavesAlpha = texAlpha;
				State.TexFileLeafAlpha = alphaFile;
			}
			else
			{
				TexLeavesAlpha = null;
				State.TexFileLeafAlpha = null;
			}
		}

		static GlobCore.Texture2D TryLoadTexture(GlobCore.Device device, TextOutput textOutput, string file, bool genMips = true)
		{
			try
			{
				return LoadColor(device, file, genMips);
			}
			catch(Exception e)
			{
				textOutput.OnError("Error loading texture " + file + ": " + e.Message);
				return null;
			}
		}

		static Texture2D LoadColor(Device device, string fileColor, bool genMips = true)
		{
			var image = System.Drawing.Image.FromFile(fileColor);
			var dbm = new DirectBitmap(image);
			Texture2D tex = new Texture2D(device, Path.GetFileName(fileColor),
				SizedInternalFormatGlob.RGBA8, image.Width, image.Height, genMips ? 0 : 1);

			tex.TexSubImage2D(device, 0, 0, 0, image.Width, image.Height,
				PixelFormat.Bgra, PixelType.UnsignedByte, dbm.Bits);

			GlobCore.Utils.SetTextureParameters(device, tex, TextureWrapMode.Repeat,
				TextureMagFilter.Linear, TextureMinFilter.LinearMipmapLinear);

			if (genMips)
			{
				device.BindTexture(TextureTarget.Texture2D, tex.Handle);
				GL.GenerateMipmap(GenerateMipmapTarget.Texture2D);
			}

			device.BindTexture(TextureTarget.Texture2D, 0);

			return tex;
		}

		#endregion
	}
}
