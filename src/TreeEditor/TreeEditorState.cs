﻿using System;
using System.Collections.Generic;
using System.Text;
using Treegen;

namespace TreeEditor
{
	/// <summary>
	/// Stores everything related to tree editing itself. Saving a tree to json simply (de)serializes this object.
	/// </summary>
	public class TreeEditorState
	{
		public string TexFileTrunk { get; set; } = null;
		public string TexFileTrunkNormal { get; set; } = null;
		public string TexFileLeaf { get; set; } = null;
		public string TexFileLeafNormal { get; set; } = null;
		public string TexFileLeafAlpha { get; set; } = null;

		public List<TreeInstanceSettings> Instances { get; set; } = new List<TreeInstanceSettings>();

		public Grower Grower { get; set; } = new Grower();
		public MesherParams MesherParams { get; set; } = new MesherParams();

		public static TreeEditorState CreateDefault()
		{
			return new TreeEditorState()
			{
				Grower = new Grower(),
				MesherParams = new MesherParams(),
				Instances = new List<TreeInstanceSettings>()
				{
					new TreeInstanceSettings(),
					new TreeInstanceSettings(0.25f, 0),
				},
			};
		}
	}

	/// <summary>
	/// Settings for one tree instance in a batch.
	/// We only allow the changing of age and seed,
	/// anything else is considered a different tree species.
	/// </summary>
	public class TreeInstanceSettings
	{
		public float Age { get; set; } = 1.0f;
		public int Seed { get; set; } = 0;

		public TreeInstanceSettings()
		{
		}

		public TreeInstanceSettings(float age, int seed)
		{
			Age = age;
			Seed = seed;
		}

		public TreeInstanceSettings Clone()
		{
			return new TreeInstanceSettings()
			{
				Age = this.Age,
				Seed = this.Seed,
			};
		}
	}
}
