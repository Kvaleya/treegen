﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using Treegen;

namespace TreeEditor.Rendering
{
	static class Utils
	{
		/// <summary>
		/// Generates a horizontal plane. Used as a "floor" for the editor scene.
		/// </summary>
		public static VertexBufferSet TestGenPlane()
		{
			List<MeshVertex> vertices = new List<MeshVertex>();
			List<int> indices = new List<int>();

			vertices.Add(new MeshVertex(new Vector3(-1, 0, -1), Vector3.UnitY, Vector4.One, new Vector2(0, 0), Vector3.UnitX));
			vertices.Add(new MeshVertex(new Vector3(+1, 0, -1), Vector3.UnitY, Vector4.One, new Vector2(0, 1), Vector3.UnitX));
			vertices.Add(new MeshVertex(new Vector3(-1, 0, +1), Vector3.UnitY, Vector4.One, new Vector2(1, 0), Vector3.UnitX));
			vertices.Add(new MeshVertex(new Vector3(+1, 0, +1), Vector3.UnitY, Vector4.One, new Vector2(1, 1), Vector3.UnitX));

			indices.Add(0);
			indices.Add(2);
			indices.Add(1);
			indices.Add(1);
			indices.Add(2);
			indices.Add(3);

			var vbs = new VertexBufferSet();
			vbs.UpdateFromMesh(vertices, indices);
			return vbs;
		}
	}
}
