﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Treegen
{
	/// <summary>
	/// A K-Means++ based cluster generator.
	/// </summary>
	class ClusterGenKmeans : IClusterGen
	{
		const int SeedInicialization = 123;
		const int Iterations = 20;

		public void GenerateClusters(List<TreeVertex> vertices, int desiredClusterCount, Dictionary<TreeVertex, TreeVertex> clusterMap, List<TreeVertex> resultVertices)
		{
			if(vertices.Count <= desiredClusterCount)
			{
				// No point in running kmeans if there are less or equal vertices than desired clusters.
				foreach(var v in vertices)
				{
					clusterMap.Add(v, v);
					resultVertices.Add(v);
				}
				return;
			}

			var points = GetPointsFromVertices(vertices);

			// Initialize K-Means++
			var centers = InitKmeansPlusPlus(points, desiredClusterCount);
			int[] clusterIndices = new int[points.Length];

			for(int i = 0; i < Iterations; i++)
			{
				// Assign each point to a cluster
				FindNearestCenterIndices(clusterIndices, points, centers);
				// Update each cluster center to the average of its points
				FindClusterAverages(points, clusterIndices, averages: centers);
			}

			// We are done iterating

			// Snap each cluster center to a real tree vertex
			int[] clusterToVertexMap = new int[centers.Length];
			// Some clusters might end up being the same vertex and thus get merges
			// but the resultVertices list must be unique. 
			HashSet<int> clusterVertexIndices = new HashSet<int>();

			for(int i = 0; i < centers.Length; i++)
			{
				int nearestIndex = FindNearestCenterToPoint(centers[i], points);
				centers[i] = points[nearestIndex];
				clusterToVertexMap[i] = nearestIndex;
				if(!clusterVertexIndices.Contains(nearestIndex))
				{
					// Add the snapped vertex into the list of branch root vertices
					// but only if it is not already present.
					resultVertices.Add(vertices[nearestIndex]);
					clusterVertexIndices.Add(nearestIndex);
				}
			}

			// Assign cluster indices one last time
			FindNearestCenterIndices(clusterIndices, points, centers);

			// Fill the vertex->cluster map
			for(int i = 0; i < points.Length; i++)
			{
				int clusterIndex = clusterIndices[i];
				int vertexIndex = clusterToVertexMap[clusterIndex];
				clusterMap.Add(vertices[i], vertices[vertexIndex]);
			}

			// Done!
		}

		/// <summary>
		/// For a given clustering in clusterIndices, computes the average of each cluster and stores that in the averages array.
		/// </summary>
		/// <param name="points">The set of points that are divided into clusters.</param>
		/// <param name="clusterIndices">Array that assigns a cluster index to each point.</param>
		/// <param name="averages">The average of a cluster is stored to the cluster index in this array. This array and its subarrays must already be inicialized to the proper size [clusterCount][size of a point].</param>
		static void FindClusterAverages(float[][] points, int[] clusterIndices, float[][] averages)
		{
			int[] clusterPointCounts = new int[averages.Length];

			for(int i = 0; i < averages.Length; i++)
			{
				// Zero the averages
				for (int j = 0; j < averages[i].Length; j++)
				{
					averages[i][j] = 0;
				}
			}

			// Add every point to its cluster's average sum
			for(int i = 0; i < points.Length; i++)
			{
				int cluster = clusterIndices[i];
				Sum(averages[cluster], points[i]);
				clusterPointCounts[cluster]++;
			}

			// Divide averages by number of elements
			for (int i = 0; i < averages.Length; i++)
			{
				if (clusterPointCounts[i] > 0)
				{
					Mult(averages[i], 1f / clusterPointCounts[i]);
				}
			}
		}

		/// <summary>
		/// Fills the indices array with index of the nearest center for each point.
		/// </summary>
		/// <param name="indices">Array of nearest center indices. Must be the same length as the points array.</param>
		/// <param name="points">Array of points for which we are finding the nearest centers.</param>
		/// <param name="centers">Array of centers.</param>
		static void FindNearestCenterIndices(int[] indices, float[][] points, float[][] centers)
		{
			for(int i = 0; i < points.Length; i++)
			{
				indices[i] = FindNearestCenterToPoint(points[i], centers);
			}
		}

		/// <summary>
		/// Returns the index of center that is nearest to the specified point.
		/// </summary>
		/// <param name="centers">Centers from which to find the nearest.</param>
		/// <param name="point">We are looking for the nearest center to this point.</param>
		/// <returns>Index of the nearest center in the centers array.</returns>
		static int FindNearestCenterToPoint(float[] point, float[][] centers)
		{
			float nearestDist = float.PositiveInfinity;
			int nearestIndex = 0;
			for(int i = 0; i < centers.Length; i++)
			{
				float distSquared = DistanceSquared(centers[i], point);
				if(distSquared < nearestDist)
				{
					nearestDist = distSquared;
					nearestIndex = i;
				}
			}
			return nearestIndex;
		}

		/// <summary>
		/// Creates array of float "vectors" that can be fed to K-Means, representing a list of tree vertices. The order of points/vertices is preserved.
		/// </summary>
		/// <param name="vertices">Input tree vertices</param>
		static float[][] GetPointsFromVertices(List<TreeVertex> vertices)
		{
			float[][] points = new float[vertices.Count][];

			for(int i = 0; i < vertices.Count; i++)
			{
				points[i] = vertices[i].GetClusteringVector();
			}

			return points;
		}

		/// <summary>
		/// Creates a set of initial cluster locations using the K-Means++ approach.
		/// </summary>
		/// <param name="points">Points from which the cluster centers will be generated.</param>
		/// <param name="clusterCount">Desired number of clusters, must not be greater the number of points.</param>
		/// <param name="seed">Seed for the internal random number generator used to inicialize the clustering.</param>
		/// <returns>Array of cluster centers, containing exactly clusterCount entries.</returns>
		static float[][] InitKmeansPlusPlus(float[][] points, int clusterCount, int seed = SeedInicialization)
		{
			if(clusterCount > points.Length || clusterCount < 1)
			{
				throw new ArgumentException("Cluster count must not be greater than the number of points and must be at least one.", nameof(clusterCount));
			}

			float[][] centers = new float[clusterCount][];

			// To ensure every point is picked at most once
			bool[] isPointPicked = new bool[points.Length];

			SimpleRandom random = new SimpleRandom(seed);

			// We start with a single random cluster center
			{
				int firstPick = random.Next(0, points.Length);
				centers[0] = points[firstPick].ToArray(); // Clone the array
				isPointPicked[firstPick] = true;
			}

			for (int clusterIndex = 1; clusterIndex < clusterCount; clusterIndex++)
			{
				// Every new cluster center is picked from the remaining points based on probabilities:
				// Every point's probability of being picked is proportional to its squared distance
				// to the nearest already picked cluster center.

				// For each point get probability of being picked
				float probabilitySum = 0f;
				float[] probabilityArray = new float[points.Length];

				for(int i = 0; i < points.Length; i++)
				{
					float minProb = float.PositiveInfinity;

					for(int j = 0; j < clusterIndex; j++)
					{
						float prob = DistanceSquared(points[i], centers[j]);
						minProb = Math.Min(minProb, prob);
					}

					probabilityArray[i] = minProb;
					probabilitySum += minProb;
				}

				// Generate a random number in range 0..probabilitySum
				float sample = (float)(random.NextDouble() * probabilitySum);

				// Find what point we picked
				int picked = probabilityArray.Length - 1;
				float currentSum = 0f;

				for(int i = 0; i < probabilityArray.Length; i++)
				{
					if(currentSum >= sample)
					{
						picked = i;
						break;
					}
					currentSum += probabilityArray[i];
				}

				// Ensure that the points was not picked already
				while(isPointPicked[picked])
				{
					picked--;
					if (picked < 0)
						picked = points.Length - 1;
				}

				isPointPicked[picked] = true;
				centers[clusterIndex] = points[picked].ToArray(); // Clone the array
			}

			return centers;
		}

		/// <summary>
		/// Computes the squared euclidean distance between two vectors
		/// </summary>
		static float DistanceSquared(float[] a, float[] b)
		{
			float dist = 0f;
			for(int i = 0; i < a.Length; i++)
			{
				float d = Math.Abs(a[i] - b[i]);
				dist += d * d;
			}
			return dist;
		}

		/// <summary>
		/// Computes the dot product of two vectors
		/// </summary>
		//static float Dot(float[] a, float[] b)
		//{
		//	float sum = 0f;
		//	for (int i = 0; i < a.Length; i++)
		//	{
		//		sum += a[i] * b[i];
		//	}
		//	return sum;
		//}

		/// <summary>
		/// Sums vec to target
		/// </summary>
		static void Sum(float[] target, float[] vec)
		{
			for (int i = 0; i < target.Length; i++)
			{
				target[i] += vec[i];
			}
		}

		/// <summary>
		/// Multiplies each value in target with the specified multiplier
		/// </summary>
		static void Mult(float[] target, float multiplier)
		{
			for (int i = 0; i < target.Length; i++)
			{
				target[i] *= multiplier;
			}
		}
	}
}
