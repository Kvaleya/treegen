﻿using GlobCore;
using System;
using System.Collections.Generic;
using System.Linq;
using OpenTK.Graphics.OpenGL;
using OpenTK.Mathematics;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// A module that handles the rendering of generic textured meshes.
	/// </summary>
	class MeshRenderModule : RenderModule
	{
		[Flags]
		enum ShaderVariantFlags
		{
			Colormap = 1,
			Normalmap = 2,
			AlphaTest = 4,
		}

		const GetPName GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT = (GetPName)0x84FF;
		const float TexAnisotropy = 8f;

		// We use a sampler object with anisotropic filtering enabled.
		int _sampler;

		// We use several variants of each shader based on what features (textures, alpha testing) are present.
		GraphicsPipeline[] _psoMainVariants;
		GraphicsPipeline[] _psoGbufferVariants;

		GraphicsPipeline _psoMeshWireframe;

		public MeshRenderModule(RenderContext context)
			: base(context)
		{
			MakeSampler();

			// Vertex format for vertices
			var vertexFormatColor = new VertexBufferFormat(
				new VertexAttribDescription(0, 0, 3, 0, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(1, 0, 3, 12, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(2, 0, 4, 24, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(3, 0, 2, 40, VertexAttribType.Float, VertexAttribClass.Float, false),
				new VertexAttribDescription(4, 0, 3, 48, VertexAttribType.Float, VertexAttribClass.Float, false));

			GraphicsPipeline[] makeVariants(string vert, string frag, List<Tuple<string, string>> baseDefines = null)
			{
				if(baseDefines == null)
				{
					baseDefines = new List<Tuple<string, string>>();
				}

				GraphicsPipeline[] variants = new GraphicsPipeline[8];
				for(int i = 0; i < variants.Length; i++)
				{
					var current = (ShaderVariantFlags)i;
					var fragmentDefines = baseDefines.ToList();

					var cullstate = CullfaceState.Back;

					if (current.HasFlag(ShaderVariantFlags.Colormap))
					{
						fragmentDefines.Add(new Tuple<string, string>("COLORMAP", "1"));
					}
					if (current.HasFlag(ShaderVariantFlags.Normalmap))
					{
						fragmentDefines.Add(new Tuple<string, string>("NORMALMAP", "1"));
					}
					if (current.HasFlag(ShaderVariantFlags.AlphaTest))
					{
						fragmentDefines.Add(new Tuple<string, string>("ALPHA_TEST", "1"));
						cullstate = CullfaceState.None;
					}

					variants[i] = new GraphicsPipeline(
						Device,
						Device.GetShader(vert),
						Device.GetShader(frag, fragmentDefines),
						vertexFormatColor,
						new RasterizerState(cullstate),
						new DepthState(DepthFunction.Lequal, true),
						null);
				}

				return variants;
			}

			_psoMainVariants = makeVariants("mesh.vert", "mesh.frag");
			_psoGbufferVariants = makeVariants("mesh.vert", "mesh.frag", new List<Tuple<string, string>>()
			{
				new Tuple<string, string>("GBUFFER", "1"),
			});

			_psoMeshWireframe = new GraphicsPipeline(Device, Device.GetShader("mesh.vert"), Device.GetShader("mesh.frag"),
				vertexFormatColor, new RasterizerState(CullfaceState.None, PolygonMode.Line, PolygonMode.Line), new DepthState(DepthFunction.Lequal, true), null);
		}

		void MakeSampler()
		{
			if (_sampler > 0)
				GL.DeleteSampler(_sampler);
			_sampler = GL.GenSampler();

			float anisotropy = GL.GetFloat(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT);
			anisotropy = Math.Min(anisotropy, TexAnisotropy);

			GL.SamplerParameter(_sampler, SamplerParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureMinFilter, (int)TextureMinFilter.LinearMipmapLinear);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapR, (int)TextureWrapMode.Repeat);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapS, (int)TextureWrapMode.Repeat);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureWrapT, (int)TextureWrapMode.Repeat);
			GL.SamplerParameter(_sampler, SamplerParameterName.TextureMaxAnisotropyExt, anisotropy);
		}

		/// <summary>
		/// Draws the supplied scene.
		/// </summary>
		/// <param name="scene">Scene description, including lighting and list of objects to render.</param>
		/// <param name="projection">Projection matrix to use while rendering.</param>
		/// <param name="isGbuffer">When true, G-buffer shader variants are used instead of forward-shading variants.</param>
		public void Draw(Scene scene, Matrix4 projection, bool isGbuffer = false)
		{
			GL.BindSampler(0, _sampler);
			GL.BindSampler(1, _sampler);
			GL.BindSampler(2, _sampler);

			Device.BindTexture(TextureTarget.Texture2D, 0, 0);
			Device.BindTexture(TextureTarget.Texture2D, 0, 1);
			Device.BindTexture(TextureTarget.Texture2D, 0, 2);

			// The G-buffer shader variants are used for polyplane rendering
			var variantArray = _psoMainVariants;
			if(isGbuffer)
			{
				variantArray = _psoGbufferVariants;
			}

			// For each object in the scene...
			foreach (var o in scene.WorldObjects)
			{
				if (o.Mesh.IndexCount == 0)
					continue;

				// Find the proper shader variant
				ShaderVariantFlags variant = (ShaderVariantFlags)0;

				if (o.TexDiffuse != null)
				{
					Device.BindTexture(o.TexDiffuse, 0);
					variant |= ShaderVariantFlags.Colormap;
				}
				if (o.TexNormal != null)
				{
					Device.BindTexture(o.TexNormal, 1);
					variant |= ShaderVariantFlags.Normalmap;
				}
				if (o.TexAlpha != null)
				{
					Device.BindTexture(o.TexAlpha, 2);
					variant |= ShaderVariantFlags.AlphaTest;
				}

				// Bind the pipeline with the proper shader
				Device.BindPipeline(variantArray[(int)variant]);

				// Set uniforms
				Device.ShaderVertex.SetUniformF("projection", projection);
				if (!isGbuffer)
				{
					Device.ShaderFragment.SetUniformF("sunDirection", scene.SunDirection);
					Device.ShaderFragment.SetUniformF("sunLight", scene.SunLight);
					Device.ShaderFragment.SetUniformF("exposure", scene.Exposure);
				}
				Device.ShaderVertex.SetUniformF("meshTransform", o.Transform);
				Device.ShaderFragment.SetUniformF("materialColor", o.Color);

				// Draw
				o.Mesh.Draw(Device);
			}

			// Reset state after rendering
			Device.BindTexture(TextureTarget.Texture2D, 0, 0);
			Device.BindTexture(TextureTarget.Texture2D, 0, 1);
			Device.BindTexture(TextureTarget.Texture2D, 0, 2);

			GL.BindSampler(0, 0);
			GL.BindSampler(1, 0);
			GL.BindSampler(2, 0);
		}

		/// <summary>
		/// Draws the wireframe outline of scene geometry.
		/// </summary>
		/// <param name="scene">Scene definition.</param>
		/// <param name="projection">Projection matrix to use while rendering.</param>
		public void DrawWireframe(Scene scene, Matrix4 projection)
		{
			Device.BindPipeline(_psoMeshWireframe);

			Device.ShaderVertex.SetUniformF("projection", projection);

			Device.ShaderFragment.SetUniformF("sunDirection", scene.SunDirection);
			Device.ShaderFragment.SetUniformF("sunLight", scene.SunLight);
			Device.ShaderFragment.SetUniformF("exposure", scene.Exposure);

			Device.ShaderFragment.SetUniformF("materialColor", new Vector4(0.0f));

			foreach (var o in scene.WorldObjects)
			{
				if (o.Mesh.IndexCount == 0)
					continue;

				Device.ShaderVertex.SetUniformF("meshTransform", o.Transform);

				o.Mesh.Draw(Device);
			}
		}
	}
}
