﻿using GlobCore;
using OpenTK.Mathematics;
using System;
using System.Collections.Generic;
using System.Text;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Describes a single renderable object - mesh, transform, color and a set of textures.
	/// </summary>
	class SceneObject
	{
		public Matrix4 Transform = Matrix4.Identity;
		public VertexBufferSet Mesh = null;
		public Texture2D TexDiffuse;
		public Texture2D TexAlpha;
		public Texture2D TexNormal;
		public Vector4 Color = Vector4.One;
	}
}
