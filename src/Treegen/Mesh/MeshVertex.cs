﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Stores data for a single vertex. This struct can be directly uploaded to the GPU for rendering.
	/// </summary>
	[StructLayout(LayoutKind.Explicit)]
	public struct MeshVertex
	{
		public static int Size = 60;

		[FieldOffset(0)]
		public Vector3 Position;

		[FieldOffset(12)]
		public Vector3 Normal;

		[FieldOffset(24)]
		public Vector4 Color;

		[FieldOffset(40)]
		public Vector2 TexCoords;

		[FieldOffset(48)]
		public Vector3 Tangent; // Unused

		public MeshVertex(Vector3 position, Vector3 normal, Vector4 color, Vector2 texCoords, Vector3 tangent)
		{
			Position = position;
			Normal = normal;
			Color = color;
			TexCoords = texCoords;
			Tangent = tangent;
		}
	}
}
