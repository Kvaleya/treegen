﻿using System;
using System.IO;

namespace TreeEditor
{
	class Program
	{
		static void Main(string[] args)
		{
			if(!Directory.Exists(ViewWindow.ShadersPath))
			{
				Console.WriteLine($"Shaders directory {ViewWindow.ShadersPath} not found!");
				Console.WriteLine("Try running the program from the root directory of the repository.");
				Console.ReadKey();
				return;
			}

			Editor ed = new Editor();
			ed.Run();
		}
	}
}
