﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	public enum TreeRandomType
	{
		Generic,
		BranchHormone,
		BranchType,
	}

	/// <summary>
	/// Stores random generators that can be used by growth modules
	/// Assumed usage:
	/// All tree vertices in a single branch share the same TreeRandomRepository (and the random generators inside)
	/// Assumes that once a tree vertex is completed, the generators will only be used by subsequent vertices
	/// When new branches split of current one, a new set of generators is created and seeded using a otherwise inaccesible generator
	/// </summary>
	public class TreeRandomRepository
	{
		public static readonly int RandomCount = Enum.GetNames(typeof(TreeRandomType)).Length;

		SimpleRandom _seeding;
		SimpleRandom[] _randoms;

		private SimpleRandom this[TreeRandomType type]
		{
			get { return _randoms[(int)type]; }
		}

		public double NextFloat(TreeRandomType type)
		{
			return this[type].NextFloat();
		}

		public double NextDouble(TreeRandomType type)
		{
			return this[type].NextDouble();
		}

		public int Next(TreeRandomType type, int minInclusive, int maxExclusive)
		{
			return this[type].Next(minInclusive, maxExclusive);
		}

		/// <summary>
		/// Returns a non-negative random integer
		/// </summary>
		public int Next(TreeRandomType type)
		{
			return this[type].Next();
		}

		/// <summary>
		/// Returns true with the specified probability
		/// </summary>
		public bool Attempt(TreeRandomType type, double probability)
		{
			return this[type].Attempt(probability);
		}

		private TreeRandomRepository()
		{
			_randoms = new SimpleRandom[RandomCount];
		}

		public TreeRandomRepository CreateForBranch()
		{
			return CreateNew(_seeding.Next());
		}

		public static TreeRandomRepository CreateNew(int seed)
		{
			TreeRandomRepository repo = new TreeRandomRepository();

			// Seeding generator uses the seed directly
			repo._seeding = new SimpleRandom(seed);

			// Every other generator uses the seed XORed with the type name hash
			// That way, even if new generator types are added, the old types remain unaffected
			for (int i = 0; i < RandomCount; i++)
			{
				int localseed = seed;

				unchecked
				{
					localseed = localseed ^ SimpleRandom.HashString(((TreeRandomType)i).ToString());
				}

				repo._randoms[i] = new SimpleRandom(localseed);
			}

			return repo;
		}
	}
}
