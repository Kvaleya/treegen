#version 430

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec2 screenPos;

uniform vec3 ray00;
uniform vec3 ray01;
uniform vec3 ray10;
uniform vec3 ray11;

void main()
{
	vec3 ray = mix(mix(ray00, ray10, screenPos.x), mix(ray01, ray11, screenPos.x), screenPos.y);
	ray = normalize(ray);

	vec3 color = vec3(0.7, 0.8, 1.0);
	
	// A "normal" sky
	color = mix(ray * 0.5 + 0.5, color, 0.9);
	
	color = mix(vec3(0.85, 0.90, 0.95), color, clamp((ray.y - 0.05) / 0.8, 0.0, 1.0));
	
	outColor = vec4(color, 1.0);
	//outColor = vec4(0.0, 0.0, 0.0, 1.0);
}
