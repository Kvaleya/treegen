﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Handles tree graph voxelization. Currently unused, but left in for possible future implementation.
	/// </summary>
	public static class Voxelizer
	{
		public const byte VoxelTrunk = 1;
		public const byte VoxelLeaf = 2;
		const float LeafRadius = 1f / 12;

		/// <summary>
		/// Voxelizes the tree.
		/// Returns voxels encoded in a byte array, one byte per voxel, and size of the voxel model. Value 0 is empty, value 1 is trunk, value 2 is leaf.
		/// Length of the array is equal to size.X*size.Y*size.Z. Voxels are stored first along X, then along Y, then along Z.
		/// </summary>
		/// <param name="root">Root vertex of the tree to voxelize</param>
		/// <param name="voxelSize">Size of a voxel</param>
		/// <returns>A tuple of voxel bytes and voxel model size</returns>
		public static (byte[], Vector3i) Voxelize(TreeVertex root, float voxelSize)
		{
			const int borderVoxels = 4;

			int leafBorder = (int)Math.Ceiling(LeafRadius / voxelSize);

			// Get tree AABB
			var (aabbMin, aabbMax) = GetTreeAABB(root);

			// Compute voxel model extents including border and model size
			Vector3i voxelMin = new Vector3i(0);
			Vector3i voxelMax = new Vector3i(0);

			for (int i = 0; i < 3; i++)
			{
				voxelMin[i] = (int)Math.Floor(aabbMin[i] / voxelSize) - borderVoxels - leafBorder;
				voxelMax[i] = (int)Math.Floor(aabbMax[i] / voxelSize) + borderVoxels + leafBorder;
			}

			Vector3i modelSize = voxelMax - voxelMin + new Vector3i(1);

			// Init voxel array
			long numVoxels = modelSize.X * modelSize.Y * modelSize.Z;

			if (modelSize.X > 256 || modelSize.Y > 256 || modelSize.Z > 256)
			{
				throw new ArgumentException("Resulting voxel model is too large!", nameof(voxelSize));
			}

			byte[] voxels = new byte[(int)numVoxels];

			// Recursive voxelization
			VoxelizeBranch(root, ref voxels, Vector3.Zero, root.Radius, voxelMin, modelSize, voxelSize);

			return (voxels, modelSize);
		}

		static void VoxelizeBranch(TreeVertex vertex, ref byte[] voxels, Vector3 origin, float prevRadius, Vector3i voxelMin, Vector3i modelSize, float voxelSize)
		{
			int leafBorder = (int)Math.Ceiling(LeafRadius / voxelSize);

			void WriteVoxel(ref byte[] vox, int x, int y, int z, bool leaf = false)
			{
				if(x < 0 || y < 0 || z < 0 ||
					x >= modelSize.X || y >= modelSize.Y || z >= modelSize.Z)
				{
					throw new Exception($"Voxel coordinates are out of range! x={x} y={y} z={z}, size={modelSize}");
				}

				int index = x + y * modelSize.X + z * modelSize.X * modelSize.Y;
				
				if(leaf)
				{
					// Trunk voxels override leaf voxels
					if (vox[index] == 0)
					{
						vox[index] = VoxelLeaf;
					}
				}
				else
				{
					//if(vox[index] == 0)
					{
						vox[index] = VoxelTrunk;
					}
				}
			}

			bool isLeaf = vertex.Children.Count == 0;

			// Special case: vertex is leaf
			
			if (isLeaf)
			{
				Vector3 leafPos = origin + vertex.Forward * vertex.Length;

				Vector3i voxel = new Vector3i(0);

				for(int i = 0; i < 3; i++)
				{
					voxel[i] = (int)Math.Floor(leafPos[i] / voxelSize) - voxelMin[i];
				}



				// Iterate over all voxels in leaf AABB
				for (int z = voxel.Z - leafBorder; z <= voxel.Z + leafBorder; z++)
				{
					for (int y = voxel.Y - leafBorder; y <= voxel.Y + leafBorder; y++)
					{
						for (int x = voxel.X - leafBorder; x <= voxel.X + leafBorder; x++)
						{
							// Center of current voxel
							Vector3 pos = (new Vector3i(x, y, z) + voxelMin + new Vector3(0.5f)) * voxelSize;

							if ((pos - leafPos).Length < LeafRadius)
							{
								WriteVoxel(ref voxels, x, y, z, true);
							}
						}
					}
				}
			}
			

			// Determine the AABB of current branch
			Vector3i areaMin = new Vector3i(int.MaxValue);
			Vector3i areaMax = new Vector3i(int.MinValue);

			Vector3 destination = origin + vertex.Forward * vertex.Length;

			float maxRadius = Math.Max(vertex.Radius, prevRadius);

			const int rasterBorder = 2;

			for(int i = 0; i < 3; i++)
			{
				float min = Math.Min(destination[i] - maxRadius, origin[i] - maxRadius);
				float max = Math.Max(destination[i] + maxRadius, origin[i] + maxRadius);

				areaMin[i] = (int)(min / voxelSize) - voxelMin[i] - rasterBorder;
				areaMax[i] = (int)(max / voxelSize) - voxelMin[i] + rasterBorder;
			}

			// Iterate over all voxels in branch AABB
			for(int z = areaMin.Z; z <= areaMax.Z; z++)
			{
				for (int y = areaMin.Y; y <= areaMax.Y; y++)
				{
					for (int x = areaMin.X; x <= areaMax.X; x++)
					{
						// For each voxel, compute its distance to branch line
						// compare to branch radius at this point
						// fill voxel if distance < radius + voxelSize * magic_constant

						// Center of current voxel
						Vector3 pos = (new Vector3i(x, y, z) + voxelMin + new Vector3(0.5f)) * voxelSize;

						// Find the closest point on branch line to this vertex
						float distAlongBranch = Vector3.Dot(pos, vertex.Forward) - Vector3.Dot(origin, vertex.Forward);
						distAlongBranch = MathUtils.Clamp(distAlongBranch, 0, vertex.Length);
						Vector3 closest = origin + vertex.Forward * distAlongBranch;

						// Radius of branch at its nearest point to voxel
						float localRadius = MathUtils.Lerp(prevRadius, vertex.Radius, distAlongBranch / vertex.Length);

						// Distance of voxel to branch
						float voxelDist = (pos - closest).Length;

						if(voxelDist < localRadius + voxelSize * 0.5f)
						{
							WriteVoxel(ref voxels, x, y, z, isLeaf);
						}
					}
				}
			}

			// Recurse to all children
			foreach(var c in vertex.Children)
			{
				VoxelizeBranch(c, ref voxels, destination, vertex.Radius, voxelMin, modelSize, voxelSize);
			}
		}

		/// <summary>
		/// Gets the AABB of a tree.
		/// </summary>
		/// <param name="root">Tree root vertex</param>
		/// <returns>Tuple of (min, max) AABB corners</returns>
		static (Vector3, Vector3) GetTreeAABB(TreeVertex root)
		{
			Vector3 min = new Vector3(-root.Radius);
			Vector3 max = new Vector3(root.Radius);

			GetTreeAABB(root, Vector3.Zero, ref min, ref max);

			return (min, max);
		}

		static void GetTreeAABB(TreeVertex vertex, Vector3 origin, ref Vector3 min, ref Vector3 max)
		{
			Vector3 pos = origin + vertex.Forward * vertex.Length;
			
			for(int i = 0; i < 3; i++)
			{
				min[i] = Math.Min(min[i], pos[i] - vertex.Radius);
				max[i] = Math.Max(max[i], pos[i] + vertex.Radius);
			}

			foreach(var c in vertex.Children)
			{
				GetTreeAABB(c, pos, ref min, ref max);
			}
		}
	}
}
