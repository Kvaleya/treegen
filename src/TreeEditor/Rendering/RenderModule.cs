﻿using System;
using System.Collections.Generic;
using System.Text;
using GlobCore;

namespace TreeEditor.Rendering
{
	abstract class RenderModule
	{
		protected readonly RenderContext RenderContext;
		protected Device Device { get { return RenderContext.Device; } }

		public RenderModule(RenderContext renderContext)
		{
			RenderContext = renderContext;
		}

		public virtual void OnResize(int displayWidth, int displayHeight, int renderWidth, int renderHeight)
		{

		}
	}
}
