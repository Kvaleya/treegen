﻿using System;

namespace Treegen
{
	/// <summary>
	/// Inicializes root vertex with starting values
	/// </summary>
	class RootModule : GrowModule
	{
		public override double Order => -1.0;

		[GrowParamSlider(0, 1)]
		public float Dominance { get; set; } = 1.0f;

		public override void Apply(GrowContext context, TreeVertex vertex)
		{
			if(vertex == context.Root)
			{
				vertex[TreeAttribute.Dominance] = Dominance;
			}
		}
	}
}
