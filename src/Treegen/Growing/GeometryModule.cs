﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using Treegen;

namespace Treegen
{
	/// <summary>
	/// Parses age, pitch and roll attributes and generates branch length, radius and direciton
	/// </summary>
	public class GeometryModule : GrowModule
	{
		public override double Order => 99.0;

		public float RadiusScale { get; set; } = 0.2f;
		public float LengthScale { get; set; } = 0.5f;

		[GrowParamSlider(0.05, 0.5)]
		public float LengthPow { get; set; } = 0.35f;

		[GrowParamSlider(0f, 0.999f)]
		public float BranchRadiusTrailing { get; set; } = 0.5f;

		public override void Apply(GrowContext context, TreeVertex vertex)
		{
			double r = Math.Sqrt(vertex[TreeAttribute.Age]) * RadiusScale;

			// Trailing radius makes child branches use radius closer to parent for first few segments to create smoother transition
			double trailingRadius = Math.Max(r, MathUtils.Lerp(r, vertex[TreeAttribute.LastBranchRadius], BranchRadiusTrailing));

			// Only take trailingRadius into consideration if it is different from correct radius by more than 3%
			if (trailingRadius / r > 1.03)
			{
				r = trailingRadius;
				vertex[TreeAttribute.LastBranchRadius] = (float)trailingRadius;
			}
			else
			{
				vertex[TreeAttribute.LastBranchRadius] = 0;
			}

			vertex.Radius = (float)r;
			vertex.Length = (float)Math.Pow(vertex[TreeAttribute.Age], LengthPow) * LengthScale;

			// Gravitropism
			float g = vertex[TreeAttribute.Gravitropism] * (1 - vertex[TreeAttribute.Stiffness]);
			vertex.SetOrientation(vertex.Forward - Vector3.UnitY * g, vertex.Right);
		}
	}
}
