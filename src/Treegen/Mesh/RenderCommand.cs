﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// A render command for polyplane rendering.
	/// </summary>
	public class RenderCommand
	{
		public Mesh MeshTrunk;
		public Mesh MeshLeaves;

		/// <summary>
		/// Matrix that moves the mesh in front of the camera.
		/// </summary>
		public Matrix4 Transform;

		/// <summary>
		/// Range of view. Anything between center+range and center-range will be visible, in each axis, as they are defined by Up and Right.
		/// </summary>
		public Vector3 Range;

		public Vector2i ViewportMin;
		public Vector2i ViewportSize;
	}
}
