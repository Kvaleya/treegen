﻿using GlobCore;
using OpenTK.Graphics.OpenGL;
using System;
using System.Collections.Generic;
using System.Text;
using Treegen;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Handles vertex buffer and index buffer of a mesh.
	/// </summary>
	class VertexBufferSet : IDisposable
	{
		int _vbo = 0;
		int _ebo = 0;
		int _indexCount = 0;
		VertexBufferSource _source;

		public int IndexCount => _indexCount;

		/// <summary>
		/// Sets the vertex and index buffer contents from a list of vertices and indices.
		/// </summary>
		public void UpdateFromMesh(List<MeshVertex> vertices, List<int> indices)
		{
			TryDelete();

			if (vertices.Count > 0)
			{
				_vbo = GlobCore.Utils.CreateBuffer(
					BufferTarget.ArrayBuffer,
					new IntPtr(MeshVertex.Size * vertices.Count),
					vertices.GetInternalArray(),
					BufferStorageFlags.None,
					"Mesh VBO");
			}
			
			if(indices.Count > 0)
			{
				_ebo = GlobCore.Utils.CreateBuffer(
					BufferTarget.ElementArrayBuffer,
					new IntPtr(4 * indices.Count),
					indices.GetInternalArray(),
					BufferStorageFlags.None,
					"Mesh EBO");
			}

			_indexCount = indices.Count;

			_source = new VertexBufferSource(0, new VertexBufferBinding(_vbo, 0, MeshVertex.Size, 0));
		}

		/// <summary>
		/// Draws the mesh. Binds the required buffers and dispatches a draw call.
		/// </summary>
		/// <param name="device"></param>
		public void Draw(Device device)
		{
			if (_indexCount <= 0)
				return;

			GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
			device.BindVertexBufferSource(_source);
			GL.DrawElements(PrimitiveType.Triangles, _indexCount, DrawElementsType.UnsignedInt, IntPtr.Zero);
		}

		public void Dispose()
		{
			TryDelete();
		}

		void TryDelete()
		{
			if (_vbo > 0)
				GL.DeleteBuffer(_vbo);
			if (_ebo > 0)
				GL.DeleteBuffer(_ebo);
			_vbo = 0;
			_ebo = 0;
			_indexCount = 0;
		}
	}
}
