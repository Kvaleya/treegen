﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using ImGuiNET;

namespace TreeEditor.UserInterface
{
	// Adapted from:
	// https://www.gitmemory.com/issue/mellinoe/ImGui.NET/22/488557014
	// https://gist.github.com/prime31/91d1582624eb2635395417393018016e
	// Original authors seem to be users mellinoe and prime31 (see the gitmemory link)
	class FilePicker
	{
		const uint ColorYellowPacked = 0xff00ffffu;

		static readonly Dictionary<object, FilePicker> _filePickers = new Dictionary<object, FilePicker>();

		public string RootFolder;
		public string CurrentFolder;
		public string SelectedFile = "";
		public List<string> AllowedExtensions;
		public bool OnlyAllowFolders;
		public bool SaveFileDialog = false;
		string _writtenText = "";
		bool _fileHasPath = true;
		bool _overwritePopup = false;

		public static FilePicker GetFolderPicker(object o, string startingPath)
			=> GetFilePicker(o, startingPath, null, true);

		public static FilePicker GetFilePicker(object o, string startingPath, string searchFilter = null,
			bool onlyAllowFolders = false, bool saveFileDialog = false)
		{
			if (File.Exists(startingPath))
			{
				startingPath = new FileInfo(startingPath).DirectoryName;
			}
			else if (string.IsNullOrEmpty(startingPath) || !Directory.Exists(startingPath))
			{
				startingPath = Environment.CurrentDirectory;
				if (string.IsNullOrEmpty(startingPath))
					startingPath = AppContext.BaseDirectory;
			}

			if (!_filePickers.TryGetValue(o, out FilePicker fp))
			{
				fp = new FilePicker();
				fp.RootFolder = startingPath;
				fp.CurrentFolder = startingPath;
				fp.OnlyAllowFolders = onlyAllowFolders;
				fp.SaveFileDialog = saveFileDialog;

				if (searchFilter != null)
				{
					if (fp.AllowedExtensions != null)
						fp.AllowedExtensions.Clear();
					else
						fp.AllowedExtensions = new List<string>();

					fp.AllowedExtensions.AddRange(searchFilter.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries));
				}

				_filePickers.Add(o, fp);
			}

			return fp;
		}

		public static void RemoveFilePicker(object o) => _filePickers.Remove(o);

		public bool Draw()
		{
			ImGui.Text("Current Folder: " + Path.GetFileName(RootFolder) + CurrentFolder.Replace(RootFolder, ""));
			bool result = false;
			bool close = false;

			// The file tree panel
			if (ImGui.BeginChildFrame(1, new System.Numerics.Vector2(400, 400)))
			{
				var di = new DirectoryInfo(CurrentFolder);
				if (di.Exists)
				{
					if (di.Parent != null && CurrentFolder != RootFolder)
					{
						ImGui.PushStyleColor(ImGuiCol.Text, ColorYellowPacked);
						if (ImGui.Selectable("../", false, ImGuiSelectableFlags.DontClosePopups))
							CurrentFolder = di.Parent.FullName;

						ImGui.PopStyleColor();
					}

					var fileSystemEntries = GetFileSystemEntries(di.FullName);
					foreach (var fse in fileSystemEntries)
					{
						if (Directory.Exists(fse))
						{
							var name = Path.GetFileName(fse);
							ImGui.PushStyleColor(ImGuiCol.Text, ColorYellowPacked);
							if (ImGui.Selectable(name + "/", false, ImGuiSelectableFlags.DontClosePopups))
								CurrentFolder = fse;
							ImGui.PopStyleColor();
						}
						else
						{
							var name = Path.GetFileName(fse);
							bool isSelected = SelectedFile == fse;
							if (ImGui.Selectable(name, isSelected, ImGuiSelectableFlags.DontClosePopups))
							{
								SelectedFile = fse;
								_fileHasPath = true;
								_writtenText = Path.GetFileNameWithoutExtension(fse);
							}

							if (ImGui.IsMouseDoubleClicked(0))
							{
								result = true;
								ImGui.CloseCurrentPopup();
							}
						}
					}
				}
			}
			ImGui.EndChildFrame();

			string finishText = SaveFileDialog ? "Save" : "Open";

			if (SaveFileDialog)
			{
				// Controls for saving
				const uint maxLength = 50;

				if(ImGui.InputText("", ref _writtenText, maxLength))
				{
					SelectedFile = _writtenText;
					_fileHasPath = false;
				}

				if(AllowedExtensions != null && AllowedExtensions.Count > 0)
				{
					ImGui.SameLine();
					ImGui.Text(AllowedExtensions[0]);
				}
				
				ImGui.SameLine();
				if (ImGui.Button(finishText))
				{
					result = true;
					close = true;
				}

				ImGui.SameLine();
				if (ImGui.Button("Cancel"))
				{
					result = false;
					close = true;
				}
			}
			else
			{
				// Controls for loading
				if (ImGui.Button("Cancel"))
				{
					result = false;
					close = true;
				}

				if (OnlyAllowFolders)
				{
					ImGui.SameLine();
					if (ImGui.Button(finishText))
					{
						result = true;
						SelectedFile = CurrentFolder;
						close = true;
					}
				}
				else if (SelectedFile != null)
				{
					ImGui.SameLine();
					if (ImGui.Button(finishText))
					{
						result = true;
						close = true;
					}
				}
			}

			if(result == true && !_fileHasPath)
			{
				// When saving a file of custom name, append current folder path
				SelectedFile = Path.Combine(CurrentFolder, SelectedFile);

				// And try to add proper extension
				if (!Path.HasExtension(SelectedFile) && AllowedExtensions != null && AllowedExtensions.Count > 0)
				{
					SelectedFile += AllowedExtensions[0];
				}
			}

			const string overwritePopup = "Overwrite?";

			if(result == true && SaveFileDialog == true && File.Exists(SelectedFile))
			{
				_overwritePopup = true;
				result = false;
				close = false;
				ImGui.OpenPopup(overwritePopup);
			}

			// Handles a popup asking the user whether to overwrite an existing file
			if(_overwritePopup)
			{
				if (ImGui.BeginPopupModal(overwritePopup, ref _overwritePopup, ImGuiWindowFlags.NoTitleBar))
				{
					ImGui.Text("Overwrite file " + Path.GetFileName(SelectedFile) + "?");

					bool yes = ImGui.Button("Yes");
					ImGui.SameLine();
					bool no = ImGui.Button("No");

					if(yes)
					{
						result = true;
						close = true;
					}

					if(no)
					{
						_overwritePopup = false;
					}

					if(yes || no)
					{
						ImGui.CloseCurrentPopup();
					}

					ImGui.EndPopup();
				}
			}

			if(close)
			{
				ImGui.CloseCurrentPopup();
			}

			return result;
		}

		List<string> GetFileSystemEntries(string fullName)
		{
			var files = new List<string>();
			var dirs = new List<string>();

			foreach (var fse in Directory.GetFileSystemEntries(fullName, ""))
			{
				if (Directory.Exists(fse))
				{
					dirs.Add(fse);
				}
				else if (!OnlyAllowFolders)
				{
					if (AllowedExtensions != null)
					{
						var ext = Path.GetExtension(fse);
						if (AllowedExtensions.Contains(ext))
							files.Add(fse);
					}
					else
					{
						files.Add(fse);
					}
				}
			}

			var ret = new List<string>(dirs);
			ret.AddRange(files);

			return ret;
		}
	}
}
