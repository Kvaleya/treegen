﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Linq;
using System.IO;
using ImGuiNET;
using Treegen;
using OpenTK.Windowing.GraphicsLibraryFramework;

namespace TreeEditor.UserInterface
{
	interface IImguiValueRef
	{
		void OnFinished();
	}

	class ImguiValueRef<T> : IImguiValueRef
	{
		public T Data;
		Action<T> _onFinished;

		public ImguiValueRef(T data, Action<T> onFinished)
		{
			Data = data;
			_onFinished = onFinished;
		}

		public void OnFinished()
		{
			_onFinished(Data);
		}
	}

	class MenuGenerator
	{
		enum FileDialogState
		{
			Closed,
			Load,
			Save,
			Export,
			LoadTex,
		}

		enum TextureType
		{
			Trunk,
			TrunkNormal,
			Leaf,
			LeafNormal,
			LeafAlpha,
		}

		const string _fileDialogId = "open-file";

		List<IImguiValueRef> _imguiValues = new List<IImguiValueRef>();
		Editor _editor;

		public bool RenderWireframe = false;
		public bool AllowMouseLook
		{
			get
			{
				return !_fileDialogOpen;
			}
		}

		bool _mesherMenuOpen = false;
		bool _instancesMenuOpen = false;
		bool _fileDialogOpen = false;
		bool _texcoordMenuOpen = false;
		bool _statsMenuOpen = false;
		bool _openPopup = false;
		FileDialogState _fileDialogState = FileDialogState.Closed;
		TextureType _texType = TextureType.Trunk;

		public MenuGenerator(Editor e)
		{
			_editor = e;
		}

		void ActionRebuild()
		{
			_editor.Tree.UpdateAll();
		}

		void ActionSave()
		{
			if (string.IsNullOrEmpty(_editor.Tree.Filename))
			{
				OpenFileDialog(FileDialogState.Save);
			}
			else
			{
				_editor.FileSave();
			}
		}

		void ActionSaveAs()
		{
			OpenFileDialog(FileDialogState.Save);
		}

		void ActionLoad()
		{
			OpenFileDialog(FileDialogState.Load);
		}

		void ActionExport()
		{
			OpenFileDialog(FileDialogState.Export);
		}

		void ActionLoadTexture(TextureType type)
		{
			_texType = type;
			OpenFileDialog(FileDialogState.LoadTex);
		}

		public void UpdateControls(Input input)
		{
			if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.R))
			{
				ActionRebuild();
			}
			if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.S))
			{
				ActionSave();
			}
			if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.L))
			{
				ActionLoad();
			}
			if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.E))
			{
				ActionExport();
			}
			if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.M))
			{
				_mesherMenuOpen = true;
			}
			if(input.IsKeyPressed(Keys.Escape))
			{
				ClosePopup();
			}

			// TODO: refactor this elsewhere
			//if (input.IsKeyDown(Keys.LeftControl) && input.IsKeyPressed(Keys.V))
			//{
			//	using(BinaryWriter bw = new BinaryWriter(File.OpenWrite("test.vox")))
			//	{
			//		var (bytes, size) = Voxelizer.Voxelize(_editor.Tree.CreateTree(), 1f / 16);
			//		VoxWriter.WriteVox(bw, bytes, size);
			//	}
			//}
		}

		public void AfterImgui()
		{
			foreach (var v in _imguiValues)
			{
				v.OnFinished();
			}
		}

		#region Menus

		public void ImGuiLayout()
		{
			_imguiValues.Clear();
			MainMenuLayout();
			GrowerMenuLayout();
			MesherSettingsLayout();
			MeshStatsLayout();
			TreeInstancesLayout();
			TexcoordMenuLayout();

			if (_openPopup)
			{
				_openPopup = false;
				ImGui.OpenPopup(_fileDialogId);
			}

			FileDialog();
		}

		void GrowerMenuLayout()
		{
			ImGui.SetNextWindowSize(new System.Numerics.Vector2(620, 600), ImGuiCond.FirstUseEver);
			ImGui.Begin("Growth modules");

			foreach (var m in _editor.Tree.State.Grower.Modules)
			{
				GenModuleMenu(m);
			}

			ImGui.End();
		}

		void MainMenuLayout()
		{
			if (ImGui.BeginMainMenuBar())
			{
				if (ImGui.BeginMenu("File"))
				{
					if (ImGui.MenuItem("Save", "Ctrl+S"))
					{
						ActionSave();
					}
					if (ImGui.MenuItem("Save as"))
					{
						ActionSaveAs();
					}
					if (ImGui.MenuItem("Load", "Ctrl+L"))
					{
						ActionLoad();
					}
					if (ImGui.MenuItem("Export", "Ctrl+E"))
					{
						ActionExport();
					}

					ImGui.EndMenu();
				}

				if (ImGui.BeginMenu("Tools"))
				{
					if (ImGui.MenuItem("Rebuild trees", "Ctrl+R"))
					{
						ActionRebuild();
					}

					ImGui.MenuItem("Wireframe", "", ref RenderWireframe);

					if (ImGui.MenuItem("Mesher settings", "Ctrl+M"))
					{
						_mesherMenuOpen = true;
					}

					if (ImGui.MenuItem("Tree instances"))
					{
						_instancesMenuOpen = true;
					}

					if (ImGui.MenuItem("Mesh stats"))
					{
						_statsMenuOpen = true;
					}

					if (ImGui.MenuItem("Reset camera"))
					{
						_editor.Camera = new EditorCamera();
					}

					ImGui.EndMenu();
				}

				if (ImGui.BeginMenu("Textures"))
				{
					if (ImGui.MenuItem("Load trunk diffuse texture", ""))
					{
						ActionLoadTexture(TextureType.Trunk);
					}
					if (ImGui.MenuItem("Load trunk normal texture", ""))
					{
						ActionLoadTexture(TextureType.TrunkNormal);
					}
					if (ImGui.MenuItem("Load leaf diffuse texture", ""))
					{
						ActionLoadTexture(TextureType.Leaf);
					}
					if (ImGui.MenuItem("Load leaf normal texture", ""))
					{
						ActionLoadTexture(TextureType.LeafNormal);
					}
					if (ImGui.MenuItem("Load leaf alpha texture", ""))
					{
						ActionLoadTexture(TextureType.LeafAlpha);
					}
					if (ImGui.MenuItem("Select leaf texcoords", ""))
					{
						_texcoordMenuOpen = true;
					}

					ImGui.EndMenu();
				}

				ImGui.EndMainMenuBar();
			}
		}

		void OpenFileDialog(FileDialogState state)
		{
			_fileDialogState = state;
			_fileDialogOpen = true;
			_openPopup = true;
		}

		void MeshStatsLayout()
		{
			void DisplayMeshStats(MeshStats s, string prefix)
			{
				ImGui.Text(prefix + $"verts: {s.Vertices}");
				ImGui.Text(prefix + $"faces: {s.Faces}");
			}

			if (!_statsMenuOpen)
				return;

			ImGui.SetNextWindowSize(new System.Numerics.Vector2(450, 250), ImGuiCond.FirstUseEver);
			if (ImGui.Begin("Mesh stats", ref _statsMenuOpen))
			{
				var stats = _editor.Tree.GetStats();

				for(int i = 0; i < stats.Length; i++)
				{
					if(ImGui.TreeNode($"Stats of instance {i}"))
					{
						for (int lod = 0; lod < stats[i].Length; lod++)
						{
							if(ImGui.TreeNode($"LOD {lod}"))
							{
								DisplayMeshStats(stats[i][lod].Trunk, "Trunk ");
								DisplayMeshStats(stats[i][lod].Leaves, "Leaf ");
								DisplayMeshStats(stats[i][lod].Polyplanes, "Polyplane ");
								DisplayMeshStats(stats[i][lod].Total, "Total ");

								ImGui.TreePop();
							}
						}

						ImGui.TreePop();
					}
				}
			}
			ImGui.End();
		}

		void TreeInstancesLayout()
		{
			if (!_instancesMenuOpen)
				return;

			ImGui.SetNextWindowSize(new System.Numerics.Vector2(450, 250), ImGuiCond.FirstUseEver);
			if (ImGui.Begin("Tree instances", ref _instancesMenuOpen))
			{
				var list = _editor.Tree.State.Instances;

				int insert = -1;

				// Generate lod params
				for (int i = 0; i < list.Count; i++)
				{
					ImGui.Separator();

					ImGui.Text($"Instance {i}");

					var instance = list[i];

					foreach (var property in typeof(TreeInstanceSettings).GetProperties())
					{
						_imguiValues.Add(GenGenericValueField(instance, property, $"Ins{i}: "));
					}

					// Imgui seems to consider elements with identical properties (such as label)
					// to be completely identical, thus the LOD{i} prefix/postfix for all of them.
					if (ImGui.Button($"Duplicate instance{i}"))
					{
						insert = i;
					}

					if (i > 0)
					{
						if (ImGui.Button($"Delete instance{i}"))
						{
							list.RemoveAt(i);
						}
					}
				}

				if (insert >= 0)
				{
					list.Insert(insert, list[insert].Clone());
				}
			}
			ImGui.End();
		}

		void MesherSettingsLayout()
		{
			if (!_mesherMenuOpen)
				return;

			ImGui.SetNextWindowSize(new System.Numerics.Vector2(450, 250), ImGuiCond.FirstUseEver);
			if (ImGui.Begin("Mesh generation settings", ref _mesherMenuOpen))
			{
				var mparams = _editor.Tree.State.MesherParams;

				if (ImGui.TreeNode("Common parameters"))
				{
					// Generate generic params first
					foreach (var property in typeof(MesherParams).GetProperties())
					{
						if (property.PropertyType == typeof(List<MesherLodParams>) ||
							property.PropertyType == typeof(MesherLeafParams))
						{
							continue;
						}
						else
						{
							_imguiValues.Add(GenGenericValueField(mparams, property));
						}
					}

					// Generate leaf params
					foreach (var property in typeof(MesherLeafParams).GetProperties())
					{
						if (property.PropertyType == typeof(JsonableVector2[]) || property.PropertyType == typeof(JsonableVector2))
							continue;

						_imguiValues.Add(GenGenericValueField(mparams.LeafParams, property));
					}

					ImGui.TreePop();
				}

				int insert = -1;

				// Generate lod params
				for (int i = 0; i < mparams.LodParams.Count; i++)
				{
					if (ImGui.TreeNode($"LOD {i}"))
					{
						var lod = mparams.LodParams[i];

						foreach (var property in typeof(MesherLodParams).GetProperties())
						{
							if (property.PropertyType == typeof(MesherTrunkParams))
							{
								foreach (var trunkParamsProperty in typeof(MesherTrunkParams).GetProperties())
								{
									_imguiValues.Add(GenGenericValueField(lod.TrunkParams, trunkParamsProperty, $"LOD{i}: "));
								}
							}
							else
							{
								_imguiValues.Add(GenGenericValueField(lod, property, $"LOD{i}: "));
							}
						}

						// Imgui seems to consider elements with identical properties (such as label)
						// to be completely identical, thus the LOD{i} prefix/postfix for all of them.
						if (ImGui.Button($"Duplicate LOD{i}"))
						{
							insert = i;
						}

						if (i > 0)
						{
							if (ImGui.Button($"Delete LOD{i}"))
							{
								mparams.LodParams.RemoveAt(i);
							}
						}

						ImGui.TreePop();
					}
				}

				if(insert >= 0)
				{
					mparams.LodParams.Insert(insert, mparams.LodParams[insert].Clone());
				}
			}
			ImGui.End();
		}

		void TexcoordMenuLayout()
		{
			if (!_texcoordMenuOpen)
				return;

			ImGui.SetNextWindowSize(new System.Numerics.Vector2(450, 650), ImGuiCond.FirstUseEver);
			if (ImGui.Begin("Leaf texcoord selection", ref _texcoordMenuOpen))
			{
				const int size = 400;

				if (_editor.Tree.LeafTextureImguiID != IntPtr.Zero)
				{
					var leafParams = _editor.Tree.State.MesherParams.LeafParams;

					void makeTcEditBox(int index, JsonableVector2 vec)
					{
						const int maxlength = 10;

						var x = new ImguiValueRef<string>(
							vec.X.ToString(CultureInfo.InvariantCulture),
							s =>
							{
								float v = 0;
								if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
									vec.X = v;
							});
						ImGui.InputText("X" + index.ToString(), ref x.Data, maxlength, ImGuiInputTextFlags.CharsDecimal);
						_imguiValues.Add(x);

						ImGui.SameLine();

						var y = new ImguiValueRef<string>(
							vec.Y.ToString(CultureInfo.InvariantCulture),
							s =>
							{
								float v = 0;
								if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
									vec.Y = v;
							});
						ImGui.InputText("Y" + index.ToString(), ref y.Data, maxlength, ImGuiInputTextFlags.CharsDecimal);
						_imguiValues.Add(y);
					}

					var pos = ImGui.GetCursorScreenPos();
					ImGui.Image(_editor.Tree.LeafTextureImguiID, new System.Numerics.Vector2(size, size), new System.Numerics.Vector2(0,0));
					var array = leafParams.LeafTexCoords.ToList();

					const uint red = 0xff0000ffu;
					const uint blue = 0xffff4411u;
					const float thickness = 2f;
					ImGui.GetWindowDrawList().AddLine(pos + array[0].ToNumerics() * size, pos + array[1].ToNumerics() * size, red, thickness);
					ImGui.GetWindowDrawList().AddLine(pos + array[1].ToNumerics() * size, pos + array[2].ToNumerics() * size, red, thickness);
					ImGui.GetWindowDrawList().AddLine(pos + array[2].ToNumerics() * size, pos + array[3].ToNumerics() * size, red, thickness);
					ImGui.GetWindowDrawList().AddLine(pos + array[3].ToNumerics() * size, pos + array[0].ToNumerics() * size, red, thickness);

					ImGui.PushItemWidth(150);

					makeTcEditBox(0, array[0]);
					makeTcEditBox(1, array[1]);
					makeTcEditBox(2, array[2]);
					makeTcEditBox(3, array[3]);

					ImGui.PopItemWidth();
				}
			}
			ImGui.End();
		}

		void GenModuleMenu(GrowModule module)
		{
			var moduleType = module.GetType();

			ImGui.Text(moduleType.Name);

			foreach (var m in moduleType.GetMembers())
			{
				if (m.MemberType != System.Reflection.MemberTypes.Property &&
					m.MemberType != System.Reflection.MemberTypes.Field)
					continue;

				if(m is PropertyInfo)
				{
					var p = m as PropertyInfo;
					if (p.SetMethod == null)
						continue;
				}

				var attribute = m.GetCustomAttribute<GrowParamSlider>(false);

				if(attribute != null)
				{
					_imguiValues.Add(GenSlider(module, attribute, m));
				}
				else
				{
					_imguiValues.Add(GenGenericValueField(module, m));
				}
			}
		}

		IImguiValueRef GenGenericValueField(object obj, MemberInfo member, string labelPrefix = "")
		{
			const int maxlength = 20;

			if (member is PropertyInfo)
			{
				var p = member as PropertyInfo;

				if (p.PropertyType == typeof(float))
				{
					var r = new ImguiValueRef<string>(
						((float)p.GetValue(obj)).ToString("0.00000", CultureInfo.InvariantCulture),
						s =>
						{
							float v = 0;
							if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
				if (p.PropertyType == typeof(double))
				{
					var r = new ImguiValueRef<string>(
						((double)p.GetValue(obj)).ToString("0.00000", CultureInfo.InvariantCulture),
						s =>
						{
							double v = 0;
							if (double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
				if (p.PropertyType == typeof(int))
				{
					var r = new ImguiValueRef<string>(
						((int)p.GetValue(obj)).ToString(CultureInfo.InvariantCulture),
						s =>
						{
							int v = 0;
							if (int.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
			}
			if (member is FieldInfo)
			{
				var p = member as FieldInfo;

				if (p.FieldType == typeof(float))
				{
					var r = new ImguiValueRef<string>(
						((float)p.GetValue(obj)).ToString("0.00000", CultureInfo.InvariantCulture),
						s =>
						{
							float v = 0;
							if (float.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
				if (p.FieldType == typeof(double))
				{
					var r = new ImguiValueRef<string>(
						((double)p.GetValue(obj)).ToString("0.00000", CultureInfo.InvariantCulture),
						s =>
						{
							double v = 0;
							if (double.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
				if (p.FieldType == typeof(int))
				{
					var r = new ImguiValueRef<string>(
						((int)p.GetValue(obj)).ToString(CultureInfo.InvariantCulture),
						s =>
						{
							int v = 0;
							if (int.TryParse(s, NumberStyles.Any, CultureInfo.InvariantCulture, out v))
								p.SetValue(obj, v);
						});
					ImGui.InputText(labelPrefix + member.Name, ref r.Data, maxlength);
					return r;
				}
			}

			throw new ArgumentException("Member is not a field or a property!", nameof(member));
		}

		IImguiValueRef GenSlider(GrowModule module, GrowParamSlider slider, MemberInfo member)
		{
			if (member is PropertyInfo)
			{
				var p = member as PropertyInfo;

				if (p.PropertyType == typeof(float))
				{
					var r = new ImguiValueRef<float>((float)p.GetValue(module), f => p.SetValue(module, f));
					ImGui.SliderFloat(member.Name, ref r.Data, (float)slider.Min, (float)slider.Max, "%f", ImGuiSliderFlags.None);
					return r;
				}
				if (p.PropertyType == typeof(int))
				{
					var r = new ImguiValueRef<int>((int)p.GetValue(module), f => p.SetValue(module, f));
					ImGui.SliderInt(member.Name, ref r.Data, (int)slider.Min, (int)slider.Max, "%d");
					return r;
				}
			}
			if (member is FieldInfo)
			{
				var p = member as FieldInfo;

				if (p.FieldType == typeof(float))
				{
					var r = new ImguiValueRef<float>((float)p.GetValue(module), f => p.SetValue(module, f));
					ImGui.SliderFloat(member.Name, ref r.Data, (float)slider.Min, (float)slider.Max, "%f", ImGuiSliderFlags.None);
					return r;
				}
				if (p.FieldType == typeof(int))
				{
					var r = new ImguiValueRef<int>((int)p.GetValue(module), f => p.SetValue(module, f));
					ImGui.SliderInt(member.Name, ref r.Data, (int)slider.Min, (int)slider.Max, "%d");
					return r;
				}
			}

			throw new ArgumentException("Member is not a field or a property!", nameof(member));
		}

		void FileDialog()
		{
			string file = "";

			switch (_fileDialogState)
			{
				case FileDialogState.Save:
					{
						if (DrawFileDialogMenu(save: true, ".json", ref file))
						{
							_editor.Tree.Filename = file;
							_editor.FileSave();
						}
						break;
					}
				case FileDialogState.Load:
					{
						if (DrawFileDialogMenu(save: false, ".json", ref file))
						{
							_editor.FileLoad(file);
						}
						break;
					}
				case FileDialogState.Export:
					{
						if (DrawFileDialogMenu(save: true, null, ref file))
						{
							_editor.FileExport(file);
						}
						break;
					}
				case FileDialogState.LoadTex:
					{
						if (DrawFileDialogMenu(save: false, ".jpg|.png", ref file))
						{
							switch (_texType)
							{
								case TextureType.Trunk:
									{
										_editor.Tree.SetTrunkTextureDiffuse(_editor.Renderer.Device, file);
										break;
									}
								case TextureType.TrunkNormal:
									{
										_editor.Tree.SetTrunkTextureNormal(_editor.Renderer.Device, file);
										break;
									}
								case TextureType.Leaf:
									{
										_editor.Tree.SetLeafTextureDiffuse(_editor.ImGui, _editor.Renderer.Device, file);
										break;
									}
								case TextureType.LeafNormal:
									{
										_editor.Tree.SetLeafTextureNormal(_editor.Renderer.Device, file);
										break;
									}
								case TextureType.LeafAlpha:
									{
										_editor.Tree.SetLeafTextureAlpha(_editor.Renderer.Device, file);
										break;
									}
							}
						}
						break;
					}
			}
		}

		bool DrawFileDialogMenu(bool save, string extensions, ref string file)
		{
			bool success = false;

			if (ImGui.BeginPopupModal(_fileDialogId, ref _fileDialogOpen, ImGuiWindowFlags.NoTitleBar))
			{
				var picker = FilePicker.GetFilePicker(this, Path.Combine(Environment.CurrentDirectory, ""), extensions,
					saveFileDialog: save);
				if (picker.Draw())
				{
					success = true;

					file = picker.SelectedFile;

					ClosePopup();
				}
				ImGui.EndPopup();
			}
			else
			{
				ClosePopup();
			}

			return success;
		}

		void ClosePopup()
		{
			if (_fileDialogState == FileDialogState.Closed)
				return;

			_fileDialogOpen = false;
			_fileDialogState = FileDialogState.Closed;
			FilePicker.RemoveFilePicker(this);
		}

		#endregion
	}
}
