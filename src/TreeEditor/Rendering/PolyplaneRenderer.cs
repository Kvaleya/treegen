﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;
using GlobCore;
using Treegen;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Handles the graphics state needed to render polyplane atlas.
	/// The class PolyplaneAtlas handles the rendering itself.
	/// </summary>
	class PolyplaneRenderer
	{
		Device _device;
		GraphicsPipeline _psoClear;
		MeshRenderModule _meshRenderer;

		public PolyplaneRenderer(Device device, MeshRenderModule meshRenderer)
		{
			_device = device;
			_meshRenderer = meshRenderer;
			_psoClear = new GraphicsPipeline(device, null, null,
				null, new RasterizerState(), new DepthState(DepthFunction.Always, true), null);
		}

		public PolyplaneAtlas GetAtlas(Vector2i size)
		{
			return new PolyplaneAtlas(this, size);
		}

		/// <summary>
		/// Stores polyplane textures and handles the execution of polyplane render commands supplied by the tree generator.
		/// </summary>
		public class PolyplaneAtlas : IDisposable
		{
			/// <summary>
			/// Texture size for this atlas
			/// </summary>
			public readonly Vector2i Size;

			/// <summary>
			/// Polyplane colormap with transparency in the alpha channel
			/// </summary>
			public Texture2D TexDiffuse;

			/// <summary>
			/// Polyplane normal map
			/// </summary>
			public Texture2D TexNormal;

			// The common approach is to combine diffuse and alpha textures into one,
			// however the tree editor uses a separate alpha texture for simplicity,
			// so the polyplane generator produces a separate alpha texture as well.
			// Also, preparing a good alpha/diffuse+alpha texture with mips is not trivial,
			// and is perhaps better left for handling by whatever engine consumes the textures.
			public Texture2D TexAlpha;

			/// <summary>
			/// Depth buffer for the polyplane fbo
			/// </summary>
			Texture2D _texDepthbuffer;

			/// <summary>
			/// Main framebuffer for this polyplane atlas
			/// </summary>
			FrameBuffer _fboMain;

			/// <summary>
			/// Vertex buffer for trunk mesh
			/// </summary>
			VertexBufferSet _vertexBuffersTrunk = new VertexBufferSet();

			/// <summary>
			/// Vertex buffer for leaves mesh
			/// </summary>
			VertexBufferSet _vertexBuffersLeaves = new VertexBufferSet();

			Device _device { get { return _polyplaneRenderer._device; } }

			PolyplaneRenderer _polyplaneRenderer;

			public PolyplaneAtlas(PolyplaneRenderer polyplaneRenderer, Vector2i size)
			{
				_polyplaneRenderer = polyplaneRenderer;
				Size = size;

				InitRendering();
			}

			/// <summary>
			/// Sets up textures and framebuffers.
			/// </summary>
			void InitRendering()
			{
				DeleteGlObjects();

				TexDiffuse = new Texture2D(_device, "PolyplaneDiffuse", SizedInternalFormatGlob.RGBA8, Size.X, Size.Y, 1);
				TexNormal = new Texture2D(_device, "PolyplaneNormal", SizedInternalFormatGlob.RGBA8, Size.X, Size.Y, 1);
				TexAlpha = new Texture2D(_device, "PolyplaneAlpha", SizedInternalFormatGlob.RGBA8, Size.X, Size.Y, 1);
				_texDepthbuffer = new Texture2D(_device, "PolyplaneDepthBuffer", SizedInternalFormatGlob.DEPTH_COMPONENT32F, Size.X, Size.Y, 1);

				_fboMain = new FrameBuffer();
				_device.BindFrameBuffer(_fboMain, FramebufferTarget.Framebuffer);
				_fboMain.Attach(FramebufferAttachment.ColorAttachment0, TexDiffuse);
				_fboMain.Attach(FramebufferAttachment.ColorAttachment1, TexNormal);
				_fboMain.Attach(FramebufferAttachment.ColorAttachment2, TexAlpha);
				_fboMain.Attach(FramebufferAttachment.DepthAttachment, _texDepthbuffer);

				_device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			}

			void DeleteGlObjects()
			{
				_fboMain?.Dispose();
				TexDiffuse?.Dispose();
				TexNormal?.Dispose();
				TexAlpha?.Dispose();
				_texDepthbuffer?.Dispose();
			}

			public void Dispose()
			{
				DeleteGlObjects();
			}

			/// <summary>
			/// Executes polyplane render commands. TreeBatch is needed to provide textures for trunk and leaves.
			/// </summary>
			public void ExecuteRenderCommands(TreeBatch treeBatch, List<RenderCommand> commands)
			{
				if (commands.Count == 0)
					return;

				// Create a dummy scene that will be filled with the currently rendered meshes.
				Scene scene = new Scene();
				scene.WorldObjects = new List<SceneObject>();

				// Enable multiple render targets
				_device.BindFrameBuffer(_fboMain, FramebufferTarget.Framebuffer);
				GL.DrawBuffers(3, new DrawBuffersEnum[]
				{
					DrawBuffersEnum.ColorAttachment0,
					DrawBuffersEnum.ColorAttachment1,
					DrawBuffersEnum.ColorAttachment2,
				});

				GL.Enable(EnableCap.ScissorTest);

				// Clear all
				GL.Viewport(0, 0, _texDepthbuffer.Width, _texDepthbuffer.Height);
				GL.Scissor(0, 0, _texDepthbuffer.Width, _texDepthbuffer.Height);
				_device.BindPipeline(_polyplaneRenderer._psoClear);
				GL.ClearColor(0f, 0f, 0f, 0f);
				GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

				// Execute every command
				foreach (var c in commands)
				{
					// Prepare matrixes

					// Projection matrix just compresses visible range into -1..1 cube
					// This cube is mapped to the proper atlas spot using glViewport
					Matrix4 projection = Matrix4.Identity;
					projection.Row0.X = 1f / c.Range.X;
					projection.Row1.Y = 1f / c.Range.Y;
					projection.Row2.Z = -1f / c.Range.Z;

					// Prepare scene
					_vertexBuffersTrunk.UpdateFromMesh(c.MeshTrunk.Vertices, c.MeshTrunk.Indices);
					_vertexBuffersLeaves.UpdateFromMesh(c.MeshLeaves.Vertices, c.MeshLeaves.Indices);

					scene.WorldObjects.Clear();

					scene.WorldObjects.Add(new SceneObject()
					{
						Color = Vector4.One,
						Mesh = _vertexBuffersTrunk,
						Transform = c.Transform,
						TexDiffuse = treeBatch.TexTrunkDiffuse,
						TexNormal = treeBatch.TexLeavesNormal,
					});

					scene.WorldObjects.Add(new SceneObject()
					{
						Color = Vector4.One,
						Mesh = _vertexBuffersLeaves,
						Transform = c.Transform,
						TexAlpha = treeBatch.TexLeavesAlpha,
						TexDiffuse = treeBatch.TexLeavesDiffuse,
						TexNormal = treeBatch.TexLeavesNormal,
					});

					// Prepare rendering
					GL.Viewport(c.ViewportMin.X, c.ViewportMin.Y, c.ViewportSize.X, c.ViewportSize.Y);
					GL.Scissor(c.ViewportMin.X, c.ViewportMin.Y, c.ViewportSize.X, c.ViewportSize.Y);
					
					// Draw
					_polyplaneRenderer._meshRenderer.Draw(scene, projection, isGbuffer: true);
				}

				// Clean up graphics state
				GL.Disable(EnableCap.ScissorTest);
				_device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
			}
		}
	}
}
