﻿using GlobCore;
using System;
using System.Collections.Generic;
using System.Text;
using TreeEditor.Rendering;
using OpenTK.Mathematics;
using OpenTK.Graphics.OpenGL;

namespace TreeEditor
{
	/// <summary>
	/// Handles the rendering of the entire scene, calls render modules.
	/// </summary>
	class WorldRenderer
	{
		public Device Device { get { return Context.Device; } }
		RenderContext Context;

		SkySimpleModule _moduleSky;
		TonemapModule _moduleTonemap;
		MeshRenderModule _moduleMeshes;

		public MeshRenderModule MeshRenderer => _moduleMeshes;

		Texture2D _texSceneColor;
		Texture2D _texSceneDepth;
		FrameBuffer _fboMainView;

		GraphicsPipeline _psoClear;

		public WorldRenderer(Device device)
		{
			Context = new RenderContext();
			Context.Device = device;

			_moduleSky = new SkySimpleModule(Context);
			_moduleTonemap = new TonemapModule(Context);
			_moduleMeshes = new MeshRenderModule(Context);

			_psoClear = new GraphicsPipeline(Device, null, null,
				null, new RasterizerState(), new DepthState(DepthFunction.Always, true), null);

			GL.LineWidth(2.0f);
		}

		public void DrawFrame(Scene scene, bool wireframe)
		{
			GL.Viewport(0, 0, Context.RenderWidth, Context.RenderHeight);

			// First, we clear the back buffer
			Device.BindPipeline(_psoClear);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

			using (Device.DebugMessageManager.PushGroupMarker("Main scene render"))
			{
				// We bind main scene buffer
				Device.BindFrameBuffer(_fboMainView, OpenTK.Graphics.OpenGL.FramebufferTarget.Framebuffer);
				GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit | ClearBufferMask.StencilBufferBit);

				// Draw sky
				_moduleSky.Draw(scene);

				// Draw the scene
				_moduleMeshes.Draw(scene, scene.Camera.GetCameraProjectionMatrix());

				// Draw scene wireframe if needed
				if(wireframe)
					_moduleMeshes.DrawWireframe(scene, scene.Camera.GetCameraProjectionMatrix());
			}
			Device.BindFrameBuffer(FrameBuffer.BackBuffer, OpenTK.Graphics.OpenGL.FramebufferTarget.Framebuffer);

			// Draw the tonemapped result to the back buffer
			_moduleTonemap.Apply(_texSceneColor, true);
		}

		public void OnResize(int width, int height)
		{
			Context.RenderWidth = width;
			Context.RenderHeight = height;

			_moduleSky.OnResize(width, height, width, height);
			_moduleTonemap.OnResize(width, height, width, height);
			_moduleMeshes.OnResize(width, height, width, height);

			RebuildFbos(width, height);
		}

		void RebuildFbos(int w, int h)
		{
			_fboMainView?.Dispose();
			_texSceneColor?.Dispose();
			_texSceneDepth?.Dispose();

			_texSceneColor = new Texture2D(Device, "SceneColor", SizedInternalFormatGlob.RGBA16F, w, h, 1);
			_texSceneDepth = new Texture2D(Device, "SceneDepth", SizedInternalFormatGlob.DEPTH32F_STENCIL8, w, h, 1);

			_fboMainView = new FrameBuffer();
			Device.BindFrameBuffer(_fboMainView, FramebufferTarget.Framebuffer);
			_fboMainView.Attach(FramebufferAttachment.ColorAttachment0, _texSceneColor);
			_fboMainView.Attach(FramebufferAttachment.DepthStencilAttachment, _texSceneDepth);

			Device.BindFrameBuffer(FrameBuffer.BackBuffer, FramebufferTarget.Framebuffer);
		}
	}
}
