﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Windowing.GraphicsLibraryFramework;
using OpenTK.Mathematics;

namespace TreeEditor
{
	class Input
	{
		public double TimeDelta;
		public double TimeEllapsed;
		public KeyboardState Keyboard;
		public MouseState Mouse;
		public Vector2 MouseDelta = Vector2.Zero;
		Input _old;

		public Input(double delta, double ellapsed, KeyboardState keys, MouseState mouse, Input old = null)
		{
			TimeDelta = delta;
			TimeEllapsed = ellapsed;
			Keyboard = keys;
			Mouse = mouse;
			_old = old;

			if (_old != null)
			{
				MouseDelta = new Vector2(Mouse.X - _old.Mouse.X, Mouse.Y - _old.Mouse.Y);
			}
		}

		public bool IsKeyDown(Keys k)
		{
			return Keyboard.IsKeyDown(k);
		}

		public bool IsKeyUp(Keys k)
		{
			return !Keyboard.IsKeyDown(k);
		}

		public bool IsKeyPressed(Keys k)
		{
			// Key is down now and was up in old input, or is down now and old input is null
			return Keyboard.IsKeyPressed(k);
		}
	}
}
