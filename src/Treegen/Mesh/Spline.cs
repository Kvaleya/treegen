﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	class Spline
	{
		// Xyz components are bezier curve control points, w is just linearly interpolated
		Vector4[] _controlPolygon;
		Vector3[] _points;

		public Spline(params Vector3[] polygon)
		{
			_controlPolygon = new Vector4[polygon.Length];

			for(int i = 0; i < _controlPolygon.Length; i++)
			{
				// Will w with "vertex index", normalized between 0..1
				_controlPolygon[i] = new Vector4(polygon[i], i / (float)(_controlPolygon.Length - 1));
			}

			// Find the control points of each cubic curve that makes up the spline
			// The start and end point of each curve is shared, except for the very first and very last point
			int cubicCurves = (polygon.Length - 1);
			_points = new Vector3[cubicCurves * 3 + 1];

			for (int i = 0; i < polygon.Length - 1; i++)
			{
				Vector3 sFirst = polygon[0];

				Vector3 third1 = (polygon[i] * 2 + polygon[i + 1]) / 3;
				Vector3 third2 = (polygon[i] + polygon[i + 1] * 2) / 3;

				if (i > 0)
				{
					Vector3 prevThird = (polygon[i - 1] + polygon[i] * 2) / 3;
					sFirst = (prevThird + third1) / 2;
				}

				_points[i * 3 + 0] = sFirst;
				_points[i * 3 + 1] = third1;
				_points[i * 3 + 2] = third2;
			}

			_points[_points.Length - 1] = polygon[polygon.Length - 1];
		}

		void GetCubicBezier(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, out Vector3 resultXyz,
			out Vector3 derivativeXyz)
		{
			float u = 1 - t;

			resultXyz = u * u * u * p0 + 3 * u * u * t * p1 + 3 * u * t * t * p2 + t * t * t * p3;
			derivativeXyz = 3 * u * u * (p1 - p0) + 6 * u * t * (p2 - p1) + 3 * t * t * (p3 - p2);
		}

		public Vector4 GetSplinePoint(float w, out Vector3 dir)
		{
			w = Math.Clamp(w * (_controlPolygon.Length - 1), 0, _controlPolygon.Length - 1.001f);

			int curveIndex = (int)Math.Floor(w);
			float t = w - curveIndex;

			curveIndex = Math.Min(Math.Max(curveIndex, 0), _controlPolygon.Length - 2);

			var p0 = _points[curveIndex * 3 + 0];
			var p1 = _points[curveIndex * 3 + 1];
			var p2 = _points[curveIndex * 3 + 2];
			var p3 = _points[curveIndex * 3 + 3];

			Vector3 pos;
			float resultW = MathUtils.Lerp(_controlPolygon[curveIndex].W, _controlPolygon[curveIndex + 1].W, t);

			GetCubicBezier(t, p0, p1, p2, p3, out pos, out dir);

			return new Vector4(pos.X, pos.Y, pos.Z, resultW);
		}
	}
}
