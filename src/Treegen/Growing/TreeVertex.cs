﻿using System;
using System.Collections.Generic;
using System.Net.Cache;
using System.Text;
using OpenTK.Mathematics;

namespace Treegen
{
	public enum TreeAttribute
	{
		Age, // "Nutrient amount", the lower this value is, the younger this spot is
		Dominance, // How trunk-like the branch is
		Stiffness, // Resistance to bending
		BranchHormone, // High value after the branch splits, prevents further splitting, value decreses with distance from split
		Roll, // In radians
		Gravitropism, // How much will the branch bend towards gravity
		LastBranchRadius, // Used to smooth out radius transitions when branching
	}

	public class TreeVertex
	{
		public static readonly int AttributeCount = Enum.GetNames(typeof(TreeAttribute)).Length;

		// Array of generic scalar attributes used in tree generation
		private float[] _attributes;

		/// <summary>
		/// Indexes into array of generic scalar attributes used for tree generation.
		/// </summary>
		/// <param name="attr">Attribute to access</param>
		/// <returns>Attribute value</returns>
		public float this[TreeAttribute attr]
		{
			get { return _attributes[(int) attr]; }
			set { _attributes[(int) attr] = value; }
		}

		/// <summary>
		/// Stores random generators that can be used by this branch
		/// </summary>
		public TreeRandomRepository Random;

		/// <summary>
		/// Child vertices of this vertex. First child is always the continuation of current branch, other children are new branches.
		/// </summary>
		public List<TreeVertex> Children = new List<TreeVertex>();

		/// <summary>
		/// Distance from previous vertex to this vertex
		/// </summary>
		public float Length = 0;

		/// <summary>
		/// Radius of the branch at this vertex
		/// </summary>
		public float Radius = 0;

		/// <summary>
		/// Direction of branch growth from previous vertex to this vertex
		/// </summary>
		public Vector3 Forward { get; internal set; } = Vector3.UnitY;

		/// <summary>
		/// Orientation of branch at this vertex
		/// </summary>
		public Vector3 Right { get; internal set; } = Vector3.UnitX;

		public Vector3 Down { get { return Vector3.Cross(Forward, Right).Normalized(); } }

		internal TreeVertex()
		{
			_attributes = new float[AttributeCount];
		}

		/// <summary>
		/// Sets branch orientation. Right vector fill be projected so that it is orthogonal to the forward vector
		/// </summary>
		/// <param name="forward"></param>
		/// <param name="right"></param>
		public void SetOrientation(Vector3 forward, Vector3 right)
		{
			Forward = forward.Normalized();
			Right = (right - Vector3.Dot(Forward, right) * Forward).Normalized();
		}

		public void Slant(float pitch)
		{
			var axis = Quaternion.FromAxisAngle(Forward, this[TreeAttribute.Roll]) * Right;

			var newforward = Quaternion.FromAxisAngle(axis, pitch) * Forward;

			SetOrientation(newforward, Right);
		}

		// Rotates the branch so that right vector is horizontal and up vector points up
		public void OrientUp()
		{
			Right = Vector3.Cross(Forward, Vector3.UnitY).Normalized();
		}

		public static TreeVertex CreateRoot(int seed, float age)
		{
			TreeVertex v = new TreeVertex();
			v[TreeAttribute.Age] = age;
			v[TreeAttribute.Dominance] = 1;
			v.Random = TreeRandomRepository.CreateNew(seed);
			v.SetOrientation(Vector3.UnitY, Vector3.UnitX);
			return v;
		}

		/// <summary>
		/// Creates a new TreeVertex, copying values from current one and using same random generators
		/// Use when adding new vertices to a branch.
		/// </summary>
		public TreeVertex CreateCurrentBranch()
		{
			return new TreeVertex(this);
		}

		/// <summary>
		/// Creates a new TreeVertex, copying values from current one and using new random generators
		/// Use when creating new branches.
		/// </summary>
		public TreeVertex CreateNewBranch()
		{
			var v = new TreeVertex(this);
			v.Random = this.Random.CreateForBranch();
			return v;
		}

		/// <summary>
		/// Creates a deep clone of another treevertex (except for the list of children which stays empty)
		/// </summary>
		/// <param name="vertex">Vertex to clone values from</param>
		private TreeVertex(TreeVertex vertex)
			: this()
		{
			this.Radius = vertex.Radius;
			this.Length = vertex.Length;
			this.Forward = vertex.Forward;
			this.Right = vertex.Right;
			this.Random = vertex.Random;

			for(int i = 0; i < AttributeCount; i++)
			{
				_attributes[i] = vertex._attributes[i];
			}
		}

		/// <summary>
		/// Returns array of values that can be used to generate branch clusters using a common algorithm such as K-Means
		/// </summary>
		internal float[] GetClusteringVector()
		{
			float[] vec = new float[0
				+ _attributes.Length // one for each attribute
				+ 1 // forward.y
				];

			for(int i = 0; i < _attributes.Length; i++)
			{
				float weight = 1.0f;

				if((TreeAttribute)i == TreeAttribute.Age)
				{
					weight *= 64.0f;
				}
				if ((TreeAttribute)i == TreeAttribute.Dominance)
				{
					weight *= 4.0f;
				}
				if ((TreeAttribute)i == TreeAttribute.BranchHormone)
				{
					weight *= 0.0f;
				}

				vec[i] = _attributes[i] * weight;
			}

			// Vertical direction of growth
			vec[_attributes.Length] = Forward.Y * 0.5f;

			return vec;
		}
	}
}
