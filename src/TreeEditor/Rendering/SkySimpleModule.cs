﻿using System;
using System.Collections.Generic;
using System.Text;
using OpenTK.Graphics.OpenGL;
using GlobCore;

namespace TreeEditor.Rendering
{
	/// <summary>
	/// Handles the rendering of a very simple sky.
	/// </summary>
	class SkySimpleModule : RenderModule
	{
		GraphicsPipeline _psoSky;

		public SkySimpleModule(RenderContext context)
			: base(context)
		{
			_psoSky = new GraphicsPipeline(Device, Device.GetShader("fullscreenSimple.vert"),
				Device.GetShader("sky.frag"), null, new RasterizerState(), new DepthState(DepthFunction.Always, false));
		}

		public void Draw(Scene scene)
		{
			Device.BindPipeline(_psoSky);
			Device.ShaderFragment.SetUniformF("ray00", scene.Camera.Ray00);
			Device.ShaderFragment.SetUniformF("ray01", scene.Camera.Ray01);
			Device.ShaderFragment.SetUniformF("ray10", scene.Camera.Ray10);
			Device.ShaderFragment.SetUniformF("ray11", scene.Camera.Ray11);
			GL.DrawArrays(PrimitiveType.Triangles, 0, 3);
		}
	}
}
