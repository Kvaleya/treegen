﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using OpenTK.Mathematics;

namespace TreeEditor
{
	public static class Extensions
	{
		public static System.Numerics.Vector2 ToNumerics(this Vector2 vec)
		{
			return new System.Numerics.Vector2(vec.X, vec.Y);
		}

		public static System.Numerics.Vector3 ToNumerics(this Vector3 vec)
		{
			return new System.Numerics.Vector3(vec.X, vec.Y, vec.Z);
		}

		public static System.Numerics.Vector4 ToNumerics(this Vector4 vec)
		{
			return new System.Numerics.Vector4(vec.X, vec.Y, vec.Z, vec.W);
		}

		public static Vector2 ToOpenTK(this System.Numerics.Vector2 vec)
		{
			return new Vector2(vec.X, vec.Y);
		}

		public static Vector3 ToOpenTK(this System.Numerics.Vector3 vec)
		{
			return new Vector3(vec.X, vec.Y, vec.Z);
		}

		public static Vector4 ToOpenTK(this System.Numerics.Vector4 vec)
		{
			return new Vector4(vec.X, vec.Y, vec.Z, vec.W);
		}

		public static Vector2[] ToOpenTK(this System.Numerics.Vector2[] array)
		{
			Vector2[] result = new Vector2[array.Length];

			for(int i = 0; i < array.Length; i++)
			{
				result[i] = array[i].ToOpenTK();
			}

			return result;
		}
	}

	// Adapted from: https://stackoverflow.com/a/17308019
	public static class ListExtensions
	{
		static class ArrayAccessor<T>
		{
			public static Func<List<T>, T[]> Getter;

			static ArrayAccessor()
			{
				var dm = new DynamicMethod("get", MethodAttributes.Static | MethodAttributes.Public, CallingConventions.Standard, typeof(T[]), new Type[] { typeof(List<T>) }, typeof(ArrayAccessor<T>), true);
				var il = dm.GetILGenerator();
				il.Emit(OpCodes.Ldarg_0); // Load List<T> argument
				il.Emit(OpCodes.Ldfld, typeof(List<T>).GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance)); // Replace argument by field
				il.Emit(OpCodes.Ret); // Return field
				Getter = (Func<List<T>, T[]>)dm.CreateDelegate(typeof(Func<List<T>, T[]>));
			}
		}

		public static T[] GetInternalArray<T>(this List<T> list)
		{
			return ArrayAccessor<T>.Getter(list);
		}
	}
}
