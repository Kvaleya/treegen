﻿using System;
using System.Collections.Generic;
using System.Text;
using Treegen;

namespace Treegen
{
	/// <summary>
	/// Handles gravitropism.
	/// </summary>
	class GravitropismModule : GrowModule
	{
		public override double Order => 10.0;

		/// <summary>
		/// Gravitropism mostly affect non-dominant branches.
		/// </summary>
		[GrowParamSlider(0.0001f, 0.1f)]
		public float GravitropismPotency { get; set; } = 0.02f;

		/// <summary>
		/// A fake tropism which pushes dominant branches upwards.
		/// </summary>
		[GrowParamSlider(0.0001f, 0.5f)]
		public float AntiGravitropismPotency { get; set; } = 0.1f;

		public override void Apply(GrowContext context, TreeVertex vertex)
		{
			double g = 0;

			// Gravitropism
			g += GravitropismPotency * (1 - vertex[TreeAttribute.Dominance]);

			// Antigravitropism
			g -= AntiGravitropismPotency * vertex[TreeAttribute.Dominance];

			vertex[TreeAttribute.Gravitropism] = MathUtils.Clamp((float)g, -1, 1);
		}
	}
}
