﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	static class MathUtils
	{
		public static double Clamp(double a, double min, double max)
		{
			return Math.Min(Math.Max(a, min), max);
		}

		public static float Clamp(float a, float min, float max)
		{
			return Math.Min(Math.Max(a, min), max);
		}

		public static double Lerp(double x, double y, double a)
		{
			return x * (1 - a) + y * a;
		}

		public static float Lerp(float x, float y, float a)
		{
			return x * (1 - a) + y * a;
		}
	}
}
