﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Treegen
{
	/// <summary>
	/// Stores the result of mesh generation. Note that multiple tree instances of the same LOD may be generated at once.
	/// </summary>
	public class MesherResult
	{
		public List<MesherResultMeshes> Meshes = new List<MesherResultMeshes>();
		public List<RenderCommand> PolyplaneRenderCommands = new List<RenderCommand>();
	}

	public class MesherResultMeshes
	{
		public Mesh MeshTrunk = new Mesh();
		public Mesh MeshLeaves = new Mesh();
		public Mesh MeshPolyplanes = new Mesh();
	}
}
