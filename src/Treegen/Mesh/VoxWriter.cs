﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK.Mathematics;

namespace Treegen
{
	/// <summary>
	/// Handles exporting a voxelized tree into the MagicaVoxel .vox format. Currently unused.
	/// </summary>
	public static class VoxWriter
	{
		const byte ColorIndexTrunk = 131; // Colors picked from MagicaVoxel's default palette
		const byte ColorIndexLeaf = 161;

		// Note: this program uses Y axis for up, but MagicaVoxel vox uses Z for up. Therefore this method swaps Y and Z axes when writing voxel data.
		// For vox format description, see: https://github.com/ephtracy/voxel-model/blob/master/MagicaVoxel-file-format-vox.txt
		public static void WriteVox(BinaryWriter bw, byte[] voxels, Vector3i modelSize)
		{
			int voxelCount = 0;
			for (int i = 0; i < voxels.Length; i++)
			{
				if (voxels[i] > 0)
					voxelCount++;
			}

			// Header
			bw.WriteChars("VOX ");
			bw.Write((int)150);

			const int chunkHeaderSize = 12;
			const int sizeChunkContents = 12;
			int xyziChunkContents = voxelCount * 4 + 4;

			// MAIN chunk
			bw.WriteChars("MAIN");
			bw.Write((int)0);
			bw.Write((int)(chunkHeaderSize + sizeChunkContents + chunkHeaderSize + xyziChunkContents));

			// SIZE chunk
			bw.WriteChars("SIZE");
			bw.Write((int)sizeChunkContents);
			bw.Write((int)0);
			bw.Write((int)modelSize.X);
			bw.Write((int)modelSize.Z); // Y and Z swapped (Y is up in TreeGen, Z is up in .vox)
			bw.Write((int)modelSize.Y);

			// XYZI chunk
			bw.WriteChars("XYZI");
			bw.Write((int)xyziChunkContents);
			bw.Write((int)0);

			bw.Write((int)voxelCount);

			for (int z = 0; z < modelSize.Z; z++)
			{
				for (int y = 0; y < modelSize.Y; y++)
				{
					for (int x = 0; x < modelSize.X; x++)
					{
						int index = x + y * modelSize.X + z * modelSize.X * modelSize.Y;
						
						// Skip empty voxels
						if (voxels[index] == 0)
							continue;

						byte color = 0;

						if (voxels[index] == Voxelizer.VoxelTrunk)
						{
							color = ColorIndexTrunk;
						}
						if (voxels[index] == Voxelizer.VoxelLeaf)
						{
							color = ColorIndexLeaf;
						}

						bw.Write((byte)x);
						bw.Write((byte)z); // Swapped Y and Z
						bw.Write((byte)y);
						bw.Write((byte)color);
					}
				}
			}
		}
	}
}
